#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2021
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""



ScriptName = 'M1MartinottiOnMovement_BehavOnly'

print(f'[{ScriptName}] Importing dependencies...')
import os
import sys
import shutil
import numpy as np
from copy import deepcopy as dcp
from glob import glob
from sciscripts.Analysis import Analysis, DLC, Stats
from sciscripts.IO import IO

RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data"
AnalysisPath = f"{RepoPath}/Examples/Analysis"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

sys.path.insert(len(sys.path), RepoPath)
import M1MartinottiOnMovement_Core as Core


LHCutOff = 0.7
CalculateOffset = True
Overwrite = False

print(f'[{ScriptName}] Done.')



#%% [PRT] Extract and classify reaches =================================
DLCFiles = [
    _ for _ in sorted(glob(f'{DataPath}/**/*.h5', recursive=True))
    if 'iteration-' not in _
    and 'labeled-data' not in _
    and '_DeepLabCut/' not in _
    and 'Behaviour_L5_Gq_MC' in _
]

GroupOverwrite = False

for DF,DLCFile in enumerate(DLCFiles):
    print(f'[{ScriptName}] File {DF+1} of {len(DLCFiles)}...')
    print(f"    [{ScriptName}] {'/'.join(DLCFile.split('/')[-2:])}")

    Output = DLCFile.split('DLC_resnet')[0]+'_Reaches'
    Output = Output.replace(DataPath, AnalysisPath)
    Output = Output.replace('/Acute/','/Acute_PRT/').replace('/Learning/','/Learning_PRT/')
    if os.path.isdir(Output) and not Overwrite:
        print(f'    [{ScriptName}] Already done!')
        continue

    GroupOverwrite = True

    print(f'    [{ScriptName}] Getting reach times...')
    DLCX, DLCY, DLCLh, BodyParts = DLC.GetXYLh(DLCFile)

    DLCVideoFile = f"{'/'.join(DLCFile.split('/')[:-1])}/{DLCFile.split('/')[-1].split('DLC_')[0]}.*"

    try:
        DLCVideoFile = glob(DLCVideoFile)[0]
        dvInfo = sioVideo.GetInfo(DLCVideoFile)
    except IndexError:
        print(f"Video not found, using camera defaults.")
        dvInfo = {}
        dvInfo['FPS'] = 50
        dvInfo['Width'] = 1920
        dvInfo['Height'] = 1080
        dvInfo['FrameNo'] = DLCX.shape[0]

    # Override - videos with corrupted metadata
    if dvInfo['FPS']==0:
        dvInfo['FPS'] = 50
        dvInfo['Width'] = 1920
        dvInfo['Height'] = 1080
        dvInfo['FrameNo'] = DLCX.shape[0]


    Time = np.arange(DLCX.shape[0])/dvInfo['FPS']

    Good = np.ones(DLCX.shape[0], dtype=bool)
    for b in range(5,7):
        Good *= DLCLh[:,b] > LHCutOff

    Reaches = np.zeros(Good.shape[0])
    Reaches[np.where(Good)[0]] = 1

    # Remove single-frame non-reaches
    ReachesSingle = np.where(
        ((Reaches[:-2] == 1) * (Reaches[1:-1] == 0)) *
        (Reaches[2:] == 1)
    )[0]+1
    Reaches[ReachesSingle] = 1

    # Remove single-frame reaches
    ReachesSingle = np.where(
        ((Reaches[:-2] == 0) * (Reaches[1:-1] == 1)) *
        (Reaches[2:] == 0)
    )[0]+1
    Reaches[ReachesSingle] = 0

    TTLs = Analysis.QuantifyTTLs(Reaches)
    TTLsSec = TTLs[np.unique(np.round(Time[TTLs]).astype(int), return_index=True)[1]]
    ReachesSec = Time[TTLsSec]

    print(f'    [{ScriptName}] Done.')

    # Write to disk
    IO.Bin.Write({
        'Reaches': Reaches,
        'ReachesSec': ReachesSec,
        'Time': Time,
        'TTLs': TTLs,
        'TTLsSec': TTLsSec
    }, Output)


print(f'[{ScriptName}] Classifying reaches...')
for DF,DLCFile in enumerate(DLCFiles):
    print(f'[{ScriptName}] File {DF+1} of {len(DLCFiles)}...')
    print(f"    [{ScriptName}] {'/'.join(DLCFile.split('/')[-2:])}")

    Output = DLCFile.split('DLC_resnet')[0]+'_Reaches'
    Output = Output.replace(DataPath, AnalysisPath)
    Output = Output.replace('/Acute/','/Acute_PRT/').replace('/Learning/','/Learning_PRT/')
    if '.' in Output: Output = Output.replace('.','_')

    if os.path.exists(Output+'/ReachesClass.dat') and not Overwrite:
        print(f'    [{ScriptName}] Already done!')
        continue

    GroupOverwrite = True

    Reaches, ReachesSec, TTLs, TTLsSec, Time = [
        IO.Bin.Read(f'{Output}/{_}.dat', AsMMap=False)[0]
        for _ in ['Reaches', 'ReachesSec', 'TTLs', 'TTLsSec', 'Time']
    ]

    OutputInvalid = Output+'/TTLsInvalid.dat'
    Output += '/ReachesClass.dat'

    DLCX, DLCY, DLCLh, BodyParts = DLC.GetXYLh(DLCFile)

    DLCVideoFile = f"{'/'.join(DLCFile.split('/')[:-1])}/{DLCFile.split('/')[-1].split('DLC_')[0]}.*"
    try:
        DLCVideoFile = glob(DLCVideoFile)[0]
        dvInfo = sioVideo.GetInfo(DLCVideoFile)
    except IndexError:
        print(f"Video not found, using camera defaults.")
        dvInfo = {}
        dvInfo['FPS'] = 50
        dvInfo['Width'] = 1920
        dvInfo['Height'] = 1080
        dvInfo['FrameNo'] = DLCX.shape[0]

    # Override - videos with corrupted metadata
    if dvInfo['FPS']==0:
        dvInfo['FPS'] = 50
        dvInfo['Width'] = 1920
        dvInfo['Height'] = 1080
        dvInfo['FrameNo'] = DLCX.shape[0]

    Result, TTLsInvalid = Core.ReachClassify(Reaches, Time, TTLsSec, LHCutOff, DLCFile, dvInfo)

    try: Session = int(DLCFile.split('/')[-2].split('_')[-1].split(' ')[-1])
    except: Session = DF

    print(f"   [{ScriptName}] Session {Session}, {Result.shape[0]} reaches, {Result[Result=='Success'].shape[0]} correct")

    IO.Bin.Write(Result, Output)
    IO.Bin.Write(np.array(TTLsInvalid), OutputInvalid)

print(f'[{ScriptName}] Done.')



#%% [PRT] Group analysis (depends on the previous cell) ================
Session5Split = IO.Txt.Read(f"{DataPath}/BehavOnly/Session5Split.dict")

if GroupOverwrite:
    print(f'[{ScriptName}] Grouping analysis...')
    Group = {}
    GroupInfo = IO.Txt.Read(f"{DataPath}/BehavOnly/BehaviourL5GqMC-AnimalExperimentalOrder.dict")
    GroupInfo = IO.Txt.DictListsToArrays(GroupInfo)


    print(f'    [{ScriptName}] Getting Exp info...')
    Group['DLCFiles'] = np.array(DLCFiles)

    Group['Sessions'] = np.array([
        int(DLCFile.replace(DataPath+'/','').split('/')[-2].split('_')[-1].split(' ')[-1])
        for DF,DLCFile in enumerate(DLCFiles)
    ])

    Group['Animals'] = np.array([
        _.split('/')[-1].split('DLC_')[0].split(' ')[0].split('_')[0]
        for _ in DLCFiles
    ])

    # Override - wrongly labeled videos
    Group['Animals'] = np.array([
        GroupInfo['New'][GroupInfo['Wrong'].tolist().index(A)]
        if A in GroupInfo['Wrong'] else A
        for A in Group['Animals']
    ])


    print(f'    [{ScriptName}] Loading reaches...')
    Group['ReachesClasses'] = [
        IO.Bin.Read(
            (DLCFile.split('DLC_resnet')[0]+'_Reaches').replace(
                DataPath, AnalysisPath
            ).replace(
                '/Acute/','/Acute_PRT/'
            ).replace(
                '/Learning/','/Learning_PRT/'
            ) + '/ReachesClass.dat',
            AsMMap=False
        )[0]
        for DF,DLCFile in enumerate(DLCFiles)
    ]

    Group['ReachesClasses'] = [
        np.array([_ if _ == 'Success' else 'Error' for _ in r])
        for r in Group['ReachesClasses']
    ]


    print(f'    [{ScriptName}] Loading times...')
    Group['TotalTimes'] = [
        IO.Bin.Read(
            (DLCFile.split('DLC_resnet')[0]+'_Reaches').replace(
                DataPath, AnalysisPath
            ).replace(
                '/Acute/','/Acute_PRT/'
            ).replace(
                '/Learning/','/Learning_PRT/'
            ) + '/Time.dat',
            AsMMap=False
        )[0]
        for DF,DLCFile in enumerate(DLCFiles)
    ]

    Group['TotalTimes'] = np.array([_[-1] for _ in Group['TotalTimes']])


    print(f'    [{ScriptName}] Merging multiple trials...')
    GInd = [
        [
            F for F,Folder in enumerate(Group['DLCFiles'])
            if Group['Animals'][F] == animal and Group['Sessions'][F] == session
        ]
        for a,animal in enumerate(np.unique(Group['Animals']))
        for s,session in enumerate(np.unique(Group['Sessions']))
    ]

    ASMatch = np.array([len(_) != 0 for _ in GInd])

    GInd = [v for e,v in enumerate(GInd) if ASMatch[e]]

    DFs = []; FToDel = []
    for F,Folder in enumerate(Group['DLCFiles']):
        if F in FToDel: continue

        ToMerge = [_ for _ in GInd if F in _]
        if len(ToMerge) != 1:
            raise LookupError('Animal-Session mismatch.')

        ToMerge = ToMerge[0]
        Keep = min(ToMerge)
        Group['TotalTimes'][Keep] = sum([Group['TotalTimes'][_] for _ in ToMerge])
        Group['ReachesClasses'][Keep] = np.hstack([Group['ReachesClasses'][_] for _ in ToMerge])

        FToDel += [_ for _ in ToMerge if _!=Keep]

    Group = {
        K: [el for e,el in enumerate(V) if e not in FToDel]
        for K,V in Group.items()
    }

    Group = {
        K: np.array(V) if K != 'ReachesClasses' else V
        for K,V in Group.items()
    }

    Group['Groups'] = np.array([
        DLCFile.replace(DataPath+'/','').split('/')[1]
        for DF,DLCFile in enumerate(Group['DLCFiles'])
    ])

    Group['Genotypes'] = np.array([
        GroupInfo['Genotypes'][GroupInfo['Animals'] == Animal][0]
        for Animal in Group['Animals']
    ])

    # Clean excluded animals
    M = Group['Genotypes']=='Exc'
    Group['ReachesClasses'] = np.array(Group['ReachesClasses'], dtype=object)
    Group = {K: V[~M] for K,V in Group.items()}


    print(f'    [{ScriptName}] Splitting session 5...')
    M = Group['Groups']=='Learning'

    GReachesSec = [
        IO.Bin.Read(
            (DLCFile.split('DLC_resnet')[0]+'_Reaches').replace(
                DataPath, AnalysisPath
            ).replace(
                '/Acute/','/Acute_PRT/'
            ).replace(
                '/Learning/','/Learning_PRT/'
            ) + '/ReachesSec.dat',
            AsMMap=False
        )[0]
        for DF,DLCFile in enumerate(Group['DLCFiles'])
    ]

    # Move >5 first
    Group['Sessions'][M*(Group['Sessions']>5)] += 1

    G6 = {K:[] for K in Group.keys()}
    for S,Ses in enumerate(Group['Sessions']):
        if M[S] and Ses==5 and len(GReachesSec[S]):
            Thr = dcp(Session5Split[Group['Animals'][S]])
            m,s = (int(_) for _ in Thr.split(':'))
            Thr = (m*60)+s
            RCs6M = GReachesSec[S]>Thr

            for K,V in Group.items(): G6[K].append(V[S])
            G6['Sessions'][-1] = 6
            G6['ReachesClasses'][-1] = G6['ReachesClasses'][-1][RCs6M]
            Group['ReachesClasses'][S] = Group['ReachesClasses'][S][~RCs6M]

    Group = IO.Bin.MergeDictsAndContents(Group, G6)
    Group['ReachesClasses'] = Group['ReachesClasses'].tolist()

    print(f'[{ScriptName}] Done. Writing to disk...')
    Out = f"{AnalysisPathShared}/BehavOnly/PRT-Reaches"
    try: shutil.rmtree(f"{Out}/ReachesClasses/")
    except FileNotFoundError: pass
    IO.Bin.Write(Group, Out)
    print(f'[{ScriptName}] Done.')



#%% [PRT] Quantify group data ==========================================
Base = f"{AnalysisPathShared}/BehavOnly/PRT-Reaches"

GroupReaches = {
    _.split('.')[0]: IO.Bin.Read(f"{Base}/{_}", AsMMap=False)[0]
    for _ in (
        'ReachesClasses', 'TotalTimes.dat', 'Animals.dat', 'Genotypes.dat',
        'Sessions.dat', 'Groups.dat', 'DLCFiles.dat'
    )
}

GroupData = {}
GroupData['Total'] = [_.shape[0] for _ in GroupReaches['ReachesClasses']]

GroupData = {**GroupData, **{
    K: [_[_==K].shape[0] for _ in GroupReaches['ReachesClasses']]
    for K in ['Success', 'Error']
}}

for K in ['Total', 'Success', 'Error']:
    GroupData[K+'Raw'] = np.array(GroupData[K])
    GroupData[K] = np.array(GroupData[K])/((GroupReaches['TotalTimes']/60)/10)

GroupData['SuccessRatio'] = GroupData['Success']/GroupData['Total']
GroupData['SuccessRatio'][GroupData['Total']==0] = 0

Out = f"{AnalysisPathShared}/BehavOnly/PRT-Reaches/ReachesQnt"
try: shutil.rmtree(f"{Out}/")
except FileNotFoundError: pass
IO.Bin.Write(GroupData, Out)

print(f'[{ScriptName}] Done.')



#%% [PRT] Describe No of reaches =======================================
Base = f"{AnalysisPathShared}/BehavOnly/PRT-Reaches"

GroupReaches = {
    _.split('.')[0]: IO.Bin.Read(f"{Base}/{_}.dat", AsMMap=False)[0]
    for _ in ('Animals', 'Genotypes', 'Sessions')
}

GroupData = IO.Bin.Read(f"{Base}/ReachesQnt")[0]

for Gen in ('Cre+','WT'):
    for Ses in np.unique(GroupReaches['Sessions']):
        i = (
            (GroupReaches['Genotypes']==Gen)*
            (GroupReaches['Sessions']==Ses)
        )

        t = GroupData['TotalRaw'][i]
        r = GroupData['SuccessRatio'][i]
        a = GroupReaches['Animals'][i]
        m = t>0
        r,t,a = r[m],t[m],a[m]

        print(f'{Gen} session {Ses} | n = {len(t)}, reaches = {sum(t)}, SR = {round(r.mean()*100,2)}')



#%% [PRT] Stats ========================================================
Keys = ('Total','SuccessRatio')
FacNames = ('Gen', 'Session')
FacPaired = (False, True)
Groups = ('Learning_PRT',)

Base = f"{AnalysisPathShared}/BehavOnly/PRT-Reaches"

GroupReaches = {
    _.split('.')[0]: IO.Bin.Read(f"{Base}/{_}.dat", AsMMap=False)[0]
    for _ in ('Animals', 'Genotypes', 'Sessions', 'Groups')
}

GroupData = IO.Bin.Read(f"{Base}/ReachesQnt")[0]

print('Calculating stats...')
for Group in Groups:
    print(f'    {Group}...')
    for Key in Keys:
        print(f'        {Key}...')
        GKMask = GroupReaches['Groups'] == Group
        AnimalsU = np.unique(GroupReaches['Animals'][GKMask])

        SessionsU = np.unique(GroupReaches['Sessions'][GKMask])

        Sets = (
            np.array([1,2,6]),
        )
        SetsNames = (
            f"Stats-Reaches_{Key}-{'_'.join(FacNames)}-1_2_6-{Group}_CZP",
        )

        for EffName,GKSessions in zip(SetsNames,Sets):
            AnimalsComplete = np.array([
                False not in [
                    s in GroupReaches['Sessions'][GroupReaches['Animals']==Animal]
                    for s in GKSessions
                ]
                for Animal in AnimalsU
            ])

            AnimalsComplete = np.array([
                AnimalsComplete[AnimalsU==_][0]
                    if _ in AnimalsU else False
                for _ in GroupReaches['Animals']
            ])

            SessionsValid = np.array([_ in GKSessions for _ in GroupReaches['Sessions']])
            GenValid = np.array([_ in ('WT','Cre+') for _ in GroupReaches['Genotypes']])

            GKMask = GroupReaches['Groups'] == Group
            GKMask = GKMask*AnimalsComplete*SessionsValid*GenValid

            Data = GroupData[Key][GKMask]
            IDs = GroupReaches['Animals'][GKMask]
            FacGen = GroupReaches['Genotypes'][GKMask]
            FacSession = GroupReaches['Sessions'][GKMask].astype(str)
            FXs = [FacGen,FacSession]

            if len(Data)==0:
                print('        Not enough data to calculate statistics!')
                continue

            print(EffName, '='*(len(EffName)-73))
            Eff = Stats.Full(
                Data, FXs, IDs, FacPaired, 'auto', FacNames,
            )

            print(Stats.GetFullReport(Eff))

            EffData = dict(
                Data = Data,
                FXs = np.array(FXs).T,
                Ids = IDs,
                FacNames = np.array(FacNames),
                FacPaired = np.array(FacPaired)
            )

            Base = f"{AnalysisPathShared}/BehavOnly/PRT_BehavOnly-Stats"
            IO.Txt.Write(Eff, f"{Base}/{EffName}.dict")
            IO.Bin.Write(EffData, f"{Base}/{EffName}_Data")


print('Done.')



#%% EOF ================================================================
