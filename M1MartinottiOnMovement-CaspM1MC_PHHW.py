#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>, B. Ciralli <barbara.ciralli@gmail.com>
@date: 2022
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""


import numpy as np, os
from sciscripts.Analysis import Stats
from sciscripts.IO import IO

RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data"
AnalysisPath = f"{RepoPath}/Examples/Analysis"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere



#%% Pasta Handling and Hanging wire stats ==============================
PHInfo = IO.Bin.Read(f"{DataPath}/CaspM1MC/PastaHandling_CaspMC")[0]
HWInfo = IO.Bin.Read(f"{DataPath}/CaspM1MC/HangingWire_CaspMC")[0]

PHAnimals, PHGen, PHStart, PHTime = (
    PHInfo[_] for _ in ('Animals','Gen','Start','Time')
)

HWAnimals, HWGen, HWStart, HWTime = (
    HWInfo[_] for _ in ('Animals','Gen','Start','Time')
)

PHAnimalsU = np.unique(PHAnimals)
PHGenU = np.array([PHGen[PHAnimals==Animal][0] for Animal in PHAnimalsU])
PHDrop = np.array([PHStart[PHAnimals==Animal].shape[0] for A,Animal in enumerate(PHAnimalsU)])
HWAnimalsU = np.unique(HWAnimals)
HWGenU = np.array([HWGen[HWAnimals==Animal][0] for Animal in HWAnimalsU])
HWDrop = np.array([HWStart[HWAnimals==Animal].shape[0] for A,Animal in enumerate(HWAnimalsU)])

PHAD = Stats.Full(PHDrop, [PHGenU], PHAnimalsU, [False], 'auto', ['Gen'])
PHAT = Stats.Full(PHTime, [PHGen], PHAnimals, [False], 'auto', ['Gen'])
HWAD = Stats.Full(HWDrop, [HWGenU], HWAnimalsU, [False], 'auto', ['Gen'])
HWAT = Stats.Full(HWTime, [HWGen], HWAnimals, [False], 'auto', ['Gen'])

IO.Txt.Write(PHAD, f"{AnalysisPathShared}/GroupAnalysis/Stats/Stats-PastaDrop-Gen.dict")
IO.Txt.Write(HWAD, f"{AnalysisPathShared}/GroupAnalysis/Stats/Stats-HangingWireDrop-Gen.dict")
IO.Txt.Write(PHAT, f"{AnalysisPathShared}/GroupAnalysis/Stats/Stats-PastaTime-Gen.dict")
IO.Txt.Write(HWAT, f"{AnalysisPathShared}/GroupAnalysis/Stats/Stats-HangingWireTime-Gen.dict")

for S,Set,Id,Gen in (('Drop',PHDrop,PHAnimalsU,PHGenU),('Time',PHTime,PHAnimals,PHGen)):
    IO.Bin.Write(dict(
            Data = Set,
            FXs = np.array([Gen]).T,
            FacNames = np.array(['Gen']),
            FacPaired = np.array([False]),
            Ids = Id,
        ),
        f"{AnalysisPathShared}/GroupAnalysis/Stats/Stats-Pasta{S}-Gen_Data"
    )

for S,Set,Id,Gen in (('Drop',HWDrop,HWAnimalsU,HWGenU),('Time',HWTime,HWAnimals,HWGen)):
    IO.Bin.Write(dict(
            Data = Set,
            FXs = np.array([Gen]).T,
            FacNames = np.array(['Gen']),
            FacPaired = np.array([False]),
            Ids = Id,
        ),
        f"{AnalysisPathShared}/GroupAnalysis/Stats/Stats-HangingWire{S}-Gen_Data"
    )



#%% EOF ================================================================
