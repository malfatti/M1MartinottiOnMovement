#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2023
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""


ScriptSuf = 'LFP'
ScriptName = f'M1MartinottiOnMovement_{ScriptSuf}'

print(f'[{ScriptName}] Importing dependencies...')
import gc, numpy as np, os, shutil, sys
from glob import glob
from sciscripts.Analysis import Analysis, Stats
from sciscripts.IO import IO
from sciscripts.IO.IO import MultiProcess

Group = 'LFPM1GqMC'
RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data/{Group}"
AnalysisPath = f"{RepoPath}/Examples/Analysis/{Group}"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

sys.path.insert(len(sys.path), RepoPath)
import M1MartinottiOnMovement_Core as Core


FreqBands = (
    ((1,100), 'Full'),
)

ETAWindow = Core.ETAWindow
WindowName = Core.WindowName

TreatmentsOrder = ('NaCl', 'CZP')

GroupInfo = IO.Txt.Read(f'{DataPath}/LFPM1GqMC-CaspMC-AnimalExperimentalOrder.dict')
GroupInfoSingleCh = IO.Txt.Read(f'{DataPath}/LFPM1GqMC-AnimalExperimentalOrderSingleCh.dict')

print(f'[{ScriptName}] Done.')



#%% [LFP_PRT-Reaches] Group info =======================================
GroupInfoDLC = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches")[0]

print(f'[{ScriptName}] Gathering group data...')
Folders = sorted(glob(f'{DataPath}/*/*_PRT'))

GroupInfoLFP = {}

GroupInfoLFP['Animals'] = np.array([
    R.split('/')[-1].split('_')[-3]
    for R in Folders
])

GroupInfoLFP['Sessions'] = np.array([
    int(R.split('/')[-2].split('_')[-1])
    for R in Folders
])

GroupInfoLFP['Genotypes'] = [
    '_'.join([
        iGen[0]
        if 'tg' in iGen[1] or 'lx' in iGen[1] else 'WT'
        for iGen in G
    ])
    for G in [
        GroupInfo['Genotype'][GroupInfo['Animals'].index(A)]
        for A in GroupInfoLFP['Animals']
    ]
]

GroupInfoLFP['Groups'] = np.array([R.split('/')[-3] for R in Folders])

GroupInfoLFP['Animals'] = np.array([
    f"{G}_{A}"
    for G,A in zip(GroupInfoLFP['Groups'],GroupInfoLFP['Animals'])
])

GroupInfoLFP['Trials'] = np.empty(len(GroupInfoLFP['Animals']))
GroupInfoLFP['Trials'][:] = np.nan
for Session in np.unique(GroupInfoLFP['Sessions']):
    for Animal in np.unique(GroupInfoLFP['Animals']):
        Trials = np.where(
            (GroupInfoLFP['Sessions']==Session)
            * (GroupInfoLFP['Animals']==Animal)
        )[0]
        for t,T in enumerate(Trials):
            GroupInfoLFP['Trials'][T] = t

GroupInfoLFP['Trials'] = np.array(GroupInfoLFP['Trials'],dtype=int)

GroupInfoLFP['Folders'] = np.array([
    _.replace(DataPath+'/','') for _ in Folders
])


FoldersLFP, FoldersDLC = [], []
for F,File in enumerate(GroupInfoLFP['Folders']):
    PRTAnimal, PRTSession = (
        GroupInfoLFP[K][F] for K in ('Animals','Sessions')
    )

    DLCOut = GroupInfoDLC['Output'][
        (GroupInfoDLC['Animals']==PRTAnimal)
        * (GroupInfoDLC['Sessions']==PRTSession)
    ]

    if len(DLCOut) != 1:
        raise LookupError(
            f'[{ScriptName}] There should be exactly one video for each LFP recording folder.'
        )

    DLCOut = DLCOut[0]

    FoldersLFP.append(File)
    FoldersDLC.append(DLCOut)

GroupInfoLFP['FoldersLFP'] = np.array(FoldersLFP)
GroupInfoLFP['FoldersDLC'] = np.array(FoldersDLC)

GroupInfoLFP = {k: np.array(v) for k,v in GroupInfoLFP.items()}

IO.Bin.Write(GroupInfoLFP, f"{AnalysisPathShared}/{Group}/LFP_PRT-Amp")

print(f'[{ScriptName}] All done.')



#%% [LFP_PRT-Amp] Amplitudes ===========================================
GroupInfoLFP = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Amp")[0]
FoldersLFP = np.array([f"{DataPath}/{_}" for _ in GroupInfoLFP['FoldersLFP']])
FoldersDLC = np.array([f"{AnalysisPath}/{_}" for _ in GroupInfoLFP['FoldersDLC']])

GroupOverwrite = False

for F,Folder in enumerate(FoldersLFP):
    SessionPath = Folder.replace(DataPath, AnalysisPath+'/LFP_PRT-Amp')
    print(f"[{ScriptName}] {SessionPath.split('/')[-2]} - Folder {F+1} of {len(FoldersLFP)}...")

    cA = len(glob(f'{SessionPath}/Accepted.dat'))>0
    cT = len(glob(f'{SessionPath}/Time.dat'))>0
    cE = 0 not in [
        len(glob(f'{SessionPath}/ETAs_{_[1]}_{WindowName}.dat'))
        for _ in FreqBands
    ]

    if cA and cT and cE:
        print(f'    [{ScriptName}] Already done!'); print('')
        continue

    if cA:
        Accepted = IO.Bin.Read(f'{SessionPath}/Accepted.dat', AsMMap=False)[0]
    else:
        ThisAnimal = Folder.split('/')[-1].split('_')[-3]
        Accepted = GroupInfo['AcceptedCh'][GroupInfo['Animals'].index(ThisAnimal)]

        if not len(Accepted):
            print('')
            print(f'    [{ScriptName}] No accepted channel found for animal {ThisAnimal}.')
            continue

        Accepted = np.array(Accepted)-1
        IO.Bin.Write(Accepted, f'{SessionPath}/Accepted.dat')

    if not cT or not cE:
        print(''); print(f'    [{ScriptName}] Loading EPhys data...')
        Data, Rate = IO.DataLoader(Folder)
        Proc = list(Data.keys())[0]
        DataExp = list(Data[Proc].keys())[0]
        RecKeys = sorted(Data[Proc][DataExp].keys(), key = lambda x: int(x))

        Data = np.concatenate([Data[Proc][DataExp][_] for _ in RecKeys], axis=0)[:,:17]
        Data, Led = Data[:,Accepted], Data[:,16]
        Rate = Rate[Proc][DataExp]


    if cT:
        Time = IO.Bin.Read(f'{SessionPath}/Time.dat', AsMMap=False)[0]
    else:
        LedStart = Analysis.QuantifyTTLs(Led, 0.8)[0]
        Time = (np.arange(Led.shape[0])-LedStart)/Rate

        IO.Bin.Write(Time, f'{SessionPath}/Time.dat')

    print(''); print(f'    [{ScriptName}] Get TTLs from behavior...')
    Reaches = IO.Bin.Read(FoldersDLC[F], AsMMap=False)[0]
    ReachesClass = Reaches['ReachesClass'].copy()
    ReachesClassSimple = np.array([_ if _ == 'Success' else 'Error' for _ in ReachesClass])

    if 'TTLsInvalid' not in Reaches.keys():
        Reaches['TTLsInvalid'] = []

    ReachesTTLs = [
        el for e,el in enumerate(Reaches['TTLsSec'])
        if e not in Reaches['TTLsInvalid']
    ]

    TTLs = np.array([
        abs(Time-_).argmin() for _ in Reaches['Time'][ReachesTTLs]
    ])

    if len(TTLs):
        for Band in FreqBands:
            BF, BN = Band

            if not len(glob(f'{SessionPath}/ETAs_{BN}_{WindowName}.dat')):
                print('')
                print(f'    [{ScriptName}] Getting Event-Triggered average (ETA) {BN} band...')
                df = Analysis.FilterSignal(Data, Rate, BF, 2)
                ETAs = Analysis.Slice(
                    df, TTLs,
                    [round(ETAWindow[0]*Rate), round(ETAWindow[1]*Rate)]
                )

                IO.Bin.Write(ETAs, f'{SessionPath}/ETAs_{BN}_{WindowName}.dat')
                del(ETAs)


            if not len(glob(f'{SessionPath}/ETATime_{WindowName}.dat')):
                ETAs = IO.Bin.Read(f'{SessionPath}/ETAs_{BN}_{WindowName}.dat')[0]
                ETATime = np.arange(ETAWindow[0], ETAWindow[1], np.ptp(ETAWindow)/ETAs.shape[0])
                IO.Bin.Write(ETATime, f'{SessionPath}/ETATime_{WindowName}.dat')
                del(ETAs)


        try: del(Data,Led,df,ETAs)
        except: pass
        gc.collect()

        print(f'    [{ScriptName}] Done.'); print('')
        GroupOverwrite = True

print(f'[{ScriptName}] All done.')



#%% [PRT_LFP-Amp] Group analysis =======================================
GroupInfoLFP = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Amp")[0]
FoldersLFP = np.array([f"{DataPath}/{_}" for _ in GroupInfoLFP['FoldersLFP']])
FoldersDLC = np.array([f"{AnalysisPath}/{_}" for _ in GroupInfoLFP['FoldersDLC']])

AnimalsList, SessionsList = [
    np.unique(_) for _ in (GroupInfoLFP['Animals'], GroupInfoLFP['Sessions'])
]

GroupMerged = {
    'Animals': np.array([
        animal
        for animal in AnimalsList for session in SessionsList
    ]),
    'Sessions': np.array([
        session
        for animal in AnimalsList for session in SessionsList
    ]),
    'Groups': np.array([
        GroupInfoLFP['Groups'][0]
        for animal in AnimalsList for session in SessionsList
    ]),
}

GroupMerged['Genotypes'] = [
    '_'.join([
        iGen[0]
        if 'tg' in iGen[1] or 'lx' in iGen[1] else 'WT'
        for iGen in G
    ])
    for G in [
        GroupInfo['Genotype'][GroupInfo['Animals'].index(A.split('_')[1])]
        for A in GroupMerged['Animals']
    ]
]

GInd = [
    [
        F for F,Folder in enumerate(FoldersLFP)
        if GroupInfoLFP['Animals'][F] == animal
        and GroupInfoLFP['Sessions'][F] == GroupMerged['Sessions'][a]
    ]
    for a,animal in enumerate(GroupMerged['Animals'])
]

ASMatch = np.array([len(_) != 0 for _ in GInd])
# Get only the last trial
GInd = np.array([max(v) for e,v in enumerate(GInd) if ASMatch[e]])


print(f'    [{ScriptName}] Loading reaches...')
ReachesClass = [
    [
        IO.Bin.Read(f"{Folder}/ReachesClass.dat", AsMMap=False)[0]
        for F,Folder in enumerate(FoldersDLC)
        if GroupInfoLFP['Animals'][F] == animal
        and GroupInfoLFP['Sessions'][F] == GroupMerged['Sessions'][a]
        and F in GInd
    ]
    for a,animal in enumerate(GroupMerged['Animals'])
]

ReachesClassSimple = [
    [
        np.array([_ if _ == 'Success' else 'Error' for _ in RC])
        for RC in RCS
    ]
    for RCS in ReachesClass
]

for Band, BandName in FreqBands:
    print(f'    [{ScriptName} {BandName}] Merging ETAs...')
    GroupMerged = {
        **GroupMerged,
        **{f'ETAs{_}_{BandName}_{WindowName}':[] for _ in ('Suc', 'Err')}
    }
    GroupMerged[f'ETAs_{BandName}_{WindowName}'] = []

    FTWritten = False
    for a,animal in enumerate(GroupMerged['Animals']):
        ETA =  [
            IO.Bin.Read(
                f"{Folder.replace(DataPath, AnalysisPath+'/LFP_PRT-Amp')}/ETAs_{BandName}_{WindowName}.dat",
                AsMMap=False
            )[0]
            for F,Folder in enumerate(FoldersLFP)
            if GroupInfoLFP['Animals'][F] == animal
            and GroupInfoLFP['Sessions'][F] == GroupMerged['Sessions'][a]
            and F in GInd
            and len(glob(
                    f"{Folder.replace(DataPath, AnalysisPath+'/LFP_PRT-Amp')}/ETAs_{BandName}_{WindowName}.dat",
            ))
        ]

        GroupMerged[f'ETAs_{BandName}_{WindowName}'].append(
            np.concatenate([
                eta.mean(axis=1)
                for e,eta in enumerate(ETA)
                if eta.size
            ], axis=1) if len(ETA) else
            np.array([])
        )

        if not FTWritten:
            ETATime =  [
                IO.Bin.Read(
                    f"{Folder.replace(DataPath, AnalysisPath+'/LFP_PRT-Amp')}/ETATime_{WindowName}.dat",
                    AsMMap=False
                )[0]
                for F,Folder in enumerate(FoldersLFP)
                if GroupInfoLFP['Animals'][F] == animal
                and GroupInfoLFP['Sessions'][F] == GroupMerged['Sessions'][a]
                and F in GInd
                and len(glob(
                        f"{Folder.replace(DataPath, AnalysisPath+'/LFP_PRT-Amp')}/ETATime_{WindowName}.dat",
                ))
            ]

            if len(ETATime):
                GroupMerged[f'ETATime_{WindowName}'] = ETATime[0]

            if f'ETATime_{WindowName}' in GroupMerged.keys():
                FTWritten = True


        for Key in ('Success', 'Error'):
            print(f"        [{ScriptName} {BandName}] Animal {animal}, {Key} reaches, Session {GroupMerged['Sessions'][a]}...")
            GroupMerged[f'ETAs{Key[:3]}_{BandName}_{WindowName}'].append(
                np.concatenate([
                    eta[:,(ReachesClassSimple[a][e]==Key),:].mean(axis=1)
                    for e,eta in enumerate(ETA)
                    if eta.size
                    if True in np.array([(ReachesClassSimple[a][e]==Key)]).ravel()
                ], axis=1) if len([
                    eta[:,(ReachesClassSimple[a][e]==Key),:].mean(axis=1)
                    for e,eta in enumerate(ETA)
                    if eta.size
                    if True in np.array([(ReachesClassSimple[a][e]==Key)]).ravel()
                ]) else np.array([])
            )


print(f'    [{ScriptName}.{BandName}] Writing results to disk...')
GroupMerged = {
    k: np.array(v)
        if 'ETAs' not in k
        else v
    for k,v in GroupMerged.items()
}

IO.Bin.Write(GroupMerged, f"{AnalysisPath}/LFP_PRT-Amp-{WindowName}-Merged")

print(f'[{ScriptName}] All done.')



#%% [PRT_LFP-Amp] Statistics ===================================================
SingleCh = True

Keys = ('Success', 'Error')
Epochs = ('Before', 'After')
FacNames = ('Class', 'Epoch', 'Gen', 'Session', 'Treatment')
FacPaired = (True,True,False,True,True)


GAnimals, GGenotypes, GGroups, GSessions = (
    IO.Bin.Read(f"{AnalysisPath}/LFP_PRT-Amp-{WindowName}-Merged/{_}.dat")[0]
    for _ in ('Animals', 'Genotypes', 'Groups', 'Sessions')
)

ETATime = IO.Bin.Read(f"{AnalysisPath}/LFP_PRT-Amp-{WindowName}-Merged/ETATime_{WindowName}.dat")[0]

Classes = [_ if _ else 'All' for _ in Keys]


if SingleCh:
    Accepted = [
        GroupInfo['AcceptedCh'][[
            str(a) for a in GroupInfo['Animals']
        ].index(str(_.split('_')[-1]))]
        for _ in GAnimals
    ]

    ACh = [
        GroupInfoSingleCh['AcceptedCh'][[
            str(a) for a in GroupInfo['Animals']
        ].index(str(_.split('_')[-1]))]
        for _ in GAnimals
    ]

    ACh = [_[0] if len(_) else np.nan for _ in ACh]

    ACh = [
        Accepted[C].index(Ch) if Ch in Accepted[C] else np.nan
        for C,Ch in enumerate(ACh)
    ]

Baseline = ETATime < 0

for Band in FreqBands:
    ETAFiles = {
        Key: np.array(sorted(
            glob(
                f"{AnalysisPath}/LFP_PRT-Amp-{WindowName}-Merged/ETAs{Key[:3]}_{Band[1]}_{WindowName}/*.dat"
            ), key=lambda x: int(x.split('_')[-1].split('.')[0])
        ))
        for Key in Keys
    }

    if SingleCh:
        AData = np.ravel([
            [
                IO.Bin.Read(ETA, AsMMap=False)[0][B,ACh[E]].mean()
                    if IO.Bin.Read(ETA, AsMMap=False)[0].size
                    and not np.isnan(ACh[E])
                    else np.nan
                for B in (Baseline, ~Baseline)
            ]
            for Key in Keys
            for E,ETA in enumerate(ETAFiles[Key])
        ])

    else:
        AData = np.ravel([
            [
                IO.Bin.Read(ETA, AsMMap=False)[0][B,:].mean()
                    if IO.Bin.Read(ETA, AsMMap=False)[0].size
                    else np.nan
                for B in (Baseline, ~Baseline)
            ]
            for Key in Keys
            for E,ETA in enumerate(ETAFiles[Key])
        ])

    AId = np.tile(np.repeat(GAnimals,len(Epochs)), len(Keys))

    AFxs = dict(
        Session = np.tile(np.repeat(GSessions,len(Epochs)).astype(str), len(Classes)),
        Gen = np.tile(np.repeat(GGenotypes,len(Epochs)), len(Classes)),
        Epoch = np.tile(np.tile(Epochs, GGroups.shape[0]), len(Classes)),
        Class = np.array([[_ if _ else 'All']*len(ETAFiles[_])*len(Epochs) for _ in Keys]).ravel(),
    )

    AFxs['Treatment'] = np.array([
        TreatmentsOrder[0] if int(_)<11 else TreatmentsOrder[1]
        for _ in AFxs['Session']
    ])

    ASession = AFxs['Session'].copy()

    GSessionsU = sorted(np.unique(ASession), key=lambda x:int(x))
    ASesTr = [(int(_)-int(GSessionsU[0]))%3 for _ in GSessionsU]
    ASesTr = dict(zip(GSessionsU,ASesTr))
    AFxs['Session'] = np.array([ASesTr[_] for _ in AFxs['Session']]).astype(str)

    Eff = Stats.Full(
        AData, [AFxs[_] for _ in FacNames], AId,
        FacPaired, 'auto', FacNames
    )

    print(f"{Band[1]} Band {'='*60}")
    print(Stats.GetFullReport(Eff))

    EffData = dict(
        Data = AData,
        FXs = np.array([AFxs[_] for _ in FacNames]).T,
        Ids = AId,
        FacNames = np.array(FacNames),
        FacPaired = np.array(FacPaired),
    )

    AOutName = f"Stats-LFPAmp_{Band[1]}{'_SingleCh' if SingleCh else ''}-{'_'.join(FacNames)}-{WindowName}"
    IO.Txt.Write(Eff, f"{AnalysisPathShared}/{Group}/LFP_PRT-Stats/{AOutName}.dict")
    IO.Bin.Write(EffData, f"{AnalysisPathShared}/{Group}/LFP_PRT-Stats/{AOutName}_Data")

print('All done.')



#%% EOF ========================================================================
