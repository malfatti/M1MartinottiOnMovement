#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 20210415
@license: GNU GPLv3 <https://gitlab.com/malfatti/MiniscopedBehavior/raw/main/LICENSE>
@homepage: https://gitlab.com/Malfatti/MiniscopedBehavior
"""


ScriptSuf = 'LFP_PRT_Reaches'
ScriptName = f'M1MartinottiOnMovement_{ScriptSuf}'

print(f'[{ScriptName}] Loading dependencies...')
import numpy as np
import os
import shutil
import sys
from glob import glob

from sciscripts.Analysis import Analysis, DLC, Stats, Videos as saVideos
from sciscripts.IO import IO, Video as sioVideo

Group = 'LFPM1GqMC'
RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data/{Group}"
AnalysisPath = f"{RepoPath}/Examples/Analysis/{Group}"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

sys.path.insert(len(sys.path), RepoPath)
import M1MartinottiOnMovement_Core as Core

LHCutOff = Core.LHCutOff
CalculateOffset = True
Overwrite = False

GroupInfo = IO.Txt.Read(f'{DataPath}/LFPM1GqMC-CaspMC-AnimalExperimentalOrder.dict')

for K,V in GroupInfo.items():
    if K in ['Genotype','AcceptedCh','ExpOrder']:
        GroupInfo[K] = np.array(V, dtype=object)
    else:
        GroupInfo[K] = np.array(V)

print(f'[{ScriptName}] Done.')



#%% [LFP_PRT-Reaches] Group info =======================================
print(f'[{ScriptName}] Gathering data info...')
DLCFilesLearning = [
    _ for _ in sorted(glob(f"{DataPath.replace(Group,'*Learning')}/*/*.h5"))
]

DLCFiles = [
    _ for _ in sorted(glob(f'{DataPath}/*/*.h5'))
]

GroupInfoDLC = [{},{}]

for S,Set in enumerate((DLCFilesLearning,DLCFiles)):
    GroupInfoDLC[S]['Groups'] = np.array([
        DLCFile.split('/')[-3]
        for DF,DLCFile in enumerate(Set)
    ])

    GroupInfoDLC[S]['Sessions'] = np.array([
        int(DLCFile.split('/')[-2].split('_')[-1])
        for DF,DLCFile in enumerate(Set)
    ])

    GroupInfoDLC[S]['Animals'] = ['' for _ in Set]
    for DF,DLCFile in enumerate(Set):
        DFDate, DFGroup, DFSession = DLCFile.split('/')[-2].split('-')
        DFGroup = '_'.join(DFGroup.split('_')[:-1])
        DFSession = int(DFSession.split('_')[1])
        DFi = (
            (GroupInfo['ExpDate']==DFDate)*
            (GroupInfo['ExpGroup']==DFGroup)*
            (GroupInfo['ExpSession']==DFSession)
        )

        if GroupInfo['ExpOrder'][DFi].shape[0] > 1:
            raise ValueError('There should be only 1 list here!')

        if GroupInfo['ExpOrder'][DFi].shape[0] < 1: continue

        DFP = sorted(glob('/'.join(DLCFile.split('/')[:-1])+'/*h5'))
        if len(GroupInfo['ExpOrder'][DFi][0]) != len(DFP): continue

        GroupInfoDLC[S]['Animals'][DF] = str(GroupInfo['ExpOrder'][DFi][0][DFP.index(DLCFile)])

    GroupInfoDLC[S]['Animals'] = [f"{Group}_{_}" for _ in GroupInfoDLC[S]['Animals']]
    GroupInfoDLC[S]['Animals'] = np.array(GroupInfoDLC[S]['Animals'])
    GroupInfoDLC[S]['Files'] = np.array(Set)

GroupInfoDLC[0]['Files'] = np.array([
    _.replace(DataPath.replace(Group,'CaspM1MC_LFPM1GqMC_PRTLearning/'),'')
    for _ in GroupInfoDLC[0]['Files']
])
GroupInfoDLC[1]['Files'] = np.array([
    _.replace(DataPath+'/','') for _ in GroupInfoDLC[1]['Files']
])


GM = np.array([
    Animal in GroupInfoDLC[1]['Animals']
    for Animal in GroupInfoDLC[0]['Animals']
])
GroupInfoDLC[0] = {K: V[GM] for K,V in GroupInfoDLC[0].items()}
GroupInfoDLC[0]['Groups'] = np.array([Group for _ in GroupInfoDLC[0]['Groups']])

GroupInfoDLC = IO.Bin.MergeDictsAndContents(*GroupInfoDLC)

GroupInfoDLC['Genotypes'] = np.array([
    '_'.join([
        iGen[0] if 'tg' in iGen[1] or 'lx' in iGen[1] else 'WT'
        for iGen in GroupInfo['Genotype'][
            GroupInfo['Animals'] == Animal.split('_')[1]
        ][0]
    ])
    if Animal.split('_')[1] in GroupInfo['Animals'] else ''
    for Animal in GroupInfoDLC['Animals']
])

# Get only the last trial
GInd = [
    [
        F for F,Folder in enumerate(GroupInfoDLC['Files'])
        if GroupInfoDLC['Animals'][F] == animal and GroupInfoDLC['Sessions'][F] == session
    ]
    for a,animal in enumerate(np.unique(GroupInfoDLC['Animals']))
    for s,session in enumerate(np.unique(GroupInfoDLC['Sessions']))
]

ASMatch = np.array([len(_) != 0 for _ in GInd])

GInd = np.array([max(v) for e,v in enumerate(GInd) if ASMatch[e]])
LastTrial = np.array([F in GInd for F,Folder in enumerate(GroupInfoDLC['Files'])])

GM = GroupInfoDLC['Animals'] != ''
GM = GM*LastTrial

GroupInfoDLC = {
    K:V[GM]
    if type(V) != list else [
        el for e,el in enumerate(V) if GM[e]
    ]
    for K,V in GroupInfoDLC.items()
}


GroupInfoDLC['Output'] = np.array([
    f"LFP_PRT-Reaches/{GroupInfoDLC['Animals'][DF]}-{ScriptSuf}-Session{Session}"
    for DF,Session in enumerate(GroupInfoDLC['Sessions'])
])

IO.Bin.Write(GroupInfoDLC, f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches")

print(f'[{ScriptName}] All done.')



#%% [LFP_PRT-Reaches] Extract reaches and get time offset ==============
GroupOverwrite = False

GroupInfoDLC = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches")[0]
DLCFiles = np.array([f"{DataPath}/{_}" for _ in GroupInfoDLC['Files']])
DLCOutput = np.array([f"{AnalysisPath}/{_}" for _ in GroupInfoDLC['Output']])

print(''); print(f'[{ScriptName}] Extracting reaches and time offsets...')
for DF,DLCFile in enumerate(DLCFiles):
    print(f'[{ScriptName}] File {DF+1} of {len(DLCFiles)}...')
    print(f"    [{ScriptName}] {'/'.join(DLCFile.split('/')[-2:])}")

    Output = DLCOutput[DF]
    if os.path.isdir(Output) and not Overwrite:
        print(f'    [{ScriptName}] Already done!')
        continue

    GroupOverwrite = True

    DLCFile = glob(
        os.path.dirname(DataPath) + '/*/'
        + DLCFile.replace(DataPath+'/','')
    )

    if len(DLCFile)!= 1: raise LookupError('DLC file not found!')
    DLCFile = DLCFile[0]

    print(f'        [{ScriptName}] Getting time offset...')
    DLCVideoFile = DLCFile.split('DLC')[0]
    if len(glob(f'{DLCVideoFile}.???')):
        DLCVideoFile = glob(f'{DLCVideoFile}.???')[0]
        dvInfo = sioVideo.GetInfo(DLCVideoFile)
        LedStart = saVideos.GetLedBlinkStart(DLCVideoFile)
    else:
        print('Video not available, assuming default settings.')
        dvInfo = {'FPS': 50, 'Width': 1920, 'Height': 1080}
        LedStart = 0
    print(f'        [{ScriptName}] Done.')


    print(f'    [{ScriptName}] Getting reach times...')
    DLCX, DLCY, DLCLh, BodyParts = DLC.GetXYLh(DLCFile)

    Time = np.arange(DLCX.shape[0])/dvInfo['FPS']
    Time -= LedStart

    Good = np.ones(DLCX.shape[0], dtype=bool)
    for b in range(5,7):
        Good *= DLCLh[:,b] > LHCutOff

    Reaches = np.zeros(Good.shape[0])
    Reaches[np.where(Good)[0]] = 1


    # Remove single-frame non-reaches ----------------------------------
    ReachesSingle = np.where(
        ((Reaches[:-2] == 1) * (Reaches[1:-1] == 0)) *
        (Reaches[2:] == 1)
    )[0]+1
    Reaches[ReachesSingle] = 1


    # Remove single-frame reaches --------------------------------------
    ReachesSingle = np.where(
        ((Reaches[:-2] == 0) * (Reaches[1:-1] == 1)) *
        (Reaches[2:] == 0)
    )[0]+1
    Reaches[ReachesSingle] = 0
    print(f'    [{ScriptName}] Done.')


    # Get maximum of one reach per second ------------------------------
    TTLs = Analysis.QuantifyTTLs(Reaches)
    TTLsSec = TTLs[np.unique(np.round(Time[TTLs]).astype(int), return_index=True)[1]]
    ReachesSec = Time[TTLsSec]


    # Visual check -----------------------------------------------------
    # if len(Reaches)>10: BreakHere

    # from sciscripts.Analysis.Plot import Plot
    # plt = Plot.plt
    # plt.style.use('SciScripts_Small')

    # FigName = 'FigDLCLhEx'
    # A4Size = [6.301496, 9.721496] # removing 25mm each margin
    # FigSize = [A4Size[0]*0.8, A4Size[1]*0.3]
    # w = (-1*dvInfo['FPS'], 1*dvInfo['FPS'])
    # R=1415

    # for R in TTLsSec:
        # t = Time-Time[R]
        # Fig, Axes = plt.subplots(1,2, figsize=FigSize, constrained_layout=True)
        # Axes[0].plot(t[R+w[0]:R+w[1]], DLCLh[R+w[0]:R+w[1],5]*100, label='Paw')
        # Axes[0].plot(t[R+w[0]:R+w[1]], DLCLh[R+w[0]:R+w[1],6]*100, label='Finger1')
        # Axes[0].plot(t[R+w[0]:R+w[1]], DLCLh[R+w[0]:R+w[1],7]*100, label='Finger2')
        # Axes[0].plot(t[R+w[0]:R+w[1]], DLCLh[R+w[0]:R+w[1],8]*100, label='Finger3')
        # Axes[0].plot([t[R]]*2, [0,100], 'k')
        # Axes[0].plot([t[R+w[0]],t[R+w[1]]], [LHCutOff*100]*2, 'k--')
        # Axes[0].set_ylabel('Likelihood of presence [%]')
        # # Axes[0].set_title(f'{Time[R]+LedStart}')
        # Axes[0].legend(ncol=2)#, loc='lower center')

        # Axes[1].plot(t[R+w[0]:R+w[1]], Reaches[R+w[0]:R+w[1]]*100, 'k')
        # Axes[1].text(0, 105, 'Detected prehension event', dict(ha='center'))

        # for Ax in Axes:
            # Ax.set_xlabel('Time [s]')
            # Ax.set_ylim((0,119))
            # Plot.Set(Ax=Ax)

        # Fig.savefig(f"{os.environ['HOME']}/.cache/{FigName}-{WindowName}.svg", dpi=600)
        # plt.show()


    # Write to disk ----------------------------------------------------
    IO.Bin.Write({
        'Reaches': Reaches,
        'ReachesSec': ReachesSec,
        'Time': Time,
        'TTLs': TTLs,
        'TTLsSec': TTLsSec
    }, Output)


print(f'[{ScriptName}] All done.')



#%% [LFP_PRT-Reaches] Classify reaches =================================
print(''); print(f'[{ScriptName}] Classifying reaches...')
GroupInfoDLC = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches")[0]
DLCFiles = np.array([f"{DataPath}/{_}" for _ in GroupInfoDLC['Files']])
DLCOutput = np.array([f"{AnalysisPath}/{_}" for _ in GroupInfoDLC['Output']])

GroupOverwrite = False

for DF,DLCFile in enumerate(DLCFiles):
    print(f'[{ScriptName}] File {DF+1} of {len(DLCFiles)}...')
    print(f"    [{ScriptName}] {'/'.join(DLCFile.split('/')[-2:])}")

    Output = DLCOutput[DF]
    if os.path.exists(Output+'/ReachesClass.dat') and not Overwrite:
        print(f'    [{ScriptName}] Already done!')
        continue

    GroupOverwrite = True

    Reaches, ReachesSec, TTLs, TTLsSec, Time = [
        IO.Bin.Read(f'{Output}/{_}.dat', AsMMap=False)[0]
        for _ in ['Reaches', 'ReachesSec', 'TTLs', 'TTLsSec', 'Time']
    ]

    OutputInvalid = Output+'/TTLsInvalid.dat'
    Output += '/ReachesClass.dat'

    DLCFile = glob(
        os.path.dirname(DataPath) + '/*/'
        + DLCFile.replace(DataPath+'/','')
    )

    if len(DLCFile)!= 1: raise LookupError('DLC file not found!')
    DLCFile = DLCFile[0]

    DLCVideoFile = DLCFile.split('DLC')[0]
    if len(glob(f'{DLCVideoFile}.???')):
        DLCVideoFile = glob(f'{DLCVideoFile}.???')[0]
        dvInfo = sioVideo.GetInfo(DLCVideoFile)
    else:
        # Video not available, assuming default settings for our camera
        dvInfo = {'FPS': 50, 'Width': 1920, 'Height': 1080}

    Result, TTLsInvalid = Core.ReachClassify(Reaches, Time, TTLsSec, LHCutOff, DLCFile, dvInfo)

    Animal = GroupInfoDLC['Animals'][DF]
    Session = GroupInfoDLC['Sessions'][DF]

    print(f"   [{ScriptName}] Animal {Animal} Session {Session}, {Result.shape[0]} reaches, {Result[Result=='Success'].shape[0]} correct")

    IO.Bin.Write(Result, Output)
    IO.Bin.Write(np.array(TTLsInvalid), OutputInvalid)

print(f'[{ScriptName}] Done.')



#%% [LFP_PRT-Reaches] Group Reaches =====================================
print(''); print(f'[{ScriptName}] Gathering group data...')
GroupInfoDLC = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches")[0]
DLCOutput = np.array([f"{AnalysisPath}/{_}" for _ in GroupInfoDLC['Output']])

print(f'    [{ScriptName}] Loading reaches...')
GroupInfoDLC['ReachesClasses'] = [
    IO.Bin.Read(Out+'/ReachesClass.dat', AsMMap=False)[0]
    for Out in DLCOutput
]
GroupInfoDLC['ReachesClasses'] = [
    np.array([_ if _ == 'Success' else 'Error' for _ in r])
    for r in GroupInfoDLC['ReachesClasses']
]

print(f'    [{ScriptName}] Loading times...')
GroupInfoDLC['TotalTimes'] = [
    IO.Bin.Read(Out+'/Time.dat', AsMMap=False)[0]
    for Out in DLCOutput
]
GroupInfoDLC['TotalTimes'] = np.array([(_[-1]/60)/10 for _ in GroupInfoDLC['TotalTimes']])

AnimalsList, SessionsList = [np.unique(GroupInfoDLC[_]) for _ in ('Animals', 'Sessions')]

GroupInfoDLCMerged = {
    'Animals': [animal for animal in AnimalsList for session in SessionsList],
    'Sessions': [session for animal in AnimalsList for session in SessionsList],
    'Genotypes': [
        GroupInfoDLC['Genotypes'][GroupInfoDLC['Animals']==animal][0]
        for animal in AnimalsList for session in SessionsList
    ],
    'Groups': [animal.split('_')[0] for animal in AnimalsList for session in SessionsList],
    'TotalTimes': [
        GroupInfoDLC['TotalTimes'][
            (GroupInfoDLC['Animals']==animal)
            * (GroupInfoDLC['Sessions']==session)
        ].sum()
        for animal in AnimalsList for session in SessionsList
    ],
    'ReachesClasses': [
        np.concatenate([
            GroupInfoDLC['ReachesClasses'][_]
            for _ in np.where(
                (GroupInfoDLC['Animals']==animal)
                * (GroupInfoDLC['Sessions']==session)
            )[0]
        ])
        if np.where(
            (GroupInfoDLC['Animals']==animal)
            * (GroupInfoDLC['Sessions']==session)
        )[0].size
        else np.array([])
        for animal in AnimalsList for session in SessionsList
    ],
}

GroupInfoDLCMerged = {k: np.array(v)  if k != 'ReachesClasses' else v for k,v in GroupInfoDLCMerged.items()}

Out = f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches-Merged"
try: shutil.rmtree(Out)
except FileNotFoundError: pass
IO.Bin.Write(GroupInfoDLCMerged, Out)

print(f'[{ScriptName}] All done.')



#%% [LFP_PRT-Reaches] Success ratio ====================================
GroupInfoDLCMerged = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches-Merged")[0]
GroupInfoDLCMerged['ReachesClasses'] = np.array(GroupInfoDLCMerged['ReachesClasses'], dtype=object)

GroupData = {}

GroupData['Total'] = [_.shape[0] for _ in GroupInfoDLCMerged['ReachesClasses']]
GroupData = {**GroupData, **{
    K: [_[_==K].shape[0] for _ in GroupInfoDLCMerged['ReachesClasses']]
    for K in ['Success', 'Error']
}}

for K in ['Total', 'Success', 'Error']:
    GroupData[K+'Raw'] = np.array(GroupData[K])
    GroupData[K] = np.array(GroupData[K])/(GroupInfoDLCMerged['TotalTimes'])

GroupData['SuccessRatio'] = GroupData['Success']/GroupData['Total']

IO.Bin.Write(GroupData, f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches-Merged")

print(f'[{ScriptName}] All done.')



#%% [LFP_PRT-Reaches] Group Reaches describe ===========================
GroupInfoDLCMerged = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches-Merged")[0]
GroupInfoDLCMerged['ReachesClasses'] = np.array(GroupInfoDLCMerged['ReachesClasses'], dtype=object)

Report = []
for InfoGroup in np.unique(GroupInfoDLCMerged['Groups']):
    Report.append(f'Group {InfoGroup}')
    iG = GroupInfoDLCMerged['Groups']==InfoGroup

    for Animal in np.unique(GroupInfoDLCMerged['Animals'][iG]):
        iA = GroupInfoDLCMerged['Animals']==Animal

        for Session in np.unique(GroupInfoDLCMerged['Sessions'][iG]):
            iS = GroupInfoDLCMerged['Sessions']==Session

            Reaches = GroupInfoDLCMerged['ReachesClasses'][iG*iA*iS]
            if len(Reaches) > 1:
                raise LookupError('Mismatching data.')
            elif len(Reaches)==0:
                Report.append(
                    f'    {Animal}, Session {Session}, No reaches'
                )
                continue

            Reaches = Reaches[0]

            if len(Reaches)==0:
                Report.append(
                    f'    {Animal}, Session {Session}, No reaches'
                )
                continue

            n = [len(Reaches[Reaches==Class]) for Class in ('Success','Error')]
            sr = GroupInfoDLCMerged['SuccessRatio'][iG*iA*iS][0]
            sr = round(sr*100)
            Report.append(
                f'    {Animal}, Session {Session}, {n[0]} successful and {n[1]} failed, success ratio of {sr}%'
            )

        Report.append('-'*70)

    Report.append('='*70)

for _ in Report: print(_)



#%% [LFP_PRT-Reaches] Stats Session x Gen ==============================
GroupInfoDLCMerged = IO.Bin.Read(f"{AnalysisPathShared}/{Group}/LFP_PRT-Reaches-Merged")[0]
GroupInfoDLCMerged['ReachesClasses'] = np.array(GroupInfoDLCMerged['ReachesClasses'], dtype=object)

Keys = ('Success', 'Error', 'Total', 'SuccessRatio')
FacNames = ('Gen', 'Session')
FacPaired = (False, True)
SessionCutOff = 7

print('Calculating stats...')
for Key in Keys:
    AOutName = f"Stats-Reaches_{Key}-{'_'.join(FacNames)}-{Group}"
    print(f'        {AOutName}...')

    M = GroupInfoDLCMerged['Sessions'] >= SessionCutOff

    AnimalsU = np.unique(GroupInfoDLCMerged['Animals'][M])
    SessionsU = np.unique(GroupInfoDLCMerged['Sessions'][M])

    AnimalsComplete = np.array([
        False not in [
            s in GroupInfoDLCMerged['Sessions'][GroupInfoDLCMerged['Animals']==Animal]
            for s in SessionsU
        ]
        for Animal in AnimalsU
    ])

    AnimalsComplete = np.array([
        AnimalsComplete[AnimalsU==_][0]
            if _ in AnimalsU else False
        for _ in GroupInfoDLCMerged['Animals']
    ])

    SessionsValid = np.array([_ in SessionsU for _ in GroupInfoDLCMerged['Sessions']])

    M = M*AnimalsComplete*SessionsValid

    ad = GroupInfoDLCMerged[Key][M]
    aid = GroupInfoDLCMerged['Animals'][M]
    afg = GroupInfoDLCMerged['Genotypes'][M]
    afs = GroupInfoDLCMerged['Sessions'][M].astype(str)

    A = Stats.Full(ad, [afg,afs], aid, FacPaired, 'auto', FacNames)
    AR = Stats.GetFullReport(A)

    print(AR)

    AD = dict(
        Data = ad,
        FXs = np.array([afg,afs]).T,
        Ids = aid,
        FacNames = np.array(FacNames)
    )

    IO.Txt.Write(A, f"{AnalysisPathShared}/{Group}/LFP_PRT-Stats/{AOutName}.dict")
    IO.Bin.Write(AD, f"{AnalysisPathShared}/{Group}/LFP_PRT-Stats/{AOutName}_Data")

print('Done.')



#%% EOF ================================================================
