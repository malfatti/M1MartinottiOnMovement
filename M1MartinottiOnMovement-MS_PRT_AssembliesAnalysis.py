#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2022
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""

ScriptName = 'Assemblies'

import numpy as np, os, shutil
from copy import deepcopy as dcp
from glob import glob
from scipy import sparse
from sciscripts.Analysis import Analysis, Stats, CalciumImaging
from sciscripts.IO import IO, Mat
from sciscripts.Analysis.Units import Units

RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data"
AnalysisPath = f"{RepoPath}/Examples/Analysis"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

ETAWindow = (-1, 1)
GenotypesOrder = ('WT', 'Cre+')
GroupsMS = ('MiniscopeL3PCandGqMC', 'MiniscopeL5MC', 'MiniscopeL5PCandGqMC')
ClassesOrder = ('Success', 'Error')

GroupInfoMS = IO.Txt.Read(f'{DataPath}/MiniscopeL3_L5-AnimalExperimentalOrder.dict')
WindowName = f'Window_{int(ETAWindow[1]*1000)}'

def AssemblySalience(PeaksIn, PeaksOut, TimeIn, TimeOut):
    Sal = ((PeaksIn/TimeIn)-(PeaksOut/TimeOut))
    return(Sal)

def AssemblyResilience(AssemblyRes, AssemblyRef):
    Intersect = np.intersect1d(AssemblyRef, AssemblyRes).shape[0]
    Ratio = AssemblyRes.shape[0] / AssemblyRef.shape[0]

    Res = (
        (Intersect / AssemblyRef.shape[0]) +
        (Intersect / AssemblyRes.shape[0]) *
        (Ratio - 1) # -1 will return "difference" instead of ratio
    )

    return(Res)


Msg = r"""
For the assembly code to work, there should be at least one assembly at each
session for each animal. Such condition will likely never be satisfied under
code testing conditions. If `CodeTest` is `1`, random assemblies (random
samples of 1/3 of the neurons) will be added when no assembly is detected.
"""
try:
    print(Msg)
    CodeTest = input('CodeTest [0|1]: ')
    while CodeTest not in ('0', '1'):
        print('The answer should be 0 or 1!')
        CodeTest = input('CodeTest [True|False]: ')
    CodeTest = bool(int(CodeTest))
except:
    print('This frontend does not support input requests!')
    print('Set the CodeTest variable manually before continuing!')



#%% [PRT_MS-dF] Assemblies Assign neuron identities through sessions ===========
print(''); print(f'[{ScriptName}] Gathering data info...')
Group = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}")[0]
Folders = Group['Files']

MatFiles = np.array([
    f"{_}/Assemblies/RawTraces_dF_F_ASSEMBLIES-COLLECTION.mat"
    for _ in Folders
])


Group = {}
Group['Animals'] = np.array([
    f"{R.split(AnalysisPath)[1].split('/')[1].replace('_','')}_{int([_.split('_')[1] for _ in R.split('/')  if 'animal' in _][0]):02d}"
        if 'animal' in R else
        f"{R.split(AnalysisPath)[1].split('/')[1].replace('_','')}_{'_'.join(R.split(AnalysisPath)[1].split('/')[2].split('_')[:-1])}"
    for R in MatFiles
])

Group['Sessions'] = np.array([
    int([_.split('_')[2] for _ in R.split('/')  if '_pellet_' in _][0])
    for R in MatFiles
])

Group['Groups'] = np.array([_.split('_')[0] for _ in Group['Animals']])


# Separate spoon and plate trials in session 5
Delivery = np.array(['Spoon' if 'spoon' in _.lower() else 'Plate' for _ in MatFiles])
Group['Sessions'][Group['Sessions']>5] += 1
Group['Sessions'][(Group['Sessions']==5)*(Delivery=='Plate')] += 1


Group['Trials'] = np.empty(len(Group['Animals']))
Group['Trials'][:] = np.nan
for Session in np.unique(Group['Sessions']):
    for Animal in np.unique(Group['Animals']):
        for t,T in enumerate(
                np.where(
                    (Group['Sessions']==Session)
                    * (Group['Animals']==Animal)
                )[0]
            ):
            Group['Trials'][T] = t
Group['Trials'] = np.array(Group['Trials'],dtype=int)

AssGroup = {_: [] for _ in (
    'Animals', 'Sessions', 'Groups', 'Files', 'Assemblies'
)}


AnimalsMerged = sorted(glob(f'{AnalysisPath}/*-Matched'))
ReRun = []

for A,AnimalMerged in enumerate(AnimalsMerged):
    ASessions = IO.Bin.Read(f"{AnimalMerged}/Sessions.dat")[0]
    ATrials = IO.Bin.Read(f"{AnimalMerged}/Trials.dat")[0]
    AAnimal = AnimalMerged.split('/')[-1].split('-')[0]
    AGroup = AAnimal.split('_')[0]

    print(f"{AAnimal}, animal {A+1} of {len(AnimalsMerged)}")

    print('    Loading Assemblies...')
    AFiles = np.array([
        MatFiles[
            (Group['Animals']==AAnimal)
            * (Group['Sessions']==Session)
            * (Group['Trials']==ATrials[S])
        ][0]
        if True in (
            (Group['Animals']==AAnimal)
            * (Group['Sessions']==Session)
            * (Group['Trials']==ATrials[S])
        ) else 'NaN'
        for S,Session in enumerate(ASessions)
    ])


    AAssemblies = [
        Mat.Read(File)["ICA_CS_assemblies"]
        if len(glob(File.replace('.mat','*')))
        else np.array([])
        for File in AFiles
    ]

    AAssemblies = [
        [np.array([_]).ravel()-1 for _ in AAss]
            if hasattr(AAss, '__iter__') else []
        for AAss in AAssemblies
    ]

    AccFiles = [
        _.replace(
            'Assemblies/RawTraces_dF_F_ASSEMBLIES-COLLECTION.mat',
            'CNMFe/Accepted.dat'
        )
        for _ in AFiles
    ]

    Accepted = []
    for File in AccFiles:
        if File != 'NaN':
            if len(glob(File)):
                Accepted.append(IO.Bin.Read(File)[0])
            else:
                Accepted.append(IO.Bin.Read(File.replace('CNMFe/',''))[0])
        else:
            Accepted.append(np.array([]))

    Assignments = IO.Bin.Read(f"{AnimalMerged}/AssignmentsActive.dat")[0]
    if not Assignments.size:
        print('    No neurons active on all sessions! Re-run session alignment for this animal.')
        print()
        ReRun.append(AnimalMerged)
        continue

    Assignments = CalciumImaging.AcceptedAssigned(Assignments, Accepted)

    AAssemblies = [
        [[np.where(Assignments[:,S]==_)[0] for _ in aass] for aass in AAss]
        if len(AAss)>0 else np.array([])
        for S,AAss in enumerate(AAssemblies)
    ]

    AAssemblies = [
        [np.array([_ for _ in aass if _.size]).ravel() for aass in AAss]
        for AAss in AAssemblies
    ]

    ASessionsU = np.unique(ASessions)
    AAssemblies = [
        [
            _ for ass,Ass in enumerate(AAssemblies) for _ in Ass
            if ASessions[ass]==S if len(_)>0
        ]
        for S in ASessionsU
    ]

    if CodeTest:
        Valid = [len([_ for _ in A if len(_)>4])>0 for A in AAssemblies]
        if False in Valid:
            print('    '+'='*75)
            print('    CodeTest enabled!!')

            ToFill = np.arange(len(Valid))[~np.array(Valid)]
            N = Assignments.shape[0]
            AN = N//3

            for AF in ToFill:
                print(f'    Generating 3 fake assemblies for session {ASessionsU[AF]}...')
                AAssemblies[AF] = [
                    np.random.permutation(np.arange(N))[:AN]
                    for _ in range(3)
                ]

            print('    Done.')
            print('    '+'='*75)

    AssGroup['Animals'] += [AAnimal]*len(ASessionsU)
    AssGroup['Files'] += [AnimalMerged]*len(ASessionsU)
    AssGroup['Groups'] += [AGroup]*len(ASessionsU)
    AssGroup['Sessions'] += ASessionsU.tolist()
    AssGroup['Assemblies'] += AAssemblies

    print('Done.'); print()


GroupInfoMS = IO.Txt.DictListsToArrays(GroupInfoMS)
AssGroup['Genotypes'] = np.array([
    GroupInfoMS['Genotype'][GroupInfoMS['Animals']==_][0]
    for _ in AssGroup['Animals']
])
AssGroup['Files'] = [_.replace(f'{AnalysisPath}/','') for _ in AssGroup['Files']]
for K in ('Animals', 'Groups', 'Files', 'Sessions'):
    AssGroup[K] = np.array(AssGroup[K])

Out = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged"
try: shutil.rmtree(f"{Out}/Assemblies")
except FileNotFoundError: pass
IO.Bin.Write(AssGroup, Out)

print(f'[{ScriptName}] All done.')



#%% [PRT_MS-dF] Assemblies SigOnly =============================================
GAnimals, GSessions = (
    IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat")[0]
    for _ in ('Animals', 'Sessions')
)

EffFull = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/Stats-dF-Epoch_EachNeuron-{WindowName}")[0]
EffFull = {K: np.array(IO.Bin.DictToList(V),dtype=object) for K,V in EffFull.items()}

AssAnimals, AssSessions = (
    IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/{_}.dat")[0]
    for _ in ('Animals', 'Sessions')
)

Assemblies = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/Assemblies", AsMMap=False)[0]

if len(AssSessions)!=len(Assemblies):
    Assemblies += [[]]*(len(AssSessions)-len(Assemblies))

for an in np.unique(GAnimals):
    anclp = EffFull['ps'][GAnimals==an].tolist()
    if True in [_.size==0 for _ in anclp]:
        ThisShape = [_.shape for _ in anclp if len(_)]
        if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
            ThisShape = ThisShape[0]
            anclp = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in anclp]

    anclp = np.sum(np.array(anclp)<0.05,axis=0).astype(bool)

    for ses in np.where(GAnimals==an)[0]:
        for K in EffFull.keys():
            if EffFull[K][ses].size:
                EffFull[K][ses] = EffFull[K][ses][anclp]

        ThisI = (AssAnimals==GAnimals[ses])*(AssSessions==GSessions[ses])
        if len(np.where(ThisI)[0]) !=1:
            raise IndexError('There should be only one recording for each animal at each session.')

        ThisI = np.where(ThisI)[0][0]

        Assemblies[ThisI] = [
            np.array([_ for _ in ass if _ in np.where(anclp)[0]])
            for ass in Assemblies[ThisI]
        ]

Assemblies = [[_ for _ in Ass if len(_)>3] for Ass in Assemblies]

IO.Bin.Write(Assemblies, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/Assemblies_SigOnly")

print('Done.')



#%% [PRT_MS-dF] Assemblies activation ==========================================
OutAct = f"{AnalysisPath}/GroupAnalysis/PRT_MS-Assemblies"

Parameters = {
    'GetSigPeaks': {
        'STDThr': 2,
        'ShuffleRounds': 1000,
        'Alpha': 0.05
    }
}

RawGroup = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}")[0]

AssAnimals, AssGenotypes, AssSessions = (
    IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/{_}.dat")[0]
    for _ in ('Animals', 'Genotypes', 'Sessions')
)

Assemblies = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/Assemblies_SigOnly", AsMMap=False)[0]

if len(AssSessions)!=len(Assemblies):
    Assemblies += [[]]*(len(AssSessions)-len(Assemblies))

AnimalsU, SessionsU = (np.unique(_) for _ in (AssAnimals,AssSessions))

AllInfo = {K:[] for K in (
    'Animals', 'Genotypes', 'Groups', 'Sessions', 'Trials', 'Assemblies', 'Rates'
)}

AllC = 0
AllCLast = 0 # Override for continuing a previously stopped analysis

for A,Animal in enumerate(AnimalsU):
    AGroup = Animal.split('_')[0]
    MergeBase = f"{AnalysisPath}/{Animal}-Matched"
    Assignments = IO.Bin.Read(f"{MergeBase}/AssignmentsActive.dat")[0]
    ASessions = IO.Bin.Read(f"{MergeBase}/Sessions.dat")[0]
    ATrials = IO.Bin.Read(f"{MergeBase}/Trials.dat")[0]
    ABase = f"{AnalysisPath}/{AGroup}/{Animal}-PRT"

    Accepted = [
        IO.Bin.Read(f"{ABase}-Session{Session}_{ATrials[S]+1}/Imaging/Accepted.dat")[0]
        if len(glob(f"{ABase}-Session{Session}_{ATrials[S]+1}/Imaging/Accepted.dat"))
        else np.array([])
        for S,Session in enumerate(ASessions)
    ]

    Assignments = CalciumImaging.AcceptedAssigned(Assignments, Accepted)

    if not Assignments.size:
        print(f"No neurons assigned and accepted for {Animal}!")
        continue

    for S,Session in enumerate(SessionsU):
        iR = (RawGroup['Animals']==Animal)*(RawGroup['Sessions']==Session)
        if not True in iR: continue

        iR = np.where(iR)[0]

        Trials = RawGroup['Trials'][iR]
        Files = RawGroup['Files'][iR]

        iM = [(ASessions==Session)*(ATrials==_) for _ in Trials]
        if True in [np.where(_)[0].shape[0]!=1 for _ in iM]:
            raise LookupError('Accepted-Assigned mismatch.')
        iM = [np.where(_)[0][0] for _ in iM]

        iA = (AssAnimals==Animal)*(AssSessions==Session)
        iA = np.where(iA)[0]
        if len(iA) != 1:
            raise LookupError('Assembly-Raw mismatch.')
        iA = iA[0]

        for F,File in enumerate(Files):
            if AllC > (AllCLast-len(Assemblies[iA])):
                print(f'Data loaded at {AllC}')
                try:
                    RawTraces = IO.Bin.Read(f'{File}/CNMFe/RawTraces_dF_F.dat', AsMMap=False)[0]
                except FileNotFoundError:
                    RawTraces = IO.Bin.Read(f'{File}/RawTraces_dF_F.dat', AsMMap=False)[0]

                RawTraces = RawTraces[:,Assignments[:,iM[F]]]

            try:
                Time = IO.Bin.Read(f'{File}/Time.dat', AsMMap=False)[0]
            except FileNotFoundError:
                Time = IO.Bin.Read(f'{File}/Time', AsMMap=False)[0]
                TK = [K for K in Time.keys() if 'Miniscope' in K]
                if len(TK)>1:
                    raise LookupError('There should be only one miniscope!')
                Time = Time[TK[0]]

            Rate = int(round((1/np.diff(Time)).mean()))

            for Ass,Assembly in enumerate(Assemblies[iA]):
                if AllC < AllCLast:
                    AllInfo['Animals'].append(Animal)
                    AllInfo['Genotypes'].append(AssGenotypes[iA])
                    AllInfo['Groups'].append(AGroup)
                    AllInfo['Sessions'].append(Session)
                    AllInfo['Trials'].append(Trials[F])
                    AllInfo['Assemblies'].append(Ass)
                    AllInfo['Rates'].append(Rate)

                    AllC += 1
                    continue

                print(f"{Animal} Session {Session}_{Trials[F]} Assembly {Ass} idx {AllC}")
                Sig, SigCoActThr, SigCoActPeaks = CalciumImaging.GetSigPeaks(
                    RawTraces[:,Assembly], **Parameters['GetSigPeaks']
                )

                AllInfo['Animals'].append(Animal)
                AllInfo['Genotypes'].append(AssGenotypes[iA])
                AllInfo['Groups'].append(AGroup)
                AllInfo['Sessions'].append(Session)
                AllInfo['Trials'].append(Trials[F])
                AllInfo['Assemblies'].append(Ass)
                AllInfo['Rates'].append(Rate)

                IO.Bin.Write(sparse.csc_matrix(Sig), f'{OutAct}/Activity/Activity_{AllC}')
                IO.Bin.Write(SigCoActPeaks, f'{OutAct}/Active/Active_{AllC}.dat')
                IO.Bin.Write(SigCoActThr, f'{OutAct}/Threshold/Threshold_{AllC}.dat')

                AllC += 1

AllInfo = {K: np.array(V) for K,V in AllInfo.items()}
IO.Bin.Write(AllInfo, OutAct)

print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Assemblies activation within reaches ===========================
OutAct = f"{AnalysisPath}/GroupAnalysis/PRT_MS-Assemblies"
OutPSTH = f"{AnalysisPath}/GroupAnalysis/PRT_MS-Assemblies_Reach-{WindowName}"

RawGroup = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}")[0]

RawBase = [
    f"{AnalysisPath}/{RawGroup['Groups'][A]}/{Animal}-PRT-Session{RawGroup['Sessions'][A]}_{RawGroup['Trials'][A]+1}"
    for A,Animal in enumerate(RawGroup['Animals'])
]

RawReaches = [
    IO.Bin.Read(f"{RB}/Imaging/ReachesTS.dat", AsMMap=False)[0]
    if os.path.exists(f"{RB}/Imaging/ReachesTS.dat") else np.array([])
    for RB in RawBase
]

RawReachesClass = [
    IO.Bin.Read(f"{RB}/Reaches/ReachesClass.dat", AsMMap=False)[0]
    if os.path.exists(f"{RB}/Reaches/ReachesClass.dat") else np.array([])
    for RB in RawBase
]

RawReachesClass = [
    np.array([_ if _=='Success' else 'Error' for _ in RRC])
    for RRC in RawReachesClass
]

AssAnimals, AssGenotypes, AssSessions = (
    IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/{_}.dat")[0]
    for _ in ('Animals', 'Genotypes', 'Sessions')
)

AnimalsU, SessionsU = (np.unique(_) for _ in (AssAnimals,AssSessions))

Assemblies = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/Assemblies_SigOnly", AsMMap=False)[0]

if len(AssSessions)!=len(Assemblies):
    Assemblies += [[]]*(len(AssSessions)-len(Assemblies))

AssemblyAct = {
    K: IO.Bin.Read(f"{OutAct}/{K}.dat", AsMMap=False)[0]
    for K in ('Animals','Assemblies','Sessions','Trials')
}

AllInfo = {K:[] for K in (
    'Animals', 'Classes', 'Genotypes', 'Groups',
    'Sessions', 'Trials', 'Assemblies', 'AssemblyPeaksOut', 'AssemblyPeaksOutTime'
)}

AllC = 0
for A,Animal in enumerate(AnimalsU):
    AGroup = Animal.split('_')[0]

    for S,Session in enumerate(SessionsU):
        iR = (RawGroup['Animals']==Animal)*(RawGroup['Sessions']==Session)
        if not True in iR: continue

        iR = np.where(iR)[0]

        TTLs = [RawReaches[_] for _ in iR]
        TTLsClass = [RawReachesClass[_] for _ in iR]
        Trials = RawGroup['Trials'][iR]
        Files = RawGroup['Files'][iR]

        iA = (AssAnimals==Animal)*(AssSessions==Session)
        iA = np.where(iA)[0]
        if len(iA) != 1:
            raise LookupError('Assembly-Raw mismatch.')
        iA = iA[0]

        print(f"{Animal} Session {Session}")
        for F,File in enumerate(Files):
            TTLsF = {K: TTLs[F][TTLsClass[F]==K] for K in ClassesOrder}

            for Ass,Assembly in enumerate(Assemblies[iA]):
                iAct = (
                    (AssemblyAct['Animals']==Animal) *
                    (AssemblyAct['Assemblies']==Ass) *
                    (AssemblyAct['Sessions']==Session) *
                    (AssemblyAct['Trials']==Trials[F])
                )

                if np.where(iAct)[0].shape[0] == 0:
                    # No neurons assigned and accepted for {Animal}
                    continue
                elif np.where(iAct)[0].shape[0] != 1:
                    raise LookupError('Activity-PSTH mismatch!')

                iAct = np.where(iAct)[0][0]
                SigCoActPeaks = IO.Bin.Read(f"{OutAct}/Active/Active_{iAct}.dat")[0]
                SigAct = IO.Bin.Read(f"{OutAct}/Activity/Activity_{iAct}")[0]
                Rate = IO.Bin.Read(f"{OutAct}/Rates.dat")[0][iAct]
                Time = np.arange(SigAct.shape[0])/Rate

                PSTHOutMask = {
                    K: np.prod([
                        (
                            (((SigCoActPeaks/Rate)-(TTL/Rate)) > max(ETAWindow)) +
                            (((SigCoActPeaks/Rate)-(TTL/Rate)) < min(ETAWindow))
                        )
                        for TTL in TTLsF[K]
                    ], axis=0).astype(bool)
                    if len(TTLsF[K]) else np.array([]).astype(bool)
                    for K in ClassesOrder
                }

                TimeOutMask = {
                    K: np.prod([
                        (
                            ((Time-(TTL/Rate)) > max(ETAWindow)) +
                            ((Time-(TTL/Rate)) < min(ETAWindow))
                        )
                        for TTL in TTLsF[K]
                    ], axis=0).astype(bool)
                    if len(TTLsF[K]) else np.array([]).astype(bool)
                    for K in ClassesOrder
                }

                PSTHOut = {
                    K: np.where(PSTHOutMask[K])[0].shape[0]
                    for K in ClassesOrder
                }

                TimeOut = {
                    K: np.where(TimeOutMask[K])[0].shape[0]
                    for K in ClassesOrder
                }

                PSTHX = np.arange(*ETAWindow, 1/Rate)

                PSTHY = {
                    K: Units.PSTH(SigCoActPeaks/Rate, TTLsF[K]/Rate, PSTHX)
                    for K in ClassesOrder
                }


                for Class in ClassesOrder:
                    AllInfo['Animals'].append(Animal)
                    AllInfo['Classes'].append(Class)
                    AllInfo['Genotypes'].append(AssGenotypes[iA])
                    AllInfo['Sessions'].append(Session)
                    AllInfo['Groups'].append(AGroup)
                    AllInfo['Trials'].append(Trials[F])
                    AllInfo['Assemblies'].append(Ass)
                    AllInfo['AssemblyPeaksOut'].append(PSTHOut[Class])
                    AllInfo['AssemblyPeaksOutTime'].append(TimeOut[Class])

                    IO.Bin.Write(TTLsF[Class], f'{OutPSTH}/TTLs/TTLs_{AllC}.dat')
                    IO.Bin.Write(PSTHX, f'{OutPSTH}/PSTHX/PSTHX_{AllC}.dat')
                    IO.Bin.Write(PSTHY[Class], f'{OutPSTH}/PSTHY/PSTHY_{AllC}.dat')
                    AllC += 1

AllInfo = {K: np.array(V) for K,V in AllInfo.items()}
IO.Bin.Write(AllInfo, OutPSTH)

print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Assemblies PSTH merge trials ===================================
OutPSTH = f"{AnalysisPath}/GroupAnalysis/PRT_MS-Assemblies_Reach-{WindowName}"

Animals, Assemblies, Classes, Gens, Groups, Sessions, Trials = (
    IO.Bin.Read(f"{OutPSTH}/{_}.dat", AsMMap=False)[0]
    for _ in ('Animals', 'Assemblies', 'Classes', 'Genotypes', 'Groups', 'Sessions', 'Trials')
)

Group = {_: [] for _ in (
    'Animals', 'Assemblies', 'Classes', 'Genotypes', 'Groups', 'Sessions',
    f'PSTHX_{WindowName}', f'PSTHY_{WindowName}',
    f'AssemblyPeaksOut_{WindowName}', f'AssemblyPeaksOutTime_{WindowName}'
)}

AssemblyPeaksOut = IO.Bin.Read(f"{OutPSTH}/AssemblyPeaksOut.dat", AsMMap=False)[0]
AssemblyPeaksOutTime = IO.Bin.Read(f"{OutPSTH}/AssemblyPeaksOutTime.dat", AsMMap=False)[0]

print(f"[{ScriptName}] Merging PSTHs...")
AnimalsU = np.unique(Animals)
for A,Animal in enumerate(AnimalsU):
    iA = Animals==Animal

    AAssemblies, AClasses, AGens, AGroups, ASessions, ATrials = (
        _[iA] for _ in (Assemblies, Classes, Gens, Groups, Sessions, Trials)
    )

    APSTHY = np.where(iA)[0]

    AAssembliesU, AClassesU, ASessionsU, ATrialsU,  = (
        np.unique(_) for _ in (AAssemblies, AClasses, ASessions, ATrials)
    )

    for Ass in AAssembliesU:
        for Ses in ASessionsU:
            iAAS = (AAssemblies==Ass)*(ASessions==Ses)
            AASPSTHX = [
                IO.Bin.Read(f"{OutPSTH}/PSTHX/PSTHX_{_}.dat", AsMMap=False)[0]
                for _ in APSTHY[iAAS]
            ]

            if len(AASPSTHX)==0: continue

            Eq = False not in (
                False not in (_==AASPSTHX[0]) for _ in AASPSTHX
            )

            if not Eq:
                raise LookupError('PSTHX mismatch!')

            AASPSTHX = AASPSTHX[0]

            AASGens = AGens[iAAS]
            if np.unique(AASGens).shape[0]!=1:
                raise LookupError('Genotype mismatch!')

            AASGens = AASGens[0]

            for Class in AClassesU:
                iAASC = (AAssemblies==Ass)*(AClasses==Class)*(ASessions==Ses)
                AASCPSTHY = [
                    IO.Bin.Read(f"{OutPSTH}/PSTHY/PSTHY_{_}.dat", AsMMap=False)[0]
                    for _ in APSTHY[iAASC]
                ]
                AASCMask = [len(_)>0 for _ in AASCPSTHY]
                AASCPSTHY = [El for E,El in enumerate(AASCPSTHY) if AASCMask[E]]

                if len(AASCPSTHY)==0: continue

                if np.unique([_.shape[0] for _ in AASCPSTHY]).shape[0] != 1:
                    raise LookupError('Data mismatch!')

                AASCAPO = [
                    El for E,El in enumerate(
                        AssemblyPeaksOut[APSTHY[iAASC]]
                    )
                    if AASCMask[E]
                ]

                AASCAPOT = [
                    El for E,El in enumerate(
                        AssemblyPeaksOutTime[APSTHY[iAASC]]
                    )
                    if AASCMask[E]
                ]

                Group['Animals'].append(Animal)
                Group['Assemblies'].append(Ass)
                Group['Classes'].append(Class)
                Group['Genotypes'].append(AASGens)
                Group['Groups'].append(AGroups[iAASC][0])
                Group['Sessions'].append(Ses)
                Group[f'PSTHY_{WindowName}'].append(np.hstack(AASCPSTHY))
                Group[f'PSTHX_{WindowName}'].append(AASPSTHX)
                Group[f'AssemblyPeaksOut_{WindowName}'].append(sum(AASCAPO))
                Group[f'AssemblyPeaksOutTime_{WindowName}'].append(sum(AASCAPOT))

Group = {K: V if 'PSTH' in K else np.array(V) for K,V in Group.items()}

IO.Bin.Write(Group, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies_Reach-Merged")

print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Assemblies Salience ============================================
Act = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies_Reach-Merged", AsMMap=False)[0]
Group = 'MiniscopeL5PCandGqMC'

# Full window ----------------------------------------------------------
mG = Act['Groups']==Group
ActIn = np.array([np.sum(_) for _ in Act['PSTHY_Window_1000']])
TimeIn = np.array([np.prod(_.shape) for _ in Act['PSTHY_Window_1000']])
ActOut, TimeOut = (Act[f"AssemblyPeaksOut{_}_Window_1000"] for _ in ('','Time'))

Data = np.array([
    AssemblySalience(*z) for z in zip(ActIn,ActOut,TimeIn,TimeOut)
]) * 100
Data = Data[mG]

FacNames = ('Classes', 'Genotypes', 'Sessions')
FXs = [Act[_][mG] for _ in FacNames]
FacNames = ('Class', 'Gen', 'Session')
FacPaired = ( True, False, False)
IDs = np.array([
    f"{Act['Animals'][A]}_{Act['Sessions'][A]}_{Ass}"
    for A,Ass in enumerate(Act['Assemblies'])
])[mG]

Animals = np.array(['_'.join(_.split('_')[:2]) for _ in IDs])
SessionsU = np.unique(FXs[FacNames.index('Session')])
AnimalsValid = [
    A for A in np.unique(Animals) if 0 not in {
        S: np.unique(IDs[(FXs[FacNames.index('Session')]==S)*(Animals==A)]).shape[0]
        for S in SessionsU
    }.values()
]
AnimalsMask = np.array([_ in AnimalsValid for _ in Animals])

Data, IDs = (_[AnimalsMask] for _ in (Data,IDs))
FXs = [_[AnimalsMask] for _ in FXs]

Eff = Stats.Full(Data, FXs, IDs, FacPaired, 'auto', FacNames)
print(Stats.GetFullReport(Eff))

AOutName = f"Stats-SalienceCorrected_AccountFR-{'_'.join(sorted(FacNames))}-{Group}-{WindowName}"
AOut = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}"
IO.Txt.Write(Eff, f"{AOut}.dict")

try: shutil.rmtree(f"{AOut}_Data")
except FileNotFoundError: pass

IO.Bin.Write(
    dict(
        Data=Data,
        FacNames=np.array(FacNames),
        FacPaired=np.array(FacPaired),
        FXs=np.array(FXs).T,
        IDs=IDs
    ),
    f"{AOut}_Data"
)



# Split epochs ---------------------------------------------------------
mG = Act['Groups']==Group

ActIn = np.array([
    Act['PSTHY_Window_1000'][x][Ep[1:],:].sum()
    for x,X in enumerate(Act['PSTHX_Window_1000'])
    for Ep in (X<0,X>0)
    if mG[x]
])
TimeIn = np.array([
    np.prod(Act['PSTHY_Window_1000'][x][Ep[1:],:].shape)
    for x,X in enumerate(Act['PSTHX_Window_1000'])
    for Ep in (X<0,X>0)
    if mG[x]
])

ActOut = np.array([
    (
        Act['AssemblyPeaksOut_Window_1000'][x] +
        Act['PSTHY_Window_1000'][x][Ep[1:],:].sum()
    )
    for x,X in enumerate(Act['PSTHX_Window_1000'])
    for Ep in (X>0,X<0) # Opposite of ActIn, to account for peaks in the other epoch
    if mG[x]
])

TimeOut = np.array([
    (
        (
            Act['AssemblyPeaksOutTime_Window_1000'][x] +
            np.prod(Act['PSTHY_Window_1000'][x][Ep[1:],:].shape)
        )
    )
    for x,X in enumerate(Act['PSTHX_Window_1000'])
    for Ep in (X>0,X<0) # Opposite of ActIn, to account for peaks in the other epoch
    if mG[x]
])

Data = np.array([
    AssemblySalience(*z) for z in zip(ActIn,ActOut,TimeIn,TimeOut)
]) * 100

FacNames = ('Classes', 'Genotypes', 'Sessions')

FXs = [
    np.array([
        X for x,X in enumerate(Act[FN])
        for Ep in  ('Before','After')
        if mG[x]
    ])
    for FN in FacNames
]

FXs.append(np.array([
    Ep for x,X in enumerate(Act['PSTHX_Window_1000'])
    for Ep in  ('Before','After')
    if mG[x]
]))

FXs = [FXs[_] for _ in (0,3,1,2)]

FacNames = ('Class', 'Epoch', 'Gen', 'Session')
FacPaired = ( True, True, False, False)
IDs = np.array([
    f"{Act['Animals'][A]}_{Act['Sessions'][A]}_{Ass}"
    for A,Ass in enumerate(Act['Assemblies'])
    for Ep in  ('Before','After')
    if mG[A]
])


Animals = np.array(['_'.join(_.split('_')[:2]) for _ in IDs])
SessionsU = np.unique(FXs[FacNames.index('Session')])
AnimalsValid = [
    A for A in np.unique(Animals) if 0 not in {
        S: np.unique(IDs[(FXs[FacNames.index('Session')]==S)*(Animals==A)]).shape[0]
        for S in SessionsU
    }.values()
]
AnimalsMask = np.array([_ in AnimalsValid for _ in Animals])

Data, IDs = (_[AnimalsMask] for _ in (Data,IDs))
FXs = [_[AnimalsMask] for _ in FXs]


Eff = Stats.Full(Data, FXs, IDs, FacPaired, 'auto', FacNames)
print(Stats.GetFullReport(Eff))

AOutName = f"Stats-SalienceCorrected_AccountFR-{'_'.join(sorted(FacNames))}-{Group}-{WindowName}"
AOut = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}"
IO.Txt.Write(Eff, f"{AOut}.dict")

try: shutil.rmtree(f"{AOut}_Data")
except FileNotFoundError: pass

IO.Bin.Write(
    dict(
        Data=Data,
        FacNames=np.array(FacNames),
        FacPaired=np.array(FacPaired),
        FXs=np.array(FXs).T,
        IDs=IDs
    ),
    f"{AOut}_Data"
)


print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Latency Matrices CoMs ==========================================
def GetMatrices(Gap, IncludeSparse, SigOnly, SortBy):
    Keys = ('Success', 'Error')
    Norm = True
    FName = 'Matrices'
    if SigOnly: FName += '_SigOnly'
    if IncludeSparse: FName += '_Sparse'
    if SortBy == 'Error': FName += '_SortByError'

    GAnimals, GClasses, GGenotypes, GGroups, GSessions = (
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat")[0]
        for _ in ('Animals', 'Classes', 'Genotypes', 'Groups', 'Sessions')
    )

    GETAs = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/ETAs_{WindowName}")[0]
    EffFull = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/Stats-dF-Epoch_EachNeuron-{WindowName}")[0]
    EffFull = {K: np.array(IO.Bin.DictToList(V),dtype=object) for K,V in EffFull.items()}

    AssAnimals, AssGenotypes, AssGroups, AssSessions = (
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/{_}.dat")[0]
        for _ in ('Animals', 'Genotypes', 'Groups', 'Sessions')
    )

    SOSuf = '_SigOnly' if SigOnly else ''
    Assemblies = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged/Assemblies{SOSuf}", AsMMap=False)[0]

    if len(AssSessions)!=len(Assemblies):
        Assemblies += [[]]*(len(AssSessions)-len(Assemblies))

    Act = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies_Reach-Merged", AsMMap=False)[0]

    if SigOnly:
        for an in np.unique(GAnimals):
            anclp = EffFull['ps'][GAnimals==an].tolist()
            if True in [_.size==0 for _ in anclp]:
                ThisShape = [_.shape for _ in anclp if len(_)]
                if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                    ThisShape = ThisShape[0]
                    anclp = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in anclp]

            anclp = np.sum(np.array(anclp)<0.05,axis=0).astype(bool)

            for ses in np.where(GAnimals==an)[0]:
                for K in EffFull.keys():
                    if EffFull[K][ses].size:
                        EffFull[K][ses] = EffFull[K][ses][anclp]

                if GETAs[ses].size: GETAs[ses] = GETAs[ses][:,:,anclp]


    for Group in GroupsMS:
        if Group == 'MiniscopeL5MC': continue

        for G,Genotype in enumerate(GenotypesOrder):
            AnimalsU = np.unique(GAnimals[(GGroups==Group)*(GGenotypes==Genotype)])
            if not len(AnimalsU): continue

            SessionKeyAnimal = [[] for _ in AnimalsU]
            SessionKeyAnimalAss = [[] for _ in AnimalsU]

            AnimalNeuronIdOffset = 0
            for A,Animal in enumerate(AnimalsU):
                SessionsU =  np.unique(GSessions[(GGroups==Group)*(GAnimals==Animal)])
                SessionKey = [[] for _ in SessionsU]
                SessionKeyAss = [[] for _ in SessionsU]
                if not IncludeSparse: SkipAnimal = False

                for S,Session in enumerate(SessionsU):
                    if not IncludeSparse:
                        if SkipAnimal: continue

                    ThisGAS = [[] for _ in Keys]
                    ThisAss = [[] for _ in Keys]

                    for RC,Class in enumerate(Keys):
                        print(Genotype,Animal,Session,Class)

                        ThisI = np.where(
                            (GAnimals==Animal)*(GSessions==Session)*
                            (GGroups==Group)*(GClasses==Class)
                        )[0]

                        if len(ThisI) != 1:
                            raise LookupError('There should be exactly one recording for each animal at each session.')

                        ThisI = ThisI[0]


                        # Latencies ------------------------------------
                        ETAs = GETAs[ThisI]

                        if not ETAs.size:
                            # No reaches or no neurons with sig. changes
                            continue

                        ThisGAS[RC] = ETAs.mean(axis=1)


                        # Assemblies -----------------------------------
                        AssThisI = (AssAnimals==Animal)*(AssSessions==Session)
                        if len(np.where(AssThisI)[0]) !=1:
                            raise LookupError('There should be only one recording for each animal at each session.')
                        AssThisI = np.where(AssThisI)[0][0]

                        ThisAss[RC] = [_ for _ in Assemblies[AssThisI] if _.size]
                        ThisAss[RC] = [_ for _ in ThisAss[RC] if len(_)>3]


                    # Latencies ----------------------------------------
                    if False in [len(_)>0 for _ in ThisGAS]:
                        if IncludeSparse:
                            ThisShape = [_.shape for _ in ThisGAS if len(_)]
                            if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                                ThisShape = ThisShape[0]
                                ThisGAS = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in ThisGAS]
                        else:
                            SkipAnimal = True
                            continue

                    SessionKey[S] = dcp(ThisGAS)


                    # Assemblies ---------------------------------------
                    if not len(ThisAss):
                        raise LookupError('There should be at least 1 item in each list.')

                    SessionKeyAss[S] = dcp(ThisAss)


                # Latencies --------------------------------------------
                if False in [len(_)>0 for _ in SessionKey]:
                    if IncludeSparse:
                        raise LookupError('There should be at least 1 item in each list.')
                    else:
                        continue

                if IncludeSparse:
                    if False in (len(_)>0 for s in SessionKey for _ in s):
                        ThisShape = [_.shape for s in SessionKey for _ in s if len(_)]
                        if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                            ThisShape = ThisShape[0]
                            SessionKey = [
                                [
                                    _ if len(_)>0 else np.empty(ThisShape)*np.nan
                                    for _ in s
                                ]
                                for s in SessionKey
                            ]

                SessionKeyAnimal[A] = dcp(SessionKey)


                # Assemblies -------------------------------------------
                if False in [len(_)>0 for _ in SessionKeyAss]:
                    raise LookupError('There should be at least 1 item in each list.')

                SessionKeyAnimalAss[A] = dcp(SessionKeyAss)



            # Latencies ------------------------------------------------
            if not True in [len(_)>0 for _ in SessionKeyAnimal]: continue

            SessionKeyAnimal = [_ for _ in SessionKeyAnimal if len(_)]
            if IncludeSparse:
                ThisShape = max([len(_) for _ in SessionKeyAnimal])
                SessionKeyAnimal = [_ for _ in SessionKeyAnimal if len(_)==ThisShape]

            AnimalNeuronIdOffsets = [_[0][0].shape[1] for _ in SessionKeyAnimal]
            AnimalNeuronIdOffsets = [0]+AnimalNeuronIdOffsets[:-1]

            ANo, SNo, KNo = len(SessionKeyAnimal), len(SessionKeyAnimal[0]), len(SessionKeyAnimal[0][0])
            SessionKey = [
                [
                    np.concatenate(
                        [SessionKeyAnimal[A][S][K] for A in range(ANo)],
                        axis=1
                    )
                    for K in range(KNo)
                ]
                for S in range(SNo)
            ]

            DataNO = SessionKey[0][Keys.index(SortBy)]
            if Norm: DataNO = Analysis.Normalize(DataNO)
            DataNO[np.isnan(DataNO)] = 0
            NeuronOrder = np.argsort(DataNO.argmax(axis=0))


            # Assemblies -----------------------------------------------
            if not True in [len(_)>0 for _ in SessionKeyAnimalAss]:
                raise LookupError('There should be at least 1 item in each list.')

            SessionKeyAnimalAss = [_ for _ in SessionKeyAnimalAss if len(_)]
            if IncludeSparse:
                ThisShape = max([len(_) for _ in SessionKeyAnimalAss])
                SessionKeyAnimalAss = [_ for _ in SessionKeyAnimalAss if len(_)==ThisShape]

            SessionKeyAnimalAss = [
                [
                    [
                        [_+AnimalNeuronIdOffsets[A] for _ in ACl]
                        for ACl in ASes
                    ]
                    for ASes in AAn
                ]
                for A,AAn in enumerate(SessionKeyAnimalAss)
            ]

            ANo, SNo, KNo = len(SessionKeyAnimalAss), len(SessionKeyAnimalAss[0]), len(SessionKeyAnimalAss[0][0])
            SessionKeyAss = [
                [
                    [
                        _
                        for Ass in [SessionKeyAnimalAss[A][S][K] for A in range(ANo)]
                        for _ in Ass
                    ]
                    for K in range(KNo)
                ]
                for S in range(SNo)
            ]

            SessionKeyAss = [
                [
                    [
                        np.array([
                            np.where(NeuronOrder==n)[0][0]
                            for n in ass
                            if len(np.where(NeuronOrder==n)[0])
                        ])
                        for ass in ACl
                    ]
                    for ACl in ASes
                ]
                for ASes in SessionKeyAss
            ]

            IO.Bin.Write(SessionKeyAss, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/{FName}_{Group}_{Genotype}_Assemblies")


    print('Done.')


Gap = 5
IncludeSparse = (True,False)
SigOnly = (True,False)
SortBy = ('Success','Error')

Combs = tuple(Stats.product(
    IncludeSparse, SigOnly, SortBy
))
Combs = tuple(((Gap,)+_ for _ in Combs))

print(f"[{ScriptName}] Running...")
c=0
for IS in IncludeSparse:
    for SO in SigOnly:
        for SB in SortBy:
            print(f"[{ScriptName}] {c+1} of {len(Combs)}...")
            GetMatrices(Gap, IS, SO, SB)
            print('='*80); print()
            c +=1
print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Latency Matrices assemblies statistics =================
def GetMatricesStats(Gap, IncludeSparse, SigOnly, SortPerSession, SortBy, AssemblySession):
    Group = GroupsMS[2]

    Suf = ''
    if SigOnly: Suf += '_SigOnly'
    if IncludeSparse: Suf += '_Sparse'
    if SortBy == 'Error': Suf += '_SortByError'
    SPS = '-SortPerSession' if SortPerSession else ''

    GGroups, GSessions = (
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat", AsMMap=False)[0]
        for _ in ('Groups', 'Sessions')
    )


    try:
        SK = [
            IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/Matrices{Suf}_{Group}_{_}", AsMMap=False)[0]
            for _ in GenotypesOrder
        ]
    except FileNotFoundError:
        N = f'PeakLat{Suf}{SPS}_AssemblySession{AssemblySession}'
        print(f'Not enough data for {N}!')
        return(None)

    AssSK = [
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/Matrices{Suf}_{Group}_{_}_Assemblies", AsMMap=False)[0]
        for _ in GenotypesOrder
    ]

    SessionsU = np.unique(GSessions[GGroups==Group])
    Sessions = np.tile(SessionsU, len(SK))
    GTs = np.repeat(GenotypesOrder, len(np.unique(Sessions)))
    SK = [_ for s in SK for _ in s]

    Assi = SessionsU.tolist().index(int(AssemblySession))
    AssSK = [[Gen[Assi] for _ in Gen] for G,Gen in enumerate(AssSK)]
    AssSK = [_ for s in AssSK for _ in s]

    if len(AssSK)!=len(SK):
        AssSK += [[]]*(len(SK)-len(AssSK))

    ad,afc,afe,afa,afs,afg,aid = [],[],[],[],[],[],[]
    afes0 = []

    Break = False
    for si,s in enumerate(SK):
        NeuronNo = s.shape[1]
        ts = (s.shape[0]-Gap)//2
        Suc = s[:ts,:].argmax(axis=0)
        Err = s[ts+Gap:,:].argmax(axis=0)
        sSort = Err if SortBy=='Error' else Suc

        aad = np.concatenate((Suc,Err))
        aafc = np.array(['Success']*NeuronNo+['Error']*NeuronNo)
        aafe = np.tile([
            'Before' if _<(ts//2) else 'After'
            for _ in sSort
        ], 2)
        aaid = np.tile(np.arange(NeuronNo), 2)+(1000*(si//len(SessionsU)))

        if not SortPerSession:
            if Sessions[si] == 1: afes0 = dcp(aafe)
            else: aafe = dcp(afes0)

        try:
            AssSort = AssSK[si][1] if SortBy=='Error' else AssSK[si][0]
        except IndexError:
            N = f'PeakLat{Suf}{SPS}_AssemblySession{AssemblySession}'
            print(f'Not enough data for {N}!')
            Break = True
            break

        AssSort = [_ for _ in AssSort if len(_)>3]
        AssSort = sorted(AssSort, key=lambda x: len(x))

        aafa = [[a for a,A in enumerate(AssSort) if n in A] for n in range(NeuronNo)]
        aafa = np.array([f'{GTs[si]}_{max(_)}' if len(_) else f'{GTs[si]}_NaN' for _ in aafa])
        aafa = np.tile(aafa,2)

        # Convert time to s
        aad = Analysis.Normalize(aad,(-(ts-1),(ts-1)))/(ts-1)

        ad = np.concatenate((ad,aad))
        afc = np.concatenate((afc,aafc))
        afe = np.concatenate((afe,aafe))
        afa = np.concatenate((afa,aafa))
        aid = np.concatenate((aid,aaid))
        afs = np.concatenate((afs,[Sessions[si]]*aad.shape[0]))
        afg = np.concatenate((afg,[GTs[si]]*aad.shape[0]))

    if Break: return(None)

    afs = afs.astype(int).astype(str)
    MaskAll = np.ones(ad.shape,dtype=bool)
    MaskAssembly = np.array(['NaN' not in _ for _ in afa])

    afn = ['Assembly','Class','Gen','Session']
    afp = [False,True,False,True]

    for Set,Mask in (('_All',MaskAll), ('',MaskAssembly)):
        AOutCore = f'PeakLat{Suf}{SPS}_AssemblySession{AssemblySession}{Set}'

        Sad, Said = ad[Mask], aid[Mask]
        afx = [_[Mask] for _ in (afa,afc,afg,afs)]
        afl = [np.unique(el) for e,el in enumerate(afx)]

        Res = Stats.Overview(Sad, afx, afn)
        Res['Effect'] = {'Effect': np.array(afn)}
        IN = Res['IsNormal']
        Res['FXs'] = {}


        print(f'{AOutCore} Class:Gen:Session')
        safn = ['Class','Gen','Session']
        safp = [afp[afn.index(_)] for _ in safn]
        safx = [afx[afn.index(_)] for _ in safn]
        safnk = ':'.join(safn)
        Res['FXs'][safnk] = Stats.Effect(Sad, safx, Said, safp, IN, safn)
        for K,Key in Res['FXs'][safnk].items():
            Res['FXs'][safnk][K]['Assembly'] = np.array(
                ['All']*len(list(Key.values())[0])
            )


        print(f'{AOutCore} Assembly:Class:Session')
        safn = ['Assembly','Class','Session']
        safp = [afp[afn.index(_)] for _ in safn]
        safx = [afx[afn.index(_)] for _ in safn]
        safgbn = [_ for _ in afn if _ not in safn]
        safgb = [afx[afn.index(_)] for _ in safgbn]
        safnk = ':'.join(safn)

        S = Stats.Summary(Sad, [], safx, safn)
        SM = (np.isnan(S['n']))+(S['n']<3)
        AssM = S['Assembly'][~SM]
        aM = np.array([_ in AssM for _ in safx[safn.index('Assembly')]])

        sad, said = (_[aM] for _ in (Sad,Said))
        safx = [_[aM] for _ in safx]
        safgb = [_[aM] for _ in safgb]

        Res['FXs'][safnk] = Stats.Effect(sad, safx, said, safp, IN, safn, safgb, safgbn)


        print(f'{AOutCore} 2Ws')
        W2s = list(Stats.combinations(afn,2))
        W2s = [_ for _ in W2s if _!=tuple(sorted(('Assembly','Gen')))]
        W2s = [_ for _ in W2s if 'Assembly' in _]

        for safn in W2s:
            safp = [afp[afn.index(_)] for _ in safn]
            safx = [afx[afn.index(_)] for _ in safn]
            safgbn = [_ for _ in afn if _ not in safn]
            safgb = [afx[afn.index(_)] for _ in safgbn]
            safnk = ':'.join(safn)

            FacPairs = tuple(Stats.product(*[np.unique(_) for _ in safgb]))
            ARes = []

            for FC in FacPairs:
                i = np.prod([safgb[l]==L for l,L in enumerate(FC)], axis=0, dtype=bool)
                ssad = Sad[i]
                ssaid = Said[i]
                ssafx = [_[i] for _ in safx]

                S = Stats.Summary(ssad, [], ssafx, safn)
                SM = (np.isnan(S['n']))+(S['n']<3)
                AssM = S['Assembly'][~SM]
                aM = np.array([_ in AssM for _ in ssafx[safn.index('Assembly')]])

                ssad, ssaid = (_[aM] for _ in (ssad,ssaid))
                ssafx = [_[aM] for _ in ssafx]

                AR = Stats.Effect(ssad, ssafx, ssaid, safp, IN, safn)
                for K,Key in AR.items():
                    for fn,FN in enumerate(safgbn):
                        AR[K][FN] = np.array([FC[fn]]*len(list(Key.values())[0]))

                if 'p' in AR['Effect'].keys():
                    ARes.append(AR)

            Test = Stats.MergeDictList(ARes)
            for K,Key in Test.items():
                if type(Key)==list: continue
                if 'p' not in Key.keys(): continue

                EfU = np.unique(Key['Effect'])
                Test[K]['p.adj'] = np.empty(len(Key['p']))*np.nan

                for efu in EfU:
                    i = Key['Effect']==efu
                    Test[K]['p.adj'][i] = Stats.multipletests(Key['p'][i], method='holm')[1]

                Test[K] = {
                    k:Key[k] for k in Stats.OrderKeys(Key, list(safn)+safgbn)
                }

            Res['FXs'][safnk] = dcp(Test)


        print(f'{AOutCore} 1Ws Assembly')
        safn = ['Assembly']
        safp = [afp[afn.index(_)] for _ in safn]
        safx = [afx[afn.index(_)] for _ in safn]
        safgbn = [_ for _ in afn if _ not in safn]
        safgb = [afx[afn.index(_)] for _ in safgbn]
        safnk = ':'.join(safn)

        Res['FXs'][safnk] = Stats.Effect(Sad, safx, Said, safp, IN, safn, safgb, safgbn)

        print(f'{AOutCore} PWCs')
        Res['PWCs'] = {}
        for safn in afn:
            print(f'    {AOutCore} {safn}')
            safp = afp[afn.index(safn)]
            safx = afx[afn.index(safn)]
            safgbn = [_ for _ in afn if _ != safn]
            safgb = [afx[afn.index(_)] for _ in safgbn]
            Res['PWCs'][safn] = Stats.PairwiseComp(Sad, safx, afp, IN, safn, safgb, safgbn)


        AOutName = f"Stats-{AOutCore}-{'_'.join(sorted(afn))}-{Group}-{WindowName}"
        IO.Txt.Write(Res, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")
        IO.Bin.Write(
            dict(
                Data=Sad,
                FacNames=np.array(afn),
                FacPaired=np.array(afp),
                FXs=np.array(afx).T,
                Ids=Said
            ),
            f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data"
        )


        print(f'{AOutCore} Done.')


Gap = 5
IncludeSparse = (True,False)
SigOnly = (True,False)
SortPerSession = (True,False)
SortBy = ('Success','Error')
AssemblySession = ('1','2','6')

Combs = tuple(Stats.product(
    IncludeSparse, SigOnly, SortPerSession, SortBy, AssemblySession
))
Combs = tuple(((Gap,)+_ for _ in Combs))

print('Running...')
IO.MultiProcess(GetMatricesStats, Combs, os.cpu_count()-2, ASync=True)

# Test one by one for debugging
# for C,(G,IS,SO,SPS,SB,AS) in enumerate(Combs):
    # if C<24: continue
    # GetMatricesStats(G,IS,SO,SPS,SB,AS)
print('Done.')


print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Assemblies group resil CoMs gen session ========================
Group = 'MiniscopeL5PCandGqMC'
Sparse = False

if CodeTest: Sparse = True
SparseSuf = '_Sparse' if Sparse else ''

gGroups = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Groups.dat")[0]
gSessions = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Sessions.dat")[0]

ASessionsU = np.unique(gSessions[gGroups==Group]).astype(str)
ASessionsU = sorted(ASessionsU, key=lambda x: int(x))
SessionNo = len(ASessionsU)

All = {K:[] for K in (
    'CoMs','Areas','Resil','ResilID','Assemblies','Gens','Sessions'
)}

for G,Genotype in enumerate(GenotypesOrder):
    try:
        GCoMs = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/Matrices_SigOnly{SparseSuf}_MiniscopeL5PCandGqMC_{Genotype}_CoMs.dat", AsMMap=False)[0]
    except FileNotFoundError:
        print(f'Not enough data for {Genotype}!')
        continue

    SessionKeyAss = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/Matrices_SigOnly{SparseSuf}_MiniscopeL5PCandGqMC_{Genotype}_Assemblies", AsMMap=False)[0]

    SCombs = tuple(Stats.combinations(ASessionsU,len(ASessionsU)-1))[::-1]

    for Comb in SCombs:
        Session = [_ for _ in ASessionsU if _ not in Comb][0]
        SessionI = ASessionsU.index(Session)
        GCAss = SessionKeyAss[SessionI][0]
        Merged = sorted(GCAss, key=lambda x:len(x), reverse=True)
        Merged = [_ for _ in Merged if len(_)>3]

        # CoMs and Areas -----------------------------------------------
        Assi = np.unique([_ for m in Merged for _ in m])

        SGCoMs = dcp(GCoMs)
        SGCoMs = SGCoMs[:,Assi.astype(int)]

        AssCoMs, AssAreas = [], []
        for A,Assembly in enumerate(Merged):
            YCoMs = dcp(SGCoMs)
            Yi = [_ for _ in range(YCoMs.shape[1]) if Assi[_] in Assembly]
            YCoMs = YCoMs[:,Yi]
            YArea = Analysis.PolygonArea(YCoMs[0,:], YCoMs[1,:])

            AssCoMs.append(YCoMs)
            AssAreas.append(YArea)

        All['CoMs'].append(AssCoMs)
        All['Areas'].append(np.array(AssAreas))


        # Resilience ---------------------------------------------------
        AssNo = len(Merged)
        ResilF = np.ones((SessionNo,AssNo))
        ResilID = np.ones((SessionNo,AssNo))#, dtype=int)
        ResilID[SessionI,:] = range(AssNo)
        ResilIDFull = []

        for A,AssemblyRef in enumerate(Merged):
            PairI = [ASessionsU.index(_) for _ in Comb]

            df = [
                np.array([
                    AssemblyResilience(AssemblyRes, AssemblyRef)
                    for AssemblyRes in [
                        m for m in sorted(
                            SessionKeyAss[S][0], key=lambda x:len(x),
                            reverse=True
                        )
                        if len(m)>3
                    ]
                ])
                for S in PairI
            ]

            ResilID[PairI,A] = [
                np.nanargmax(v) if len(v) else np.nan for v in df
            ]
            ResilF[PairI,A] = [
                np.nanmax(v) if len(v) else np.nan for v in df
            ]

            ResilIDF = [
                [
                    _ for _ in sorted(
                        SessionKeyAss[s][0], key=lambda x:len(x), reverse=True
                    )
                    if len(_)>3
                ][int(ses)]
                if ~np.isnan(ses) else []
                for s,ses in enumerate(ResilID[:,A])
            ]
            ResilIDFull.append(ResilIDF)


        All['Gens'].append(Genotype)
        All['Sessions'].append(Session)
        All['Resil'].append(ResilF)
        All['ResilID'].append(ResilID)
        All['Assemblies'].append(ResilIDFull)

for K in ('Gens','Sessions'): All[K] = np.array(All[K])
All['SessionsU'] = np.array(ASessionsU)

IO.Bin.Write(All, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged-CoMsResilGenSession{SparseSuf}_{Group}")

print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Assemblies distance area stats =================================
Group = 'MiniscopeL5PCandGqMC'
AssemblySession = '6'
PlotType = ['Violins','Scatters']
Sparse = False

if CodeTest: Sparse = True
SparseSuf = '_Sparse' if Sparse else ''


All = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged-CoMsResilGenSession{SparseSuf}_{Group}", AsMMap=False)[0]

Data = np.array([_ for a in All['Areas'] for _ in a])
FGens = np.array([
    _ for A,Ass in enumerate(All['Areas'])
    for _ in [All['Gens'][A]]*Ass.shape[0]
])
FSes = np.array([
    _ for A,Ass in enumerate(All['Areas'])
    for _ in [All['Sessions'][A]]*Ass.shape[0]
])
IDs = np.arange(Data.shape[0])

FacPaired = (False,False)
FacNames = ('Gen','Session')
Facs = (FGens,FSes)
Res = Stats.Full(Data, Facs, IDs, FacPaired, 'auto', FacNames)

AOutName = f"Stats-AssemblyArea{SparseSuf}-{'_'.join(sorted(FacNames))}-{Group}-{WindowName}"

print(AOutName)

IO.Txt.Write(Res, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")
IO.Bin.Write(
    dict(
        Data=Data,
        FacNames=np.array(FacNames),
        FacPaired=np.array(FacPaired),
        FXs=np.array(Facs).T,
        IDs=IDs
    ),
    f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data"
)

print(Stats.GetFullReport(Res))

print('Done.')



#%% Cells describe stats ===============================================
Group = 'MiniscopeL5PCandGqMC'
Sparse = False

if CodeTest: Sparse = True
SparseSuf = '_Sparse' if Sparse else ''

Desc = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Describe")[0]
All = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged-CoMsResilGenSession{SparseSuf}_{Group}", AsMMap=False)[0]

ad, ar, afg, afs = [[] for _ in range(4)]

iGrD = Desc['Groups']==Group
SU = np.unique(All['Sessions'])

for G in GenotypesOrder:
    iG = All['Gens']==G
    iGD = Desc['Genotypes']==G

    for s,S in enumerate(SU):
        iS = All['Sessions']==S
        iSD = Desc['Sessions']==int(S)
        Ass = All['Assemblies'][np.where(iG*iS)[0][0]]
        Ass = [_[s] for _ in Ass]
        AssN = np.unique([_ for a in Ass for _ in a]).shape[0]
        Total = sum(Desc['TotalNo'][iGrD*iGD*iSD])

        afg += [G]*2
        afs += [S]*2
        ar += ['InAssembly', 'NotInAssembly']
        ad += [AssN, Total-AssN]

ad, ar, afg, afs = (np.array(_) for _ in (ad, ar, afg, afs))

EffG = Stats.McNemar(ad, [ar, afg], [afs], ['Session'])
EffS = Stats.McNemar(ad, [ar, afs], [afg], ['Gen'])

for S,Set in zip(('Gen','Session'), (EffG,EffS)):
    AOutName = f"Stats-CellsInAssembly-{S}_Assembly-{Group}-{WindowName}"
    IO.Txt.Write(Set, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")

FXs = np.array([ar,afg,afs]).T
afn = np.array(('Assembly','Gen','Session'))
Data = dict(Data=ad, FXs=FXs, FacNames=afn)
AOutName = f"Stats-CellsInAssembly-Gen_Session_Assembly-{Group}-{WindowName}"
IO.Bin.Write(Data, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data")

print(f"[{ScriptName}] All done.")



#%% Describe assemblies ================================================
Dict = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Assemblies-Merged", AsMMap=False)[0]
Dict['Assemblies_SigOnly'] = np.array(Dict['Assemblies_SigOnly'], dtype=object)

Report = []
for Group in GroupsMS:
    Report.append(f'Group: {Group}')
    iG = Dict['Groups']==Group
    AU = np.unique(Dict['Animals'][iG])

    for A in AU:
        iA = Dict['Animals']==A
        SU = np.unique(Dict['Sessions'][iG*iA])
        Gen = Dict['Genotypes'][iA][0]

        DL = []
        for S in SU:
            iS = Dict['Sessions']==S
            GAS = Dict['Assemblies_SigOnly'][iG*iA*iS]
            if len(GAS):
                GAS = [_ for _ in GAS[0] if len(_)>4]
                N = np.unique([_ for a in GAS for _ in a]).shape[0]
                D = len(GAS)
            else:
                D,N = 0

            DL.append(D)
            Report.append(f'{A} {Gen} Session {S}: {D} assemblies, {N} neurons')

        Report.append('-'*50)

    Report.append('='*50)

if CodeTest:
    Report.append('')
    Report.append('')
    Report.append('CodeTest is ENABLED! Those are random assemblies!')

for _ in Report: print(_)



#%% EOF ========================================================================
