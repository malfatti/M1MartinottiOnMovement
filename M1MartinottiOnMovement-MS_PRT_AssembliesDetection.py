#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2022
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""

ScriptName = 'Assemblies'

import numpy as np, os
from glob import glob
from sciscripts.IO import IO, Mat

RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
AnalysisPath = f"{RepoPath}/Examples/Analysis"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

ETAWindow = (-1, 1)
WindowName = f'Window_{int(ETAWindow[1]*1000)}'
MLab = '/usr/bin/matlab'



#%% Convert bin to .mat ========================================================
print(''); print('Converting to .mat...')
Group = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}")[0]
Folders = Group['Files']

for F,Folder in enumerate(Folders):
    print(Folder.replace(AnalysisPath+'/',''), '|', f'{F+1} of {len(Folders)}')
    MSFile = f"{Folder}/CNMFe/RawTraces_dF_F.dat"
    MSInfo = f"{Folder}/CaImAn.dict"

    try:
        Data = IO.Bin.Read(MSFile)[0]
    except FileNotFoundError:
        Data = IO.Bin.Read(f"{Folder}/RawTraces_dF_F.dat")[0]

    Info = IO.Txt.Read(MSInfo)
    SampleNo, NeuronNo = Data.shape
    Data = {'calcium_fluorescence': {'F': np.nan, 'F0': np.nan, 'dF_F': Data}}

    FPS = Info['CaImAn']['ParametersMotionCorrection']['fr']
    Data['topology'] = np.empty((NeuronNo,2))*np.nan
    Data['parameter'] = {
        'calcium_T1_2': 1.0,
        'dT_step': 1/FPS,
        'time_steps': SampleNo,
        'units': NeuronNo
    }

    Out = MSFile.replace('CNMFe','Assemblies')
    Out = Out.replace('.dat','_CALCIUM-FLUORESCENCE.mat')
    Mat.Write(Data, Out)


print('All done.')



#%% Pre-processing =============================================================
print(''); print('Pre-processing...')
Group = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}")[0]

MatFiles = [
    f"{_}/Assemblies/RawTraces_dF_F_CALCIUM-FLUORESCENCE.mat"
    for _ in Group['Files']
]

def PreProcess(MatFile):
    Name = '/'.join(MatFile.replace(AnalysisPath+'/','').split('/')[:-2])
    print(f"{Name} | Pre-processing...")
    Cmd = f"File = '{MatFile}'; CALCIUM_FLUORESCENCE_PROCESSING(File); quit"
    LogFile = f"{os.environ['HOME']}/.cache/{Name.replace('/','-')}.log"
    IO.RunProcess([MLab, '-nodesktop -nosplash -r', f'"{Cmd}"'], LogFile)
    print(f"{MatFile.replace(AnalysisPath,'')} | Done.")
    return(None)

IO.MultiProcess(PreProcess, [[_] for _ in MatFiles], Procs=4, ASync=True)
print('\nAll done.\n')



#%% Get assemblies - all algorithms ============================================
print(''); print('Getting assemblies...')
Group = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}")[0]

MatFiles = [
    f"{_}/Assemblies/RawTraces_dF_F_CALCIUM-FLUORESCENCE.mat"
    for _ in Group['Files']
]

def GetAssemblies(MatFile):
    Name = '/'.join(MatFile.replace(AnalysisPath+'/','').split('/')[:-2])
    Base = '_'.join(MatFile.split('_')[:-1])
    GetColl = False
    LogFile = f"{os.environ['HOME']}/.cache/{Name.replace('/','-')}.log"

    if not len(glob(f'{Base}_ICA-ASSEMBLIES.mat')):
        print(f"{Name} | ICA...")
        Out = f'{Base}_CALCIUM-FLUORESCENCE.mat'
        Cmd = f"File = '{Out}'; ICA_ASSEMBLY_DETECTION(File); quit"
        IO.RunProcess([MLab, '-nodesktop -nosplash -r', f'"{Cmd}"'], LogFile)
        GetColl = True
        print('')

    if not len(glob(f'{Base}_PROMAX-CS-ASSEMBLIES.mat')):
        print(f"{Name} | PROMAX-CS...")
        Out = f'{Base}_RASTER.mat'
        Cmd = f"File = '{Out}'; PROMAX_CS_ASSEMBLY_DETECTION(File); quit"
        IO.RunProcess([MLab, '-nodesktop -nosplash -r', f'"{Cmd}"'], LogFile)
        GetColl = True
        print('')

    if not len(glob(f'{Base}_PROMAX-MP-ASSEMBLIES.mat')):
        print(f"{Name} | PROMAX-MP...")
        Out = f'{Base}_RASTER.mat'
        Cmd = f"File = '{Out}'; PROMAX_MP_ASSEMBLY_DETECTION(File); quit"
        IO.RunProcess([MLab, '-nodesktop -nosplash -r', f'"{Cmd}"'], LogFile)
        GetColl = True
        print('')

    if not len(glob(f'{Base}_SVD-ASSEMBLIES.mat')):
        print(f"{Name} | SVD...")
        Out = f'{Base}_SPIKE-PROBABILITY-RASTER.mat'
        Cmd = f"File = '{Out}'; SVD_ASSEMBLY_DETECTION(File); quit"
        IO.RunProcess([MLab, '-nodesktop -nosplash -r', f'"{Cmd}"'], LogFile)
        GetColl = True
        print('')

    if GetColl:
        print(f"{Name} | Collection...")
        Out = f'{Base}_CALCIUM-FLUORESCENCE.mat'
        Cmd = f"File = '{Out}'; ASSEMBLIES_COLLECTION(File); quit"
        IO.RunProcess([MLab, '-nodesktop -nosplash -r', f'"{Cmd}"'], LogFile)
        print('')

    return(None)


IO.MultiProcess(GetAssemblies, [[_] for _ in MatFiles], Procs=4, ASync=True)
print('\nAll done.\n')



#%% EOF ========================================================================
