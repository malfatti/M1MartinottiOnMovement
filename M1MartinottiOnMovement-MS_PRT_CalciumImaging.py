#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2023
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""


ScriptSuf = 'PRT'
ScriptName = f'M1MartinottiOnMovement_{ScriptSuf}'

print(f'[{ScriptName}] Importing dependencies...')
import gc, numpy as np, os, shutil, sys
from copy import deepcopy as dcp
from glob import glob
from scipy.signal import find_peaks, peak_widths
from sciscripts.Analysis import Analysis, Stats
from sciscripts.IO import IO
from sciscripts.IO.IO import MultiProcess

RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data"
AnalysisPath = f"{RepoPath}/Examples/Analysis"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

sys.path.insert(len(sys.path), RepoPath)
import M1MartinottiOnMovement_Core as Core

ETAWindow = Core.ETAWindow
WindowName = Core.WindowName

GenotypesOrder = ('WT', 'Cre+')
GroupsMS = ('MiniscopeL3PCandGqMC', 'MiniscopeL5MC', 'MiniscopeL5PCandGqMC')

GroupInfoMS = IO.Txt.Read(f'{DataPath}/MiniscopeL3_L5-AnimalExperimentalOrder.dict')



print(f'[{ScriptName}] Done.')



#%% [PRT_MS-dF] ETAs ===========================================================
print(f'[{ScriptName}] Gathering data info...')
Group = IO.Bin.Read(f"{AnalysisPath}/PRT_MCSEDCEM")[0]

FMatched = [
    F
    for F,File in enumerate(Group['MSFiles'])
    if len(glob(
        f"{AnalysisPath}/{Group['Groups'][F]}/{Group['Animals'][F]}-{ScriptSuf}-Session{Group['Sessions'][F]}_{Group['Trials'][F]+1}/Reaches"
    ))
]

MSFiles = Group['MSFiles'][FMatched]
Group = {k:v[FMatched] for k,v in Group.items()}

MiniscopeFiles = np.array([f"{AnalysisPath}/{_}" for _ in MSFiles])

for MF,MFile in enumerate(MiniscopeFiles):
    SessionPath = f"{AnalysisPath}/{Group['Groups'][MF]}/{Group['Animals'][MF]}-{ScriptSuf}-Session{Group['Sessions'][MF]}_{Group['Trials'][MF]+1}/Imaging"
    ReachesFile = SessionPath.replace('Imaging','Reaches')
    print(f"{SessionPath.replace(AnalysisPath,'')[1:]} - File {MF+1} of {len(MiniscopeFiles)}...")

    if not len(glob(ReachesFile)):
        print('No reaches detected for this experiment.')
        continue

    print(''); print(f'    [{ScriptName}] Loading Analysis results...')
    try:
        RawTraces = IO.Bin.Read(f'{MFile}/CNMFe/RawTraces_dF_F.dat', AsMMap=False)[0]
    except FileNotFoundError:
        RawTraces = IO.Bin.Read(f'{MFile}/RawTraces_dF_F.dat', AsMMap=False)[0]

    try:
        Time = IO.Bin.Read(f'{MFile}/Time.dat', AsMMap=False)[0]
    except FileNotFoundError:
        Time = IO.Bin.Read(f'{MFile}/Time', AsMMap=False)[0]
        TK = [K for K in Time.keys() if 'Miniscope' in K]
        if len(TK)>1:
            raise LookupError('There should be only one miniscope!')
        Time = Time[TK[0]]

    vInfo = IO.Txt.Read(f'{MFile}/CaImAn.dict')
    RateAvg = vInfo['CaImAn']['ParametersMotionCorrection']['fr']

    TimeStart = np.where((Time >= 0))[0][0]
    Time -= Time[TimeStart]    # remove offset

    print(''); print(f'    [{ScriptName}] Get TTLs from behavior...')
    Reaches = IO.Bin.Read(ReachesFile, AsMMap=False)[0]
    ReachesClass = Reaches['ReachesClass'].copy()
    ReachesClassSimple = np.array([_ if _ == 'Success' else 'Error' for _ in ReachesClass])

    TTLs = np.array([
        abs(Time-_).argmin() for _ in Reaches['Time'][Reaches['TTLsSec']]
    ])

    if len(TTLs):
        IO.Bin.Write(TTLs, f'{SessionPath}/ReachesTS.dat')
        if not len(glob(f'{SessionPath}/ETAs_{WindowName}.dat')):
            print(''); print(f'    [{ScriptName}] Getting Event-Triggered average (ETA)...')
            ETAs = Analysis.Slice(
                RawTraces, TTLs,
                [round(ETAWindow[0]*RateAvg), round(ETAWindow[1]*RateAvg)]
            )
            ETATime = np.arange(ETAWindow[0], ETAWindow[1], np.ptp(ETAWindow)/ETAs.shape[0])

            IO.Bin.Write(ETAs, f'{SessionPath}/ETAs_{WindowName}.dat')
            IO.Bin.Write(ETATime, f'{SessionPath}/ETATime_{WindowName}.dat')
            IO.Bin.Write(TTLs, f'{SessionPath}/ReachesTS.dat')
        else:
            print(f'    [{ScriptName}] ETAs already done.')

        if not len(glob(f'{SessionPath}/Accepted.dat')):
            try:
                shutil.copy(f'{MFile}/CNMFe/Accepted.dat', f'{SessionPath}/Accepted.dat')
                shutil.copy(f'{MFile}/CNMFe/Accepted-Info.dict', f'{SessionPath}/Accepted-Info.dict')
            except FileNotFoundError:
                shutil.copy(f'{MFile}/Accepted.dat', f'{SessionPath}/Accepted.dat')
                shutil.copy(f'{MFile}/Accepted-Info.dict', f'{SessionPath}/Accepted-Info.dict')


GroupInfoMS = IO.Txt.DictListsToArrays(GroupInfoMS)
Group['Genotypes'] = np.array([
    GroupInfoMS['Genotype'][GroupInfoMS['Animals']==_][0]
    for _ in Group['Animals']
])
Group['Files'] = MiniscopeFiles
IO.Bin.Write(Group, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}")

print(f'[{ScriptName}] Done.')



#%% [PRT_MS-dF] ETAs Assign neuron identities through sessions =================
AnimalsMerged = sorted(glob(f'{AnalysisPath}/*-Matched'))
Group = {_: [] for _ in (
    'Animals', 'Sessions', 'Groups', 'Classes', 'Files',
    'CoMs', 'CellAreas', f'ETAs_{WindowName}'
)}
ReRun = []

for A,AnimalMerged in enumerate(AnimalsMerged):
    ASessions = IO.Bin.Read(f"{AnimalMerged}/Sessions.dat")[0]
    ATrials = IO.Bin.Read(f"{AnimalMerged}/Trials.dat")[0]
    AAnimal = AnimalMerged.split('/')[-1].split('-')[0]
    AGroup = AAnimal.split('_')[0]

    Assignments = IO.Bin.Read(f"{AnimalMerged}/AssignmentsActive.dat")[0]
    Areas = IO.Bin.Read(f"{AnimalMerged}/CellArea.dat")[0]
    CoMs = IO.Bin.Read(f"{AnimalMerged}/CoMActive.dat")[0]

    if not Assignments.size:
        print('    No neurons active on all sessions! Re-run session alignment for this animal.')
        print()
        ReRun.append(AnimalMerged)
        continue


    print(f"{AAnimal}, animal {A+1} of {len(AnimalsMerged)}")

    Base = f"{AnalysisPath}/{AGroup}/{AAnimal}-{ScriptSuf}"

    ETAs = [
        IO.Bin.Read(f"{Base}-Session{Session}_{ATrials[S]+1}/Imaging/ETAs_{WindowName}.dat")[0]
        if len(glob(f"{Base}-Session{Session}_{ATrials[S]+1}/Imaging/ETAs_{WindowName}.dat"))
        else np.array([])
        for S,Session in enumerate(ASessions)
    ]

    Accepted = [
        IO.Bin.Read(f"{Base}-Session{Session}_{ATrials[S]+1}/Imaging/Accepted.dat")[0]
        if len(glob(f"{Base}-Session{Session}_{ATrials[S]+1}/Imaging/Accepted.dat"))
        else np.array([])
        for S,Session in enumerate(ASessions)
    ]

    AsAc = np.array([[n in Acc for n in Assignments[:,S]] for S,Acc in enumerate(Accepted)]).T
    AsAc = AsAc.sum(axis=1)>0
    Assignments = Assignments[AsAc,:]

    if not Assignments.size:
        print('    No accepted neurons active on at least one session! Re-run session alignment and/or component evaluation for this animal.')
        print()
        ReRun.append(AnimalMerged)
        continue

    Areas = Areas[AsAc]
    CoMs = CoMs[AsAc,:]

    ETAs = [
        ETA[:,:,Assignments[:,S]]
        if ETA.size else np.array([])
        for S,ETA in enumerate(ETAs)
    ]

    AReachesClass = [
        IO.Bin.Read(
            f"{Base}-Session{Session}_{ATrials[S]+1}/Reaches/ReachesClass.dat"
        )[0]
        if len(glob(f"{Base}-Session{Session}_{ATrials[S]+1}/Reaches/ReachesClass.dat"))
        else np.array([])
        for S,Session in enumerate(ASessions)
    ]

    AReachesClass = [np.array([_ if _ == 'Success' else 'Error' for _ in RC]) for RC in AReachesClass]

    if (
            len([_.shape for _ in ETAs if _.size]) == 0
            and len([_.shape for _ in AReachesClass if _.size]) == 0
        ):
        print('    No reaches for this animal!'); print()
        continue


    Empty = list([_.shape for _ in ETAs if _.size][0])
    Empty[1] = 0
    ETAsSuc, ETAsErr = (
        [
            ETA[:,AReachesClass[S]==Key,:]
            if ETA.size else np.empty(Empty)
            for S,ETA in enumerate(ETAs)
        ]
            if len([
                ETA[:,AReachesClass[S]==Key,:]
                for S,ETA in enumerate(ETAs)
                if ETA.size
            ])
            else np.array([])
        for Key in ('Success', 'Error')
    )

    ASessionsU = np.unique(ASessions)
    ETAsF = {C:
        [
            np.concatenate(
                [ETA for E,ETA in enumerate(Class) if ASessions[E]==S],
                axis=1
            ) for S in ASessionsU
        ]
        for C,Class in (('Success',ETAsSuc), ('Error',ETAsErr))
    }

    for Class in ('Success','Error'):
        Group['Animals'] += [AAnimal]*len(ASessionsU)
        Group['Files'] += [AnimalMerged]*len(ASessionsU)
        Group['Groups'] += [AGroup]*len(ASessionsU)
        Group['Classes'] += [Class]*len(ASessionsU)
        Group['Sessions'] += ASessionsU.tolist()
        Group['CellAreas'] += [Areas]*len(ASessionsU)
        Group['CoMs'] += [CoMs]*len(ASessionsU)
        Group[f'ETAs_{WindowName}'] += ETAsF[Class]

    print('Done.'); print()


GroupInfoMS = IO.Txt.DictListsToArrays(GroupInfoMS)
Group['Genotypes'] = np.array([
    GroupInfoMS['Genotype'][GroupInfoMS['Animals']==_][0]
    for _ in Group['Animals']
])
Group['Files'] = [_.replace(f'{AnalysisPath}/','') for _ in Group['Files']]
for K in ('Animals', 'Groups', 'Files', 'Sessions', 'Classes'):
    Group[K] = np.array(Group[K])

Out = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged"
ToDel = [f"{Out}/{_}*" for _ in Group.keys()]
for F in ToDel:
    for f in glob(F):
        try: shutil.rmtree(f)
        except NotADirectoryError: os.remove(f)
        except FileNotFoundError: pass

IO.Bin.Write(Group, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged")

print('All done.')



#%% [PRT_MS-dF] Single neuron statistics =======================================
ClassesOrder = ('Success', 'Error')

GAnimals, GGenotypes, GGroups, GSessions, GClasses = (
    IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat")[0]
    for _ in ('Animals', 'Genotypes', 'Groups', 'Sessions', 'Classes')
)

ETAs = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/ETAs_{WindowName}")[0]

Eff = {K: [] for K in ('ps', 'BL', 'Res')}

for Gi, ETA in enumerate(ETAs):
    print(f"[{ScriptName}] {Gi+1} of {len(ETAs)}...")

    if not ETA.size:
        Eff['ps'].append(np.array([]))
        Eff['BL'].append(np.array([]))
        Eff['Res'].append(np.array([]))
        continue

    ETA = ETA.mean(axis=1)
    Time = np.arange(ETAWindow[0], ETAWindow[1], np.ptp(ETAWindow)/ETA.shape[0])
    Baseline = Time < 0

    def RRun(L):
        N,ETA,BLN = L
        Y = ETA[:,N]
        f = np.array(['A']*BLN.shape[0])
        f[BLN]='B'
        i = np.arange(f.shape[0])

        if np.all(ETA==0):
            d = [1.0, 0.0, 0.0, N]
        else:
            Result = Stats.Effect(Y, [f], i, [False])

            d = [
                Result['Effect']['p'][0],
                Y[BLN].mean(),
                Y[~BLN].mean(),
                N
            ]

        return(d)

    Args = [[(_,ETA,Baseline)] for _ in range(ETA.shape[1])]
    d = IO.MultiProcess(RRun, Args, os.cpu_count()-1, ASync=True)
    if [_[-1] for _ in d] != list(range(len(d))):
        raise LookupError('Wrong indexing in MultiProcess!')
    d = np.array(d)

    Eff['ps'].append(np.array(d[:,0]))
    Eff['BL'].append(np.array(d[:,1]))
    Eff['Res'].append(np.array(d[:,2]))


Out = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/Stats-dF-Epoch_EachNeuron-{WindowName}"
try: shutil.rmtree(Out)
except FileNotFoundError: pass
IO.Bin.Write(Eff, Out)

print(f'[{ScriptName}] All done.')



#%% [PRT_MS-dF] ETAs statistics ================================================
def GetETAsStats(Sparse, SigOnly):
    Keys = ('Success', 'Error')
    Epochs = ('Before', 'After')
    FacNames = ('Class', 'Epoch', 'Gen', 'Session')
    FacPaired = (True,True,False,True)

    GAnimals, GGenotypes, GGroups, GSessions, GClasses = (
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat")[0]
        for _ in ('Animals', 'Genotypes', 'Groups', 'Sessions', 'Classes')
    )

    GEff = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/Stats-dF-Epoch_EachNeuron-{WindowName}")[0]
    GEff = {k: np.array(IO.Bin.DictToList(v), dtype=object) for k,v in GEff.items()}

    Classes = [_ if _ else 'All' for _ in Keys]

    if SigOnly:
        for an in np.unique(GAnimals):
            for cl in np.unique(GClasses):
                ancli = np.prod([
                    s == e
                    for s,e in zip((GAnimals,GClasses),(an,cl))
                ], axis=0).astype(bool)

                anclp = GEff['ps'][ancli].tolist()
                if True in [_.size==0 for _ in anclp]:
                    ThisShape = [_.shape for _ in anclp if len(_)]
                    if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                        ThisShape = ThisShape[0]
                        anclp = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in anclp]

                anclp = np.sum(np.array(anclp)<0.05,axis=0).astype(bool)

                for ses in np.where(ancli)[0]:
                    for K in GEff.keys():
                        if GEff[K][ses].size:
                            GEff[K][ses] = GEff[K][ses][anclp]


    for Group in GroupsMS:
        ThisGroup = GGroups==Group
        SufSO = '_SigOnly' if SigOnly else ''
        SufSparse = '_Sparse' if Sparse else ''
        AOutName = f"Stats-dF{SufSO}{SufSparse}-Class_Epoch_Gen_Session-{Group}-{WindowName}"

        print(f'{Group} dF{SufSO}{SufSparse}...')
        print('')

        GpsS = [_.shape[0] for _ in GEff['ps'][ThisGroup]]

        if not len(GpsS):
            print(f'No data for {AOutName}.')
            print('')
            continue

        ps = np.array([_ for el in GEff['ps'][ThisGroup] for _ in el])
        BL = np.array([_ for el in GEff['BL'][ThisGroup] for _ in el])
        Res = np.array([_ for el in GEff['Res'][ThisGroup] for _ in el])

        psClass, psGen, psSession = (
            np.array([
                _ for s,Sh in enumerate(GpsS)
                for _ in [Set[ThisGroup][s]]*Sh
            ])
            for Set in (GClasses, GGenotypes, GSessions)
        )

        psId = np.array([
            _ for s,Sh in enumerate(GpsS)
            for _ in (f'{GAnimals[ThisGroup][s]}_{n}' for n in range(Sh))
        ])

        AData = np.concatenate((BL, Res))
        AEpoch = np.repeat(Epochs, BL.shape[0])
        AClass, AGen, AId, ASes = (
            np.tile(_,2) for _ in (psClass, psGen, psId, psSession)
        )

        if not Sparse:
            AIdU, ACU, AEU, ASU = (np.unique(_) for _ in (AId,AClass,AEpoch,ASes))
            AIdUFull = np.array([
                False not in (
                    ASes[(AId==N)*(AClass==C)*(AEpoch==E)].shape[0]==ASU.shape[0]
                    for C in ACU for E in AEU
                )
                for N in AIdU
            ])

            AIdFull = np.array([_ in AIdU[AIdUFull] for _ in AId])

            AData, AClass, AEpoch, AGen, ASes, AId = (
                _[AIdFull] for _ in (AData, AClass, AEpoch, AGen, ASes, AId)
            )

        if len(AData)==0:
            print(f'No data for {AOutName}.')
            print('')
            continue

        AFxs = dict(
            Session = ASes,
            Gen = AGen,
            Class = AClass,
            Epoch = AEpoch
        )

        ThisFacPaired = [_ for _ in FacPaired]
        ThisFacNames = [_ for _ in FacNames]

        EffData = dict(
            Data = AData,
            FXs = np.array([AFxs[_] for _ in FacNames]).T,
            Ids = AId,
            FacNames = np.array(FacNames),
            FacPaired = np.array(FacPaired)
        )

        Eff = Stats.Full(
            AData, [AFxs[_] for _ in ThisFacNames], AId,
            ThisFacPaired, 'auto', ThisFacNames
        )

        IO.Txt.Write(Eff, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")
        IO.Bin.Write(EffData, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data")


Sparse = (True,False)
SigOnly = (True,False)
Combs = tuple(Stats.product(Sparse, SigOnly))

print(f"[{ScriptName}] Running...")
IO.MultiProcess(GetETAsStats, Combs, os.cpu_count()-2, ASync=True)
print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Latency Matrices CoMs ==========================================
def GetMatrices(Gap, IncludeSparse, SigOnly, SortBy):
    Keys = ('Success', 'Error')
    Norm = True
    FName = 'Matrices'
    if SigOnly: FName += '_SigOnly'
    if IncludeSparse: FName += '_Sparse'
    if SortBy == 'Error': FName += '_SortByError'

    GAnimals, GClasses, GGenotypes, GGroups, GSessions = (
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat")[0]
        for _ in ('Animals', 'Classes', 'Genotypes', 'Groups', 'Sessions')
    )

    GETAs = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/ETAs_{WindowName}")[0]
    GCoMs = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/CoMs")[0]
    GAreas = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/CellAreas")[0]
    GEff = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/Stats-dF-Epoch_EachNeuron-{WindowName}")[0]
    GEff = {K: np.array(V, dtype=object) for K,V in GEff.items()}

    if SigOnly:
        for an in np.unique(GAnimals):
            anclp = GEff['ps'][GAnimals==an].tolist()
            if True in [_.size==0 for _ in anclp]:
                ThisShape = [_.shape for _ in anclp if len(_)]
                if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                    ThisShape = ThisShape[0]
                    anclp = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in anclp]

            anclp = np.sum(np.array(anclp)<0.05,axis=0).astype(bool)

            for ses in np.where(GAnimals==an)[0]:
                for K in GEff.keys():
                    if GEff[K][ses].size:
                        GEff[K][ses] = GEff[K][ses][anclp]

                if GETAs[ses].size: GETAs[ses] = GETAs[ses][:,:,anclp]


    for Group in GroupsMS:
        for G,Genotype in enumerate(GenotypesOrder):
            AnimalsU = np.unique(GAnimals[(GGroups==Group)*(GGenotypes==Genotype)])
            if not len(AnimalsU): continue

            SessionKeyAnimal = [[] for _ in AnimalsU]
            SessionKeyAnimalAll = [[] for _ in AnimalsU]
            SessionKeyAnimalCoMs = [[] for _ in AnimalsU]
            SessionKeyAnimalAllCoMs = [[] for _ in AnimalsU]
            SessionKeyAnimalAreas = [[] for _ in AnimalsU]
            SessionKeyAnimalAllAreas = [[] for _ in AnimalsU]

            for A,Animal in enumerate(AnimalsU):
                SessionsU =  np.unique(GSessions[(GGroups==Group)*(GAnimals==Animal)])
                SessionKey = [[] for _ in SessionsU]
                SessionKeyCoMs = [[] for _ in SessionsU]
                SessionKeyAreas = [[] for _ in SessionsU]
                if not IncludeSparse: SkipAnimal = False

                for S,Session in enumerate(SessionsU):
                    if not IncludeSparse:
                        if SkipAnimal: continue

                    ThisGAS = [[] for _ in Keys]
                    ThisCoMs = [[] for _ in Keys]
                    ThisAreas = [[] for _ in Keys]
                    for RC,Class in enumerate(Keys):
                        print(Genotype,Animal,Session,Class)

                        ThisI = np.where(
                            (GAnimals==Animal)*(GSessions==Session)*
                            (GGroups==Group)*(GClasses==Class)
                        )[0]

                        if len(ThisI) != 1:
                            raise IndexError('There should be exactly one recording for each animal at each session.')

                        ThisI = ThisI[0]


                        # Latencies ------------------------------------
                        ETAs = GETAs[ThisI]

                        if not ETAs.size:
                            # No reaches or no neurons with sig. changes
                            continue

                        ThisGAS[RC] = ETAs.mean(axis=1)


                        # CoMs -----------------------------------------
                        CoMs = GCoMs[ThisI]
                        if not CoMs.size:
                            raise IndexError('There should be exactly one recording for each animal at each session.')

                        ThisCoMs[RC] = dcp(CoMs).T

                        # Areas ----------------------------------------
                        Areas = GAreas[ThisI]
                        if not Areas.size:
                            raise IndexError('There should be exactly one recording for each animal at each session.')

                        ThisAreas[RC] = dcp(Areas).T


                    # Latencies ----------------------------------------
                    if False in [len(_)>0 for _ in ThisGAS]:
                        if IncludeSparse:
                            ThisShape = [_.shape for _ in ThisGAS if len(_)]
                            if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                                ThisShape = ThisShape[0]
                                ThisGAS = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in ThisGAS]
                        else:
                            SkipAnimal = True
                            print(f'No reaches! {[len(_) for _ in ThisGAS]}')
                            continue


                    SessionKey[S] = dcp(ThisGAS)


                    # CoMs ---------------------------------------------
                    if not len(ThisCoMs):
                        raise LookupError('There should be at least 1 item in each list.')

                    SessionKeyCoMs[S] = dcp(ThisCoMs)
                    IO.Bin.Write(ThisCoMs[0], f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/{FName}_{Group}_{Genotype}_CoMs/Session_{Session}.dat")


                    # Areas --------------------------------------------
                    if not len(ThisAreas):
                        raise LookupError('There should be at least 1 item in each list.')

                    SessionKeyAreas[S] = dcp(ThisAreas)
                    IO.Bin.Write(ThisAreas[0], f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/{FName}_{Group}_{Genotype}_Areas/Session_{Session}.dat")


                # Latencies --------------------------------------------
                if False in [len(_)>0 for _ in SessionKey]:
                    if IncludeSparse:
                        raise LookupError('There should be at least 1 item in each list.')
                    else:
                        continue

                if IncludeSparse:
                    if False in (len(_)>0 for s in SessionKey for _ in s):
                        ThisShape = [_.shape for s in SessionKey for _ in s if len(_)]
                        if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                            ThisShape = ThisShape[0]
                            SessionKey = [
                                [
                                    _ if len(_)>0 else np.empty(ThisShape)*np.nan
                                    for _ in s
                                ]
                                for s in SessionKey
                            ]

                SessionKeyAnimal[A] = dcp(SessionKey)

                SessionKeyAll = [[] for _ in Keys]
                for RC,Class in enumerate(Keys):
                    ThisAll = np.where(
                        (GAnimals==Animal)*(GGroups==Group)*(GClasses==Class)
                    )[0]
                    ETAsAll = [GETAs[e] for e in ThisAll]

                    if False in [len(_)>0 for _ in ETAsAll]:
                        ThisShape = [(_.shape[0], 0, _.shape[2]) for _ in ETAsAll if len(_)]
                        if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                            ThisShape = ThisShape[0]
                            ETAsAll = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in ETAsAll]

                    if False in [len(_)>0 for _ in ETAsAll]:
                        SessionKeyAll[RC] = []
                    else:
                        ETAsAll = np.concatenate(ETAsAll, axis=1)
                        SessionKeyAll[RC] = ETAsAll.mean(axis=1)

                if False in [len(_)>0 for _ in SessionKeyAll]:
                    if IncludeSparse:
                        ThisShape = [_.shape for _ in SessionKeyAll if len(_)]
                        if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                            ThisShape = ThisShape[0]
                            SessionKeyAll = [_ if len(_)>0 else np.empty(ThisShape)*np.nan for _ in SessionKeyAll]
                    else:
                        SkipAnimal = True
                        continue


                SessionKeyAnimalAll[A] = dcp(SessionKeyAll)


                # CoMs -------------------------------------------------
                if False in [len(_)>0 for _ in SessionKeyCoMs]:
                    raise LookupError('There should be at least 1 item in each list.')

                if IncludeSparse:
                    for skc,SKC in enumerate(SessionKeyCoMs):
                        if False in (len(_)>0 for _ in SKC) and True in (len(_)>0 for _ in SKC):
                            TI = [e for e,el in enumerate(SKC) if len(el)][0]
                            TO = [e for e,el in enumerate(SKC) if not len(el)][0]
                            SessionKeyCoMs[skc][TO] = SessionKeyCoMs[skc][TI]

                    if False in (len(_)>0 for s in SessionKeyCoMs for _ in s):
                        ThisShape = [_.shape for s in SessionKeyCoMs for _ in s if len(_)]
                        if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                            ThisShape = ThisShape[0]
                            SessionKeyCoMs = [
                                [
                                    _ if len(_)>0 else np.empty(ThisShape)*np.nan
                                    for _ in s
                                ]
                                for s in SessionKeyCoMs
                            ]
                else:
                    ThisGSShapeCoMs = [_.shape for a in SessionKeyCoMs for _ in a][0]
                    SessionKeyCoMs = [
                        [_ if len(_) else np.empty(ThisGSShapeCoMs)*np.nan for _ in Session]
                        for Session in SessionKeyCoMs
                    ]

                SessionKeyAnimalCoMs[A] = dcp(SessionKeyCoMs)


                # Areas ------------------------------------------------
                if False in [len(_)>0 for _ in SessionKeyAreas]:
                    raise LookupError('There should be at least 1 item in each list.')

                if IncludeSparse:
                    for skc,SKC in enumerate(SessionKeyAreas):
                        if False in (len(_)>0 for _ in SKC) and True in (len(_)>0 for _ in SKC):
                            TI = [e for e,el in enumerate(SKC) if len(el)][0]
                            TO = [e for e,el in enumerate(SKC) if not len(el)][0]
                            SessionKeyAreas[skc][TO] = SessionKeyAreas[skc][TI]

                    if False in (len(_)>0 for s in SessionKeyAreas for _ in s):
                        ThisShape = [_.shape for s in SessionKeyAreas for _ in s if len(_)]
                        if len(ThisShape)>0 and np.unique(ThisShape, axis=0).shape[0]==1:
                            ThisShape = ThisShape[0]
                            SessionKeyAreas = [
                                [
                                    _ if len(_)>0 else np.empty(ThisShape)*np.nan
                                    for _ in s
                                ]
                                for s in SessionKeyAreas
                            ]
                else:
                    ThisGSShapeAreas = [_.shape for a in SessionKeyAreas for _ in a][0]
                    SessionKeyAreas = [
                        [_ if len(_) else np.empty(ThisGSShapeAreas)*np.nan for _ in Session]
                        for Session in SessionKeyAreas
                    ]

                SessionKeyAnimalAreas[A] = dcp(SessionKeyAreas)


            # Latencies ------------------------------------------------
            if not True in [len(_)>0 for _ in SessionKeyAnimal]: continue
            if not True in [len(_)>0 for _ in SessionKeyAnimalAll]: continue

            SessionKeyAnimal = [_ for _ in SessionKeyAnimal if len(_)]
            if IncludeSparse:
                ThisShape = max([len(_) for _ in SessionKeyAnimal])
                SessionKeyAnimal = [_ for _ in SessionKeyAnimal if len(_)==ThisShape]

            ANo, SNo, KNo = len(SessionKeyAnimal), len(SessionKeyAnimal[0]), len(SessionKeyAnimal[0][0])
            print(ANo, SNo, KNo)
            SessionKey = [
                [
                    np.concatenate(
                        [SessionKeyAnimal[A][S][K] for A in range(ANo)],
                        axis=1
                    )
                    for K in range(KNo)
                ]
                for S in range(SNo)
            ]

            SessionKeyAnimalAll = [_ for _ in SessionKeyAnimalAll if len(_)]
            ANo, KNo = len(SessionKeyAnimalAll), len(SessionKeyAnimalAll[0])
            SessionKeyAll = [
                np.concatenate(
                    [SessionKeyAnimalAll[A][K] for A in range(ANo)],
                    axis=1
                )
                for K in range(KNo)
            ]

            DataNO = SessionKey[0][Keys.index(SortBy)]
            if Norm: DataNO = Analysis.Normalize(DataNO)
            DataNO[np.isnan(DataNO)] = 0
            NeuronOrder = np.argsort(DataNO.argmax(axis=0))

            for S in range(SNo):
                ThisGS = [_ for s in SessionKey[S] for _ in (s[:,NeuronOrder],np.empty((Gap,s.shape[1]))*np.nan)]
                ThisGS = ThisGS[:-1]
                ThisGS = np.concatenate(ThisGS)
                ThisGS = Analysis.Normalize(ThisGS) if Norm else ThisGS
                SessionKey[S] = dcp(ThisGS)

            DataNO = SessionKeyAll[Keys.index(SortBy)]
            if Norm: DataNO = Analysis.Normalize(DataNO)
            DataNO[np.isnan(DataNO)] = 0
            NeuronOrderAll = np.argsort(DataNO.argmax(axis=0))
            ThisG = [_ for s in SessionKeyAll for _ in (s[:,NeuronOrderAll],np.empty((Gap,s.shape[1]))*np.nan)]
            ThisG = ThisG[:-1]
            ThisG = np.concatenate(ThisG)
            ThisG = Analysis.Normalize(ThisG) if Norm else ThisG
            SessionKeyAll = dcp(ThisG)

            aout = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/{FName}_{Group}_{Genotype}"
            try: shutil.rmtree(aout)
            except FileNotFoundError: pass
            IO.Bin.Write(SessionKey, aout)

            IO.Bin.Write(SessionKeyAll, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/{FName}AllSessions_{Group}_{Genotype}.dat")


            # CoMs -----------------------------------------------------
            if not True in [len(_)>0 for _ in SessionKeyAnimalCoMs]:
                raise LookupError('There should be at least 1 item in each list.')

            SessionKeyAnimalCoMs = [_ for _ in SessionKeyAnimalCoMs if len(_)]
            if IncludeSparse:
                ThisShape = max([len(_) for _ in SessionKeyAnimalCoMs])
                SessionKeyAnimalCoMs = [_ for _ in SessionKeyAnimalCoMs if len(_)==ThisShape]

            ANo, SNo, KNo = len(SessionKeyAnimalCoMs), len(SessionKeyAnimalCoMs[0]), len(SessionKeyAnimalCoMs[0][0])
            SessionKeyCoMs = [
                [
                    np.concatenate(
                        [SessionKeyAnimalCoMs[A][S][K] for A in range(ANo)],
                        axis=1
                    )
                    for K in range(KNo)
                ]
                for S in range(SNo)
            ]

            SessionKeyCoMs = SessionKeyCoMs[0][0][:,NeuronOrder]

            IO.Bin.Write(SessionKeyCoMs, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/{FName}_{Group}_{Genotype}_CoMs.dat")


            # Areas ----------------------------------------------------
            if not True in [len(_)>0 for _ in SessionKeyAnimalAreas]:
                raise LookupError('There should be at least 1 item in each list.')

            SessionKeyAnimalAreas = [_ for _ in SessionKeyAnimalAreas if len(_)]
            if IncludeSparse:
                ThisShape = max([len(_) for _ in SessionKeyAnimalAreas])
                SessionKeyAnimalAreas = [_ for _ in SessionKeyAnimalAreas if len(_)==ThisShape]

            ANo, SNo, KNo = len(SessionKeyAnimalAreas), len(SessionKeyAnimalAreas[0]), len(SessionKeyAnimalAreas[0][0])
            SessionKeyAreas = [
                [
                    np.concatenate(
                        [SessionKeyAnimalAreas[A][S][K] for A in range(ANo)],
                        axis=0
                    )
                    for K in range(KNo)
                ]
                for S in range(SNo)
            ]

            SessionKeyAreas = SessionKeyAreas[0][0][NeuronOrder]

            IO.Bin.Write(SessionKeyAreas, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/{FName}_{Group}_{Genotype}_Areas.dat")

    print('Done.')


Gap = 5
IncludeSparse = (True,False)
SigOnly = (True,False)
SortBy = ('Success','Error')

Combs = tuple(Stats.product(
    IncludeSparse, SigOnly, SortBy
))
Combs = tuple(((Gap,)+_ for _ in Combs))

print(f"[{ScriptName}] Running...")
c=0
for IS in IncludeSparse:
    for SO in SigOnly:
        for SB in SortBy:
            print(f"[{ScriptName}] {c+1} of {len(Combs)}...")
            GetMatrices(Gap, IS, SO, SB)
            c += 1
            print('='*80); print()
print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Latency Matrices statistics ====================================
def GetMatricesStats(Gap, IncludeSparse, SigOnly, SortPerSession, SortBy):
    Suf = ''
    if SigOnly: Suf += '_SigOnly'
    if IncludeSparse: Suf += '_Sparse'
    if SortBy == 'Error': Suf += '_SortByError'
    SPS = '-SortPerSession' if SortPerSession else ''

    GGroups, GSessions = (
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat")[0]
        for _ in ('Groups', 'Sessions')
    )

    print(f"Matrices{Suf}{SPS}")

    for Group in GroupsMS:
        try:
            SK = [
                IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/Matrices{Suf}_{Group}_{_}")[0]
                for _ in GenotypesOrder
            ]
        except FileNotFoundError:
            print(f'[{ScriptName}] No {Suf} matrix data for group {Group}.')
            continue

        SessionsU = np.unique(GSessions[GGroups==Group])
        Sessions = np.tile(SessionsU, len(SK))
        GTs = np.repeat(GenotypesOrder, len(np.unique(Sessions)))

        afes0 = []
        if not SortPerSession:
            for g,gt in enumerate(GenotypesOrder):
                s = SK[g][0]
                NeuronNo = s.shape[1]
                ts = (s.shape[0]-Gap)//2
                Suc = s[:ts,:].argmax(axis=0)
                Err = s[ts+Gap:,:].argmax(axis=0)
                sSort = Err if SortBy=='Error' else Suc

                afes0.append(
                    np.tile([
                        'Before' if _<(ts//2) else 'After'
                        for _ in sSort
                    ], 2)
                )

        SK = [_ for s in SK for _ in s]

        ad,afc,afe,afs,afg,aid = [],[],[],[],[],[]

        for si,s in enumerate(SK):
            NeuronNo = s.shape[1]
            ts = (s.shape[0]-Gap)//2
            Suc = s[:ts,:].argmax(axis=0)
            Err = s[ts+Gap:,:].argmax(axis=0)
            sSort = Err if SortBy=='Error' else Suc

            aad = np.concatenate((Suc,Err))
            aafc = np.array(['Success']*NeuronNo+['Error']*NeuronNo)
            aafe = np.tile([
                'Before' if _<(ts//2) else 'After'
                for _ in sSort
            ], 2)
            aaid = np.tile(np.arange(NeuronNo), 2)+(1000*(si//3))

            if not SortPerSession:
                aafe = dcp(afes0[GenotypesOrder.index(GTs[si])])

            # Convert time to s
            aad = Analysis.Normalize(aad,(-(ts-1),(ts-1)))/(ts-1)

            ad = np.concatenate((ad,aad))
            afc = np.concatenate((afc,aafc))
            afe = np.concatenate((afe,aafe))
            aid = np.concatenate((aid,aaid))
            afs = np.concatenate((afs,[Sessions[si]]*aad.shape[0]))
            afg = np.concatenate((afg,[GTs[si]]*aad.shape[0]))

        afs = afs.astype(int).astype(str)


        # Full dataset -------------------------------------------------
        afn = ['Class','Gen','Session']
        Res = Stats.Full(
            ad, [afc,afg,afs], aid, [True,False,True], 'auto', afn,
        )

        AOutName = f"Stats-PeakLat{Suf}{SPS}-{'_'.join(sorted(afn))}-{Group}-{WindowName}"
        IO.Txt.Write(Res, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")
        IO.Bin.Write(
            dict(
                Data=ad,
                FacNames=np.array(afn),
                FXs=np.array([afc,afg,afs]).T,
                Ids=aid
            ),
            f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data"
        )


        # Separate by epoch --------------------------------------------
        afn = ['Class','Gen','Session','Epoch']
        Res = Stats.Full(
            ad, [afc,afg,afs,afe], aid, [True,False,True,False], 'auto', afn,
        )

        AOutName = f"Stats-PeakLat{Suf}{SPS}-{'_'.join(sorted(afn))}-{Group}-{WindowName}"
        IO.Txt.Write(Res, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")
        IO.Bin.Write(
            dict(
                Data=ad,
                FacNames=np.array(afn),
                FXs=np.array([afc,afg,afs,afe]).T,
                Ids=aid
            ),
            f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data"
        )

    msg = f"Matrices{Suf}{SPS} done"
    print(msg, '='*(78-len(msg)))


Gap = 5
IncludeSparse = (True,False)
SigOnly = (True,False)
SortPerSession = (True,False)
SortBy = ('Success','Error')

Combs = tuple(Stats.product(
    IncludeSparse, SigOnly, SortPerSession, SortBy
))
Combs = tuple(((Gap,)+_ for _ in Combs))

print(f"[{ScriptName}] Running...")
IO.MultiProcess(GetMatricesStats, Combs, os.cpu_count()-2, ASync=True)
print(f"[{ScriptName}] All done.")



#%% [PRT_MS-dF] Peak width statistics ==================================
def GetPeakWidthStats(Gap, IncludeSparse, SigOnly, SortPerSession, SortBy):
    PeakRelHeight = 0.5

    Suf = ''
    if SigOnly: Suf += '_SigOnly'
    if IncludeSparse: Suf += '_Sparse'
    if SortBy == 'Error': Suf += '_SortByError'
    SPS = '-SortPerSession' if SortPerSession else ''

    GGroups, GSessions = (
        IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/{_}.dat")[0]
        for _ in ('Groups', 'Sessions')
    )

    print(f"Matrices{Suf}{SPS}")

    for Group in GroupsMS:
        try:
            SK = [
                IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Matrices/Matrices{Suf}_{Group}_{_}")[0]
                for _ in GenotypesOrder
            ]
        except FileNotFoundError:
            print(f'[{ScriptName}] No {Suf} matrix data for group {Group}.')
            continue

        SessionsU = np.unique(GSessions[GGroups==Group])
        Sessions = np.tile(SessionsU, len(SK))
        GTs = np.repeat(GenotypesOrder, len(np.unique(Sessions)))

        afes0 = []
        if not SortPerSession:
            for g,gt in enumerate(GenotypesOrder):
                s = SK[g][0]
                NeuronNo = s.shape[1]
                ts = (s.shape[0]-Gap)//2
                Suc = s[:ts,:].argmax(axis=0)
                Err = s[ts+Gap:,:].argmax(axis=0)
                sSort = Err if SortBy=='Error' else Suc

                afes0.append(
                    np.tile([
                        'Before' if _<(ts//2) else 'After'
                        for _ in sSort
                    ], 2)
                )

        SK = [_ for s in SK for _ in s]

        ad,ap,afc,afe,afs,afg,aid = [[] for _ in range(7)]

        for si,s in enumerate(SK):
            NeuronNo = s.shape[1]
            ts = (s.shape[0]-Gap)//2
            Suc = s[:ts,:]
            Err = s[ts+Gap:,:]

            PWs, PMs = [], []
            for Sig in ((Suc,Err)):
                SigPeaks = [
                    find_peaks(Sig[:,_])[0] for _ in range(Sig.shape[1])
                ]

                SigPWs = [
                    peak_widths(
                        Sig[:,_], SigPeaks[_], rel_height=PeakRelHeight
                    )[0]
                    for _ in range(Sig.shape[1])
                ]

                SigPeaksMax = [
                    np.argmax(Sig[:,_][SigPeaks[_]])
                        if len(SigPeaks[_]) else []
                    for _ in range(Sig.shape[1])
                ]

                SigPW = np.array([
                    pw[p] if type(p)!=list else np.nan
                    for pw,p in zip(SigPWs,SigPeaksMax)
                ])

                # Convert to s
                SigPM = Analysis.Normalize(
                    Sig.argmax(axis=0), ETAWindow, (0,ts)
                    # SigPM, ETAWindow, (0,ts)
                )

                SigPW = Analysis.Normalize(
                    SigPW, (0,sum(np.abs(ETAWindow))), (0,ts)
                )

                PWs.append(SigPW)
                PMs.append(SigPM)

            aad = np.concatenate(PWs)
            aap = np.concatenate(PMs)
            aafc = np.array(['Success']*NeuronNo+['Error']*NeuronNo)
            aaid = np.tile(np.arange(NeuronNo), 2)+(1000*(si//3))

            sSort = Err if SortBy=='Error' else Suc
            sSort = sSort.argmax(axis=0)
            aafe = np.tile([
                'Before' if _<(ts//2) else 'After'
                for _ in sSort
            ], 2)

            if not SortPerSession:
                aafe = dcp(afes0[GenotypesOrder.index(GTs[si])])

            ad = np.concatenate((ad,aad))
            ap = np.concatenate((ap,aap))
            afc = np.concatenate((afc,aafc))
            afe = np.concatenate((afe,aafe))
            aid = np.concatenate((aid,aaid))
            afs = np.concatenate((afs,[Sessions[si]]*aad.shape[0]))
            afg = np.concatenate((afg,[GTs[si]]*aad.shape[0]))

        afs = afs.astype(int).astype(str)


        # Full dataset -------------------------------------------------
        afn = ['Class','Gen','Session']
        afp = [True,False,True]
        Res = Stats.Full(ad, [afc,afg,afs], aid, afp, 'auto', afn)

        AOutName = f"Stats-PeakWidth{Suf}{SPS}-{'_'.join(sorted(afn))}-{Group}-{WindowName}"
        IO.Txt.Write(Res, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")
        IO.Bin.Write(
            dict(
                Data=ad,
                Peaks=ap,
                FacNames=np.array(afn),
                FacPaired=np.array(afp),
                FXs=np.array([afc,afg,afs]).T,
                IDs=aid
            ),
            f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data"
        )


        # Separate by epoch --------------------------------------------
        afn = ['Class','Gen','Session','Epoch']
        afp = [True,False,True,False]
        Res = Stats.Full(ad, [afc,afg,afs,afe], aid, afp, 'auto', afn)

        AOutName = f"Stats-PeakWidth{Suf}{SPS}-{'_'.join(sorted(afn))}-{Group}-{WindowName}"
        IO.Txt.Write(Res, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}.dict")
        IO.Bin.Write(
            dict(
                Data=ad,
                Peaks=ap,
                FacNames=np.array(afn),
                FXs=np.array([afc,afg,afs,afe]).T,
                IDs=aid
            ),
            f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Stats/{AOutName}_Data"
        )

    msg = f"Matrices{Suf}{SPS} done"
    print(msg, '='*(78-len(msg)))


Gap = 5
IncludeSparse = (True,False)
SigOnly = (True,False)
SortPerSession = (True,False)
SortBy = ('Success','Error')

Combs = tuple(Stats.product(
    IncludeSparse, SigOnly, SortPerSession, SortBy
))
Combs = tuple(((Gap,)+_ for _ in Combs))

print(f"[{ScriptName}] Running...")
IO.MultiProcess(GetPeakWidthStats, Combs, os.cpu_count()-2, ASync=True)
print(f"[{ScriptName}] All done.")



#%% Describe Cells =============================================================
Dict = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged", AsMMap=False)[0]
Dict['ETAs_Window_1000'] = np.array(Dict['ETAs_Window_1000'], dtype=object)

Report = []
DescKeys = (
    'Groups','Animals','Sessions','Genotypes','TotalNo'
)
Desc = {K:[] for K in DescKeys}

for Group in GroupsMS:
    Report.append(f'Group: {Group}')
    iG = Dict['Groups']==Group
    AU = np.unique(Dict['Animals'][iG])

    for A in AU:
        iA = Dict['Animals']==A
        SU = np.unique(Dict['Sessions'][iG*iA])
        Gen = Dict['Genotypes'][iA][0]

        DL = []
        for S in SU:
            iS = Dict['Sessions']==S
            CU = np.unique(Dict['Classes'][iG*iA*iS])

            Report.append(f'{A} {Gen} Session {S}')

            D = {
                C: Dict['ETAs_Window_1000'][iG*iA*iS*(Dict['Classes']==C)][0].shape
                if len(Dict['ETAs_Window_1000'][iG*iA*iS*(Dict['Classes']==C)])
                else 0
                for C in CU
            }

            DL.append(D)
            Report.append(D)

        F = [r[k] for r in DL for k in r.keys()]
        C = [r[k] for r in DL for k in r.keys() if len(r[k])==3]
        C = C[0][2] if len(C) else 0
        Report.append(f"Total: {C} cells")
        if (0,) in F:
            Report.append(f"ANIMAL EXCLUDED")
        else:
            for S in SU:
                for K,V in zip(DescKeys, (Group, A, S, Gen, C)):
                    Desc[K].append(V)

        Report.append('-'*50)

    Report.append('='*50)

Desc = IO.Txt.DictListsToArrays(Desc)

for _ in Report: print(_)

IO.Bin.Write(Desc, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-dF-{WindowName}-Merged/Describe")



#%% EOF ========================================================================
