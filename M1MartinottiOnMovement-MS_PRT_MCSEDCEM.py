#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2022
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""


ScriptName = 'MCSEDCEM'

import os
import numpy as np

from glob import glob
from sciscripts.Analysis import CalciumImaging
from sciscripts.IO import IO


RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data"
AnalysisPath = f"{RepoPath}/Examples/Analysis"



#%% Detect cells, extract sources, evaluate components and deconvolve to spks ==
Suf = 'PRT'

print(f'[{ScriptName}.{Suf}] Getting videos...')
ExpsV3 = glob(f"{DataPath}/{Suf}/**/msCam1.avi", recursive=True)
ExpsV3 = ['/'.join(_.split('/')[:-1]) for _ in ExpsV3]
ExpsV4 = glob(f"{DataPath}/{Suf}/**/0.avi", recursive=True)
ExpsV4 = ['/'.join(_.split('/')[:-1]) for _ in ExpsV4 if 'cameraDeviceName/0.avi' not in _]
Exps = np.array(sorted(ExpsV3+ExpsV4))
Exps = np.array([
    Exp.replace(f"{DataPath}/{Suf}/","")
    for Exp in Exps
])


ExpsOut = np.array([
    Exp.replace(
        f"/{Exp.split('/')[1]}/",
        f"/{Exp.split('/')[1]}_{Suf}/"
    ).replace('/miniscopeDeviceName', '').replace(' ','').replace('.','')
    for Exp in Exps
])

for E,Exp in enumerate(Exps):
    ExpD = f"{DataPath}/{Suf}/{Exp}"
    ExpA = f"{AnalysisPath}/{ExpsOut[E]}"

    CalciumImaging.Full(ExpD, ExpA)

print(f'[{ScriptName}.{Suf}] All done.')



#%% [PRT_MS] Group info ========================================================
Suf = 'PRT'

print(f'[{ScriptName}.{Suf}] Gathering data info...')
MSFiles = sorted(glob(f"{AnalysisPath}/Miniscope_*/*_{Suf}/**/.AllDone", recursive=True))
MSFiles = ['/'.join(_.split('/')[:-1])+'/Result' for _ in MSFiles]
MSFiles = [_.replace(f"{AnalysisPath}/",'') for _ in MSFiles]

# Override - Skip tests and broken recordings
MSFiles = np.array([
    _ for _ in MSFiles
    if 'Analysis_' not in _
    and 'test' not in _
    and 'No signal' not in _
    and 'Spoon/10_48_47 (1)' not in _
    and 'animal_1_miniscope/H12_M44_S15' not in _
    and 'animal_1_miniscope/H12_M46_S2' not in _
    and 'animal_1_miniscope/animal_2_miniscope_part2' not in _
    and 'animal_3_miniscope/H11_M31_S27' not in _
])


Group = {}

Group['Animals'] = np.array([
    f"{R.split('/')[0].replace('_','')}_{int([_.split('_')[1] for _ in R.split('/')  if 'animal' in _][0]):02d}"
        if 'animal' in R else
        f"{R.split('/')[0].replace('_','')}_{R.split('/')[1].split('_')[0]}"
    for R in MSFiles
])

Group['Sessions'] = np.array([
    int([_.split('_')[2] for _ in R.split('/')  if '_pellet_' in _][0])
    for R in MSFiles
])

Group['Groups'] = np.array([_.split('_')[0] for _ in Group['Animals']])


# Separate spoon and plate trials in session 5
Delivery = np.array(['Spoon' if 'spoon' in _.lower() else 'Plate' for _ in MSFiles])
Group['Sessions'][Group['Sessions']>5] += 1
Group['Sessions'][(Group['Sessions']==5)*(Delivery=='Plate')] += 1


Group['Trials'] = np.empty(len(Group['Animals']))
Group['Trials'][:] = np.nan
for Session in np.unique(Group['Sessions']):
    for Animal in np.unique(Group['Animals']):
        for t,T in enumerate(np.where((Group['Sessions']==Session)*(Group['Animals']==Animal))[0]):
            Group['Trials'][T] = t
Group['Trials'] = np.array(Group['Trials'],dtype=int)

Group['MSFiles'] = MSFiles

IO.Bin.Write(Group, f'{AnalysisPath}/{Suf}_{ScriptName}')

print(f'[{ScriptName}.{Suf}] All done.')



#%% [PRT_MS] Merge =============================================================
Suf = 'PRT'

Group = IO.Bin.Read(f'{AnalysisPath}/{Suf}_{ScriptName}')[0]
MSFiles = Group.pop('MSFiles')
MSFiles = [f"{AnalysisPath}/{_}" for _ in MSFiles]

CalciumImaging.MatchRecsFull(MSFiles, Group, AnalysisPath)


print(f'[{ScriptName}.{Suf}] All done.')



#%% EOF ========================================================================
