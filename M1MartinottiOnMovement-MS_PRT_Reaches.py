#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2023
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""


ScriptSuf = 'PRT'
ScriptName = f'M1MartinottiOnMovement_MS_{ScriptSuf}_Reaches'

print(f'[{ScriptName}] Importing dependencies...')
import numpy as np
import os
import shutil
import sys
from glob import glob

from sciscripts.Analysis import Analysis, DLC, Videos as saVideos
from sciscripts.IO import IO, Video as sioVideo


RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data"
AnalysisPath = f"{RepoPath}/Examples/Analysis"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

sys.path.insert(len(sys.path), RepoPath)
import M1MartinottiOnMovement_Core as Core


print(f'[{ScriptName}] Done.')



#%% [PRT_MS-Reaches] Extract reaches and get time offset =======================
print(f'[{ScriptName}] Gathering data info...')
DLCFiles = [
    _ for _ in sorted(glob(
        f'{DataPath}/{ScriptSuf}/**/*.h5', recursive=True
    ))
    if 'iteration-' not in _      # DLC folder
    and 'labeled-data' not in _   # DLC folder
    and '_DeepLabCut' not in _    # DLC folder
    and 'Miniscope' in _          # Testing analysis
]

Group = {}

Group['Sessions'] = np.array([
    int([_.split('_')[2] for _ in R.split('/')  if '_pellet_' in _][0])
    for R in DLCFiles
])

Group['Animals'] = []
for R in DLCFiles:
    if 'animal' in R:
        Group['Animals'].append(
            R.replace(DataPath,'').split('/')[2].replace('_','') +
            f"_{int([_.split('_')[1] for _ in R.split('/')  if 'animal' in _][0]):02d}"
        )
    else:
        Group['Animals'].append(
            R.replace(DataPath,'').split('/')[2].replace('_','')+'_'+
            R.replace(DataPath,'').split('/')[3]
        )
Group['Animals'] = np.array(Group['Animals'])

Group['Groups'] = np.array([_.split('_')[0] for _ in Group['Animals']])

# Separate spoon and plate trials in session 5
Delivery = np.array(['Spoon' if 'spoon' in _.lower() else 'Plate' for _ in DLCFiles])
Group['Sessions'][Group['Sessions']>5] += 1
Group['Sessions'][(Group['Sessions']==5)*(Delivery=='Plate')] += 1

Group['Trials'] = np.empty(len(Group['Animals']))
Group['Trials'][:] = np.nan
for Session in np.unique(Group['Sessions']):
    for Animal in np.unique(Group['Animals']):
        for t,T in enumerate(np.where((Group['Sessions']==Session)*(Group['Animals']==Animal))[0]):
            Group['Trials'][T] = t
Group['Trials'] = np.array(Group['Trials'],dtype=int)


Group['Output'] = np.array([
    f"{AnalysisPath}/{Group['Groups'][DF]}/{Group['Animals'][DF]}-{ScriptSuf}-Session{Session}_{Group['Trials'][DF]+1}/Reaches"
    for DF,Session in enumerate(Group['Sessions'])
])


print(''); print(f'[{ScriptName}] Extracting reaches and time offsets...')
for DF,DLCFile in enumerate(DLCFiles):
    print(f'[{ScriptName}] File {DF+1} of {len(DLCFiles)}...')
    print(f"    [{ScriptName}] {'/'.join(DLCFile.split('/')[-2:])}")

    Output = Group['Output'][DF]

    if os.path.isdir(Output):
        print(f'    [{ScriptName}] Already done!')
        continue

    print(f'        [{ScriptName}] Getting time offset...')
    DLCVideoFile = DLCFile.split('DLC')[0]
    if len(glob(f'{DLCVideoFile}.???')):
        DLCVideoFile = glob(f'{DLCVideoFile}.???')[0]
        dvInfo = sioVideo.GetInfo(DLCVideoFile)
        LedStart = saVideos.GetLedBlinkStart(DLCVideoFile)
    else:
        dvInfo = {'FPS': 50, 'Width': 1920, 'Height': 1080}
        LedStart = 0
    print(f'        [{ScriptName}] Done.')


    print(f'        [{ScriptName}] Getting reach times...')
    DLCX, DLCY, DLCLh, BodyParts = DLC.GetXYLh(DLCFile)

    Time = np.arange(DLCX.shape[0])/dvInfo['FPS']
    Time -= LedStart

    Good = np.ones(DLCX.shape[0], dtype=bool)
    for b in range(5,7):
        Good *= DLCLh[:,b] > Core.LHCutOff

    Reaches = np.zeros(Good.shape[0])
    Reaches[np.where(Good)[0]] = 1

    # Remove single-frame non-reaches
    ReachesSingle = np.where(
        ((Reaches[:-2] == 1) * (Reaches[1:-1] == 0)) *
        (Reaches[2:] == 1)
    )[0]+1
    Reaches[ReachesSingle] = 1

    # Remove single-frame reaches
    ReachesSingle = np.where(
        ((Reaches[:-2] == 0) * (Reaches[1:-1] == 1)) *
        (Reaches[2:] == 0)
    )[0]+1
    Reaches[ReachesSingle] = 0
    print(f'        [{ScriptName}] Done.')

    TTLs = Analysis.QuantifyTTLs(Reaches)
    TTLsSec = TTLs[np.unique(np.round(Time[TTLs]).astype(int), return_index=True)[1]]
    ReachesSec = Time[TTLsSec]

    # Write to disk
    IO.Bin.Write({
        'Reaches': Reaches,
        'ReachesSec': ReachesSec,
        'Time': Time,
        'TTLs': TTLs,
        'TTLsSec': TTLsSec
    }, Output)

Group['Files'] = np.array(DLCFiles)
IO.Bin.Write(Group, f"{AnalysisPath}/GroupAnalysis/PRT_MS-Reaches")

print(f'[{ScriptName}] All done.')



#%% [PRT_MS-Reaches] Classify reaches ==========================================
print(''); print(f'[{ScriptName}] Classifying reaches...')
Group = IO.Bin.Read(f"{AnalysisPath}/GroupAnalysis/PRT_MS-Reaches")[0]
DLCFiles = Group['Files']

for DF,DLCFile in enumerate(DLCFiles):
    print(f'[{ScriptName}] File {DF+1} of {len(DLCFiles)}...')
    print(f"    [{ScriptName}] {'/'.join(DLCFile.split('/')[-2:])}")

    Output = Group['Output'][DF]
    if os.path.exists(Output+'/ReachesClass.dat'):
        print(f'    [{ScriptName}] Already done!')
        continue

    Reaches, ReachesSec, TTLs, TTLsSec, Time = [
        IO.Bin.Read(f'{Output}/{_}.dat', AsMMap=False)[0]
        for _ in ['Reaches', 'ReachesSec', 'TTLs', 'TTLsSec', 'Time']
    ]

    OutputInvalid = Output+'/TTLsInvalid.dat'
    Output += '/ReachesClass.dat'

    DLCVideoFile = DLCFile.split('DLC')[0]
    if len(glob(f'{DLCVideoFile}.???')):
        DLCVideoFile = glob(f'{DLCVideoFile}.???')[0]
        dvInfo = sioVideo.GetInfo(DLCVideoFile)
    else:
        dvInfo = {'FPS': 50, 'Width': 1920, 'Height': 1080}

    Result, TTLsInvalid = Core.ReachClassify(Reaches, Time, TTLsSec, Core.LHCutOff, DLCFile, dvInfo)

    try: Session = int(DLCFile.split('/')[-2].split('_')[-1])
    except: Session = DF

    print(f"   [{ScriptName}] Session {Session}, {Result.shape[0]} reaches, {Result[Result=='Success'].shape[0]} correct")

    IO.Bin.Write(Result, Output)
    IO.Bin.Write(TTLsInvalid, OutputInvalid)



#%% [PRT_MS-Reaches] Group Reaches =============================================
print(''); print(f'[{ScriptName}] Gathering group data...')
Group = IO.Bin.Read(f"{AnalysisPath}/GroupAnalysis/PRT_MS-Reaches")[0]
DLCFiles = Group['Output']

print(f'    [{ScriptName}] Loading reaches...')
Group['ReachesClasses'] = [
    IO.Bin.Read(
        DLCFile+'/ReachesClass.dat',
        AsMMap=False
    )[0]
    for DF,DLCFile in enumerate(DLCFiles)
]
Group['ReachesClasses'] = [
    np.array([_ if _ == 'Success' else 'Error' for _ in r])
    for r in Group['ReachesClasses']
]

print(f'    [{ScriptName}] Loading times...')
Group['TotalTimes'] = [
    IO.Bin.Read(
        DLCFile+'/Time.dat',
        AsMMap=False
    )[0]
    for DF,DLCFile in enumerate(DLCFiles)
]
Group['TotalTimes'] = np.array([(_[-1]/60)/10 for _ in Group['TotalTimes']])

AnimalsList, SessionsList = [np.unique(Group[_]) for _ in ('Animals', 'Sessions')]

GroupMerged = {
    'Animals': [animal for animal in AnimalsList for session in SessionsList],
    'Sessions': [session for animal in AnimalsList for session in SessionsList],
    'Groups': [animal.split('_')[0] for animal in AnimalsList for session in SessionsList],
    'TotalTimes': [
        Group['TotalTimes'][(Group['Animals']==animal)*(Group['Sessions']==session)].sum()
        for animal in AnimalsList for session in SessionsList
    ],
    'ReachesClasses': [
        np.concatenate([
            Group['ReachesClasses'][_]
            for _ in np.where((Group['Animals']==animal)*(Group['Sessions']==session))[0]
        ])
        if np.where((Group['Animals']==animal)*(Group['Sessions']==session))[0].size
        else np.array([])
        for animal in AnimalsList for session in SessionsList
    ],
}

GroupMerged = {k: np.array(v)  if k != 'ReachesClasses' else v for k,v in GroupMerged.items()}

try: shutil.rmtree(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Reaches-Merged")
except FileNotFoundError: pass
IO.Bin.Write(GroupMerged, f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Reaches-Merged")
print('Done.')



#%% [PRT] Quantify group data ==========================================
print(''); print(f'[{ScriptName}] Quantifying group data...')
Base = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Reaches-Merged"

GroupReaches = {
    _.split('.')[0]: IO.Bin.Read(f"{Base}/{_}", AsMMap=False)[0]
    for _ in (
        'ReachesClasses', 'TotalTimes.dat', 'Animals.dat',
        'Sessions.dat', 'Groups.dat'
    )
}

GroupData = {}
GroupData['Total'] = [_.shape[0] for _ in GroupReaches['ReachesClasses']]

GroupData = {**GroupData, **{
    K: [_[_==K].shape[0] for _ in GroupReaches['ReachesClasses']]
    for K in ['Success', 'Error']
}}

for K in ['Total', 'Success', 'Error']:
    GroupData[K+'Raw'] = np.array(GroupData[K])
    GroupData[K] = np.array(GroupData[K])/((GroupReaches['TotalTimes']/60)/10)

GroupData['SuccessRatio'] = GroupData['Success']/GroupData['Total']
GroupData['SuccessRatio'][GroupData['TotalRaw']==0] = 0

Out = f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Reaches-Merged/ReachesQnt"
try: shutil.rmtree(f"{Out}/")
except FileNotFoundError: pass
IO.Bin.Write(GroupData, Out)

print(f'[{ScriptName}] Done.')




#%% [PRT_MS-Reaches] Group Reaches describe ====================================
GroupMerged = IO.Bin.Read(f"{AnalysisPathShared}/GroupAnalysis/PRT_MS-Reaches-Merged")[0]
GroupMerged['ReachesClasses'] = np.array(GroupMerged['ReachesClasses'], dtype=object)

Report = []
for Group in np.unique(GroupMerged['Groups']):
    Report.append(f'Group {Group}')
    iG = GroupMerged['Groups']==Group

    for Animal in np.unique(GroupMerged['Animals'][iG]):
        iA = GroupMerged['Animals']==Animal

        for Session in np.unique(GroupMerged['Sessions'][iG]):
            if Session in (3,4): continue
            iS = GroupMerged['Sessions']==Session

            Reaches = GroupMerged['ReachesClasses'][iG*iA*iS]
            if len(Reaches) > 1:
                raise LookupError('Mismatching data.')
            elif len(Reaches)==0:
                Report.append(
                    f'    {Animal}, Session {Session}, No reaches'
                )
                continue

            Reaches = Reaches[0]

            if len(Reaches)==0:
                Report.append(
                    f'    {Animal}, Session {Session}, No reaches'
                )
                continue

            for Class in ('Success','Error'):
                n = len(Reaches[Reaches==Class])
                Report.append(
                    f'    {Animal}, Session {Session}, {Class} = {n}'
                )


        Report.append('-'*70)

    Report.append('='*70)

for _ in Report: print(_)



#%% EOF ========================================================================
