#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2021
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""

ScriptSuf = 'PRT_Tracking'
ScriptName = f'M1MartinottiOnMovement_{ScriptSuf}'

print(f'[{ScriptName}] Importing dependencies...')
import os; os.environ["DLClight"]="True"
import deeplabcut
from glob import glob

RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DLCNetworks = RepoPath
DataPath = f"{RepoPath}/Examples/Data"

print(f'[{ScriptName}] Done.')



#%% [Tracking] =========================================================
VideoFormats = ['MP4', 'mp4', 'mkv']
DLCNetworkPath = f"{DLCNetworks}/Mice-Fingers_Forepaws_Nose_Pellet-PRT-Front"
path_config_file = f'{DLCNetworkPath}/config.yaml'

for Ext in VideoFormats:
    Videos = []
    Videos += sorted(glob(f'{DataPath}/CaspM1MC*/*PelletReaching*/*.{Ext}'))
    Videos += sorted(glob(f'{DataPath}/LFPM1GqMC/*/*.{Ext}'))
    Videos += sorted(glob(f'{DataPath}/PRT/**/*.{Ext}', recursive=True))

    Videos = [_ for _ in Videos if '_labeled' not in _]

    if not len(Videos): continue
    deeplabcut.analyze_videos(path_config_file, Videos, videotype=Ext)

print('Done.')



#%% EOF ================================================================
