#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2021
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""


ScriptName = 'M1MartinottiOnMovement_Tracing'

print(f'[{ScriptName}] Importing dependencies...')
import numpy as np, os
from glob import glob
from scipy.interpolate import interp1d
from scipy.stats import gaussian_kde
from sciscripts.Analysis import Analysis, Stats
from sciscripts.IO import IO

RepoPath = f"{os.environ['HOME']}/M1MartinottiOnMovement"
DataPath = f"{RepoPath}/Examples/Data"
AnalysisPath = f"{RepoPath}/Examples/Analysis"
AnalysisPathShared = AnalysisPath # Provided so the final (lighter) files can be saved elsewhere

print(f'[{ScriptName}] Done.')



#%% [Tracing] Cell area ================================================
Sets = sorted(glob(f"{DataPath}/Tracing-CellsQnt/Cell_diameter_*"))

for S,Set in enumerate(Sets):
    CSVs = sorted(glob(f"{Set}/*.csv"))
    APDP = {'AP': [], 'Area':[]}

    print(f'[{ScriptName}] Processing CSVs...')
    for File in CSVs:
        print(f"    {File.split('/')[-1]}")
        Animal = File.split('/')[-1].split('.')[0].split('_')[0].split('_')[0]
        try:
            AP = int(File.split('/')[-1].split('.')[0].split('AP')[1].split('_')[0])
        except IndexError:
            print(f'[{ScriptName}] AP not found in {os.path.basename(File)}.')
            continue

        FData = np.genfromtxt(File, delimiter=',', skip_header=1)

        if len(FData.shape) == 2: Area = FData[:,1]
        else: Area = np.array([FData[1]])

        APDP['AP'] += [AP]*len(Area)
        APDP['Area'] += Area.tolist()

    APDP = IO.Txt.DictListsToArrays(APDP)
    Out = os.path.dirname(CSVs[0])+'/Area'
    Out = Out.replace(DataPath,AnalysisPathShared)
    IO.Bin.Write(APDP, Out)


print(f'[{ScriptName}] Done.')



#%% [Tracing] Cell area statistics =====================================
SetsOrder = ['500_um_anterior', 'injection_site', '1_mm_posterior']

Sets = sorted(glob(f"{AnalysisPathShared}/Tracing-CellsQnt/Cell_diameter_*/Area"))
Sets = [s for A in SetsOrder for s in [_ for _ in Sets if A in _]]
TotalSets = [IO.Bin.Read(Set)[0]['Area'] for Set in Sets]

ad = np.array([_ for a in TotalSets for _ in a])
af = np.array([_ for S,Set in enumerate(TotalSets) for _ in [SetsOrder[S]]*len(Set)])
aid = np.array([_ for S,Set in enumerate(TotalSets) for _ in np.arange(len(Set))+(1000*S)])
AR = Stats.Full(ad, [af], aid, [False], 'auto', ['AP'])

IO.Txt.Write(AR, f"{AnalysisPathShared}/Tracing-CellsQnt/Stats/Stats-CellArea-AP.dict")
IO.Bin.Write(
    dict(
        Data = ad,
        FXs = np.array([af]).T,
        Ids = aid,
        FacNames = np.array(['Area']),
        FacPaired = np.array([False])
    ),
    f"{AnalysisPathShared}/Tracing-CellsQnt/Stats/Stats-CellArea-AP_Data"
)


print(f'[{ScriptName}] Done.')



#%% [Tracing] Cell density AP_DV =======================================
CSVSet = (
    sorted(glob(f"{DataPath}/Tracing-CellsQnt/Depth_injection_site/*.csv")),
    sorted(glob(f"{DataPath}/Tracing-CellsQnt/Depth_1_mm_posterior_S1/*.csv")),
    sorted(glob(f"{DataPath}/Tracing-CellsQnt/Depth_500_um_anterior_M1_M2/*.csv"))
)

for CSVs in CSVSet:
    APDP = {'AP': [], 'FX':[], 'FY':[], 'Dist':[]}

    print(f'[{ScriptName}] Processing CSVs...')
    for File in CSVs:
        print(f"    {File.split('/')[-1]}")
        Animal = File.split('/')[-1].split('.')[0].split('_')[0].split('_')[0]
        SBSize_um = int(File.split('/')[-1].split('.')[0].split('SB')[1])
        AP = int(File.split('/')[-1].split('.')[0].split('AP')[1].split('_')[0])
        FData = np.genfromtxt(File, delimiter=',', skip_header=1)

        CtxSf, SB = FData[-4:-2,[2,3]], FData[-2:,[2,3]]
        SBSize_px = np.diff(SB[:,0])[0]
        pxSize = SBSize_um/SBSize_px

        FX, FY = FData[:-4,2], FData[:-4,3]
        fi = interp1d(CtxSf[:,0],CtxSf[:,1],fill_value='extrapolate')

        # Distance straight up -----------------------------------------
        # CtxSfI = fi(FX)
        # Dist = FY - CtxSfI

        # Account for ctx surface angle --------------------------------
        FullMin = 1000*(FX.min()//1000)-(1000*((FX.min()%1000)>0))
        FullMax = 1000*(FX.max()//1000)+(1000*((FX.max()%1000)>0))
        FullX = np.linspace(FullMin, FullMax, 4000)
        FullY = fi(FullX)

        CellsDist = [
            [np.array([[C,f] for f in F]).ravel() for C in S]
            for S,F in zip((FX,FY),(FullX,FullY))
        ]

        CellsDist = np.array([
            Analysis.EucDist(CellsDist[0][C], CellsDist[1][C])[0][::2]
            for C in range(len(FX))
        ])

        Dist, DistPos = (
            np.array([f(D) for D in CellsDist])
            for f in (np.min,np.argmin)
        )

        Dist *= pxSize

        APDP['AP'].append(np.array([AP]*len(FX)))
        APDP['FX'].append(FX)
        APDP['FY'].append(FY)
        APDP['Dist'].append(-Dist)

    print(f'[{ScriptName}] Processing histogram...')
    TotalDist = np.array([_ for a in APDP['Dist'] for _ in a])
    BinSize = 2*abs(np.diff(np.percentile(TotalDist, [75,25])))/(len(Dist)**(1/3))
    BinNo =  int((np.ptp(TotalDist)/BinSize).round())
    Hist = np.histogram(TotalDist, BinNo)[0]


    print(f'[{ScriptName}] Calculating density...')
    FZ = np.array([_ for a in APDP['Dist'] for _ in a])
    FY = np.array([_ for a in APDP['AP'] for _ in a])
    deltaZ = (max(FZ) - min(FZ))/5
    deltaY = (max(FY) - min(FY))/5
    zmin = min(FZ) - deltaZ
    zmax = max(FZ) + deltaZ
    ymin = min(FY) - deltaY
    ymax = max(FY) + deltaY
    zz, yy = np.mgrid[zmin:zmax:100j, ymin:ymax:100j]

    positions = np.vstack([zz.ravel(), yy.ravel()])
    values = np.vstack([FZ,FY])
    kernel = gaussian_kde(values)
    f = np.reshape(kernel(positions).T, zz.shape)


    print(f'[{ScriptName}] Writing...')
    Out = os.path.dirname(CSVs[0])+'/Density-AP_DV'
    Out = Out.replace(DataPath,AnalysisPathShared)
    IO.Bin.Write(dict(
        FY=FY, FZ=FZ, yy=yy, zz=zz, f=f, TotalDist=TotalDist, Hist=Hist
    ), Out)


print(f'[{ScriptName}] Done.')



#%% [Tracing] Cell density ML_DV =======================================
CSVs = sorted(glob(f"{DataPath}/Tracing-CellsQnt/Density_plot/*CSVs/*.csv"))
APDP = {'AP': [], 'FX':[], 'FY':[], 'Dist':[]}

for File in CSVs:
    Animal = File.split('/')[-1].split('.')[0].split('Animal')[1].split('_')[0]
    SBSize_um = int(File.split('/')[-1].split('.')[0].split('SB')[1])
    AP = int(File.split('/')[-1].split('.')[0].split('AP')[1].split('_')[0])
    FData = np.genfromtxt(File, delimiter=',', skip_header=1)

    ML, SB = FData[-4:-2,[2,3]], FData[-2:,[2,3]]
    SBSize_px = np.diff(SB[:,0])[0]
    pxSize = SBSize_um/SBSize_px


    # Get distance
    FX, FY = FData[:-4,2], FData[:-4,3]
    Dist = FX - np.interp(FY,ML[:,1],ML[:,0])
    Dist *= pxSize
    APDP['AP'].append(AP)
    APDP['FX'].append(FX)
    APDP['FY'].append(FY)
    APDP['Dist'].append(Dist)
    if FX.shape[0] <= 1: continue


    ## Get density
    FX, FY = FData[:,2]-ML[:,0].mean(), FData[:,3]
    deltaX = (max(FX) - min(FX))/5
    deltaY = (max(FY) - min(FY))/5
    xmin = min(FX) - deltaX
    xmax = max(FX) + deltaX
    ymin = min(FY) - deltaY
    ymax = max(FY) + deltaY
    xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]

    FX, FY = FData[:-4,2], FData[:-4,3]
    FX, FY = FX*pxSize, FY*pxSize
    FX -= ML[:,0].mean()
    positions = np.vstack([xx.ravel(), yy.ravel()])
    values = np.vstack([FX,FY])
    kernel = gaussian_kde(values)
    f = np.reshape(kernel(positions).T, xx.shape)

    fi = interp1d(ML[:,1],ML[:,0],fill_value='extrapolate')
    MLX = fi([ymin, ymax])
    fi = interp1d(ML[:,0],ML[:,1],fill_value='extrapolate')
    MLY = fi([xmin, xmax])


APDP['AP'] = np.array(APDP['AP'])
APFY = [np.array([APDP['AP'][C]]*Cell.shape[0]) for C,Cell in enumerate(APDP['FX'])]

FX = np.array([_ for a in APDP['Dist'] for _ in a])
FY = np.array([_ for a in APFY for _ in a])
deltaX = (max(FX) - min(FX))/5
deltaY = (max(FY) - min(FY))/5
xmin = min(FX) - deltaX
xmax = max(FX) + deltaX
ymin = min(FY) - deltaY
ymax = max(FY) + deltaY
xx, yy = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]

positions = np.vstack([xx.ravel(), yy.ravel()])
values = np.vstack([FX,FY])
kernel = gaussian_kde(values)
f = np.reshape(kernel(positions).T, xx.shape)

fi = interp1d(ML[:,1],ML[:,0],fill_value='extrapolate')
MLX = fi([ymin, ymax])
fi = interp1d(ML[:,0],ML[:,1],fill_value='extrapolate')
MLY = fi([xmin, xmax])

IO.Bin.Write(dict(
    CellsY=FY,
    CellsX=FX,
    GridX=xx,
    GridY=yy,
    PDF=f,
    XLim=np.array((xmin,xmax)),
    YLim=np.array((ymin,ymax)),
    Dist=np.array([[_ for a in APDP['Dist'] for _ in a]])
), f"{AnalysisPathShared}/Tracing-CellsQnt/Density_plot/Density-ML_DV"
)



#%% EOF ========================================================================
