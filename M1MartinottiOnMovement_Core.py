#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti <malfatti@disroot.org>
@date: 2023
@license: GNU GPLv3 <https://gitlab.com/malfatti/M1MartinottiOnMovement/raw/main/LICENSE>
@homepage: https://gitlab.com/malfatti/M1MartinottiOnMovement
"""


ScriptName = 'M1MartinottiOnMovement_Core'

print(f'[{ScriptName}] Importing dependencies...')
import numpy as np
from sciscripts.Analysis import Analysis, DLC
print(f'[{ScriptName}] Done.')


LHCutOff = 0.7
ETAWindow = (-1, 1)
WindowName = f'Window_{int(ETAWindow[1]*1000)}'


def ReachClassify(Reaches, Time, TTLsSec, LHCutOff, DLCFile, dvInfo):
    DLCX, DLCY, DLCLh, BodyParts = DLC.GetXYLh(DLCFile)
    LPaw = Analysis.EucDist(DLCX[:,5], DLCY[:,5])[0]
    LPaw[~Reaches[1:].astype(bool)] = 0
    Pellet = Analysis.EucDist(DLCX[:,-1], DLCY[:,-1])[0]
    Pellet[~Reaches[1:].astype(bool)] = 0
    PelletAngle = Analysis.EucDist(DLCX[:,-1], DLCY[:,-1])[1]
    PelletAngle[~Reaches[1:].astype(bool)] = 0

    FullArea = dvInfo['Width']*dvInfo['Height']
    w = dvInfo['FPS']//2

    PelletArea = np.zeros(TTLsSec.shape[0], dtype=float)
    PelletMoveXY = np.zeros((TTLsSec.shape[0],2), dtype=float)
    tpMean = np.zeros(TTLsSec.shape[0], dtype=float)
    tlhMean = np.zeros(TTLsSec.shape[0], dtype=float)
    tlhMeanBL = np.zeros(TTLsSec.shape[0], dtype=float)
    tlhMeanRs = np.zeros(TTLsSec.shape[0], dtype=float)
    ppdMeanBL = np.zeros(TTLsSec.shape[0], dtype=float)
    ppdMeanRs = np.zeros(TTLsSec.shape[0], dtype=float)

    Result = []
    TTLsInvalid = []

    for T,TTL in enumerate(TTLsSec):
        tw = [TTL-w, TTL+w]
        if min(tw) < 0 or max(tw) > Time.shape[0]:
            # Reach happened at the edge (time) of the video
            TTLsInvalid.append(T)
            continue

        tReachesEnd = Analysis.QuantifyTTLs(Reaches[tw[0]:tw[1]], LHCutOff, 'fall')
        if len(tReachesEnd): tReachesEnd = tReachesEnd[0]+1
        else: tReachesEnd = Reaches[tw[0]:tw[1]].shape[0]

        tReaches = Reaches.copy()
        tReaches[tw[0]:tw[1]][tReachesEnd:] = 0
        ReachRange = np.where((tReaches[tw[0]:tw[1]]))[0]

        tTime = Time[tw[0]:tw[1]]
        tBL = tTime < Time[TTL]
        tl, tp, tlh = LPaw[tw[0]:tw[1]], Pellet[tw[0]:tw[1]], DLCLh[tw[0]:tw[1],-1]
        tpa = PelletAngle[tw[0]:tw[1]]
        tpaUp = np.pi/2-0.5 < tpa[ReachRange].mean() < np.pi/2+0.5

        PA = Analysis.PolygonArea(DLCX[tw[0]:tw[1],-1][~tBL], DLCY[tw[0]:tw[1],-1][~tBL])
        PA /= FullArea
        PelletArea[T]  = PA
        PA = PA < 5e-4

        tpMean[T] = tp[ReachRange].mean()
        tpMeanM = 0.01 < tpMean[T] < 0.5

        tlhMean[T] = tlh.mean()
        tlhMeanBL[T] = tlh[tBL].mean()
        tlhMeanRs[T] = tlh[~tBL].mean()

        tlhM = tlhMean[T] > LHCutOff
        tlhMBL = tlhMeanBL[T] > LHCutOff
        tlhMRs = tlhMeanRs[T] > LHCutOff

        ppd = Analysis.EucDist(DLCX[tw[0]:tw[1],[5,-1]].ravel(), DLCY[tw[0]:tw[1],[5,-1]].ravel())[0][::2]
        ppdMeanBL[T] = ppd[tBL].mean()
        ppdMeanRs[T] = ppd[~tBL].mean()

        PelletMoveXY[T,:] = Analysis.RMS(np.diff(DLCX[tw[0]:tw[1],-1][~tBL])), Analysis.RMS(np.diff(DLCY[tw[0]:tw[1],-1][~tBL]))

        if tlhM and PA and tpMeanM:
            Result.append('HitMiss')

        elif PA and tlhMBL == tlhMRs:
            Result.append('Miss')

        elif tlhMBL and not tlhMRs and not tpaUp and PelletMoveXY[T,0] > PelletMoveXY[T,1] and ppdMeanRs[T] > ppdMeanBL[T]:
            Result.append('HitDrop')

        elif not tlhMBL:
            Result.append('NoPellet')

        else:
            Result.append('Success')

    Result = np.array(Result)
    TTLsInvalid = np.array(TTLsInvalid)
    return(Result, TTLsInvalid)



#%% EOF ========================================================================
