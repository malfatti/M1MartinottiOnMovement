# M1MartinottiOnMovement

This repository contains all the code used for the analysis of the publication:

Motor execution, but not learning, improves upon increased layer 5 specific Martinotti cell excitation



## Dependencies

### Python  
- Numpy  
- Scipy  
- DeepLabCut  
- CaImAn    
- OpenCV      
- SciScripts  
- R  
- RPy2  

### R  
- rstatix  
- rcompanion  
- coin  
- nparLD  

### Other
- OS: Linux  
- Matlab (only for assembly detection)  
- [GoodhillLab/neural-assembly-detection](https://github.com/GoodhillLab/neural-assembly-detection)  


## Instructions

These instructions assume that `conda` is available for Python environment management.

1. Clone this repository in your home folder (paths are hardcoded in the scripts):

```bash
$ cd ~/

$ git clone https://gitlab.com/malfatti/M1MartinottiOnMovement
[...]

$ cd M1MartinottiOnMovement
```


2. Place the data in the `Examples/` folder following its hierarchy:

```bash
Examples/
└── Data
    ├── Behaviour_L5_Gq_MC
    │   └── [data folders]
    ├── BehavOnly
    │   ├── BehaviourL5GqMC-AnimalExperimentalOrder.dict
    │   └── Session5Split.dict
    ├── CaspM1MC
    │   └── [data folders]
    ├── CaspM1MC_LFPM1GqMC_PRTLearning
    │   └── [data folders]
    ├── LFPM1GqMC
    │   ├── [data folders]
    │   ├── LFPM1GqMC-AnimalExperimentalOrderSingleCh.dict
    │   └── LFPM1GqMC-CaspMC-AnimalExperimentalOrder.dict
    ├── MiniscopeL3_L5-AnimalExperimentalOrder.dict
    ├── PRT
    │   └── [data folders]
    └── Tracing-CellsQnt
        └── [data folders]
```


3. Create the necessary environments:

```bash
$ conda env create -f M1MoM_DLC.yml
[...]

$ conda env create -f M1MoM_CaImAn.yml
[...]

$ conda env create -f M1MoM_SS.yml
[...]

$ conda activate M1MoM_SS

$ python -c 'from sciscripts.Analysis import Stats; Stats.RCheckPackage(["rstatix","nparLD","rcompanion","coin"])'
[...]
```


4. Run DeepLabCut:

```bash
$ conda activate M1MoM_DLC

$ python M1MartinottiOnMovement-PRT_Tracking.py
[...]
```


5. Run CaImAn:

```bash
$ conda activate M1MoM_CaImAn

$ python M1MartinottiOnMovement-MS_PRT_MCSEDCEM.py
[...]
```

6. Run prehension detection and classification, calcium imaging analysis, LFP analysis, hanging wire and pasta handling quantification, and cell tracing quantification:

```bash
$ conda activate M1MoM_SS

$ python M1MartinottiOnMovement-MS_PRT_Reaches.py
[...]

$ python M1MartinottiOnMovement-MS_PRT_CalciumImaging.py
[...]

$ python M1MartinottiOnMovement-LFP_PRT_Reaches.py
[...]

$ python M1MartinottiOnMovement-CaspM1MC_PHHW.py
[...]

$ python M1MartinottiOnMovement-BehavOnly.py
[...]

$ python M1MartinottiOnMovement-Tracing.py
[...]
```

7. Run calcium imaging assemblies detection:

This is the only portion of code that relies on Matlab. Clone the [GoodhillLab/neural-assembly-detection](https://github.com/GoodhillLab/neural-assembly-detection) repository and add it to the Matlab path. Matlab is assumed to be found at `/usr/bin/matlab`. If that is not the case, modify the `M1MartinottiOnMovement-MS_PRT_AssembliesDetection.py` script, specifically the variable `MLab`.

```bash
$ conda activate M1MoM_SS

$ python M1MartinottiOnMovement-MS_PRT_AssembliesDetection.py
[...]
```

8. Run calcium imaging assemblies analysis:

If using a small dataset (for code testing), there may not be any assemblies detected, making it impossible to evaluate the code. To circumvent this issue, the code will ask if it is a test run (by setting the `CodeTest` variable to 1). In that case, in situations where no assemblies are found, 3 assemblies will be made of a random selection of neurons from the available data.

```bash
$ conda activate M1MoM_SS

$ python M1MartinottiOnMovement-MS_PRT_AssembliesAnalysis.py
[...]
```

Done!



